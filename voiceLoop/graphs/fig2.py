import sys
sys.path.append('..')
import os
import shutil
import torch

import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt

import numpy
#%matplotlib notebook

from voiceLoop.data import *
from voiceLoop.model import Loop
from voiceLoop.utils import generate_merlin_wav

from torch.autograd import Variable
from IPython.display import Audio

def plot(data, labels, dict_file):
    labels_dict = dict_file
    labels_dict = {v: k for k, v in labels_dict.iteritems()}
    labels = [labels_dict[x].decode('latin-1') for x in labels]

    axarr = plt.subplot()
    axarr.imshow(data.T, aspect='auto', origin='lower', interpolation='nearest', cmap=cm.viridis)
    axarr.set_yticks(numpy.arange(0, len(data.T)))
    axarr.set_yticklabels(labels, rotation=90)

data_path = os.path.abspath('../data/vctk/numpy_features')
norm_info = os.path.abspath('../data/vctk/norm_info/norm.dat')
    
train_dataset = NpzFolder(data_path)
valid_dataset = NpzFolder(data_path + '_valid')

# torch.cuda.set_device(1)

checkpoint = '../models/vctk'
# weights = torch.load(checkpoint + '/bestmodel.pth')
weights = torch.load(checkpoint + '/bestmodel.pth', map_location=lambda storage, loc: storage)


args = torch.load(checkpoint + '/args.pth')
opt = args[0]
opt.noise = 0

model = Loop(opt)
model.load_state_dict(weights)
# model.cuda();
# model.eval();
print(model)

N_a = w[2].data.numpy()
N_o = w[6].data.numpy()
N_u = w[12].data.numpy()

N_list = [N_a, N_o, N_u]


import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt

from matplotlib.ticker import FuncFormatter, MaxNLocator

#%matplotlib notebook
fig = plt.figure(figsize=(8, 4.25))
ax = fig.add_subplot(111)



N_plot = np.mean(np.abs(N_list[0]),0)
N_plot = N_plot.reshape(319,20)
N_plot = np.mean(N_plot, 0)
plt.plot(np.arange(1,21,1, dtype=np.uint8),N_plot, '-ro')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

N_plot = np.mean(np.abs(N_list[1]),0)
N_plot = N_plot.reshape(319,20)
N_plot = np.mean(N_plot, 0)
plt.plot(np.arange(1,21,1, dtype=np.uint8),N_plot, '-b*')

N_plot = np.mean(np.abs(N_list[2]),0)
N_plot = N_plot.reshape(319,20)
N_plot = np.mean(N_plot, 0)
plt.plot(np.arange(1,21,1, dtype=np.uint8),N_plot, '-g^')




plt.legend(('Na', 'No', 'Nu'))

plt.xlim([0.75,20.25])


plt.xlabel('Buffer Position', fontsize=13)
plt.ylabel('Mean Of Absolute Weights', fontsize=13)
plt.grid()
plt.savefig('/private/home/eliyan/yaniv_model/fig5/loop/Buffer_weights.png', dpi=200)
