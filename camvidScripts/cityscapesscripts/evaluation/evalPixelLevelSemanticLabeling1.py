#!/usr/bin/python
#
# The evaluation script for pixel-level semantic labeling.
# We use this script to evaluate your approach on the test set.
# You can use the script to evaluate on the validation set.
#
# Please check the description of the "getPrediction" method below
# and set the required environment variables as needed, such that
# this script can locate your results.
# If the default implementation of the method works, then it's most likely
# that our evaluation server will be able to process your results as well.
#
# Note that the script is a lot faster, if you enable cython support.
# WARNING: Cython only tested for Ubuntu 64bit OS.
# To enable cython, run
# setup.py build_ext --inplace
#
# To run this script, make sure that your results are images,
# where pixels encode the class IDs as defined in labels.py.
# Note that the regular ID is used, not the train ID.
# Further note that many classes are ignored from evaluation.
# Thus, authors are not expected to predict these classes and all
# pixels with a ground truth label that is ignored are ignored in
# evaluation.

# python imports
from __future__ import print_function

import argparse
import glob
import os, sys
import platform
import fnmatch
import numpy as np
import math
from scipy.misc import imsave
import ntpath

from PIL import Image


from camvidScripts.cityscapesscripts.helpers import labels
from camvidScripts.cityscapesscripts.helpers.csHelpers import printError, getCsFileInfo, colors, getColorEntry, \
    ensurePath, writeDict2JSON
from camvidScripts.cityscapesscripts.helpers.labels import id2label, labels
from semanticSegmentationPytorchFineTuned import CamVidConstants
from semanticSegmentationPytorchFineTuned.utils import colorEncode

try:
    from itertools import izip
except ImportError:
    izip = zip

# Cityscapes imports
sys.path.append( os.path.normpath( os.path.join( os.path.dirname( __file__ ) , '..' , 'helpers' ) ) )
from camvidScripts.cityscapesscripts.helpers.csHelpers import *

# C Support
# Enable the cython support for faster evaluation
# Only tested for Ubuntu 64bit OS
CSUPPORT = False#True
# Check if C-Support is available for better performance
if CSUPPORT:
    try:
        import addToConfusionMatrix
    except:
        CSUPPORT = False


###################################
# PLEASE READ THESE INSTRUCTIONS!!!
###################################
# Provide the prediction file for the given ground truth file.
#
# The current implementation expects the results to be in a certain root folder.
# This folder is one of the following with decreasing priority:
#   - environment variable CITYSCAPES_RESULTS
#   - environment variable CITYSCAPES_DATASET/results
#   - ../../results/"
#
# Within the root folder, a matching prediction file is recursively searched.
# A file matches, if the filename follows the pattern
# <city>_123456_123456*.png
# for a ground truth filename
# <city>_123456_123456_gtFine_labelIds.png
def getPrediction( args, groundTruthFile ):
    # determine the prediction path, if the method is first called
    if not args.predictionPath:
        rootPath = None
        if 'CITYSCAPES_RESULTS' in os.environ:
            rootPath = os.environ['CITYSCAPES_RESULTS']
        elif 'CITYSCAPES_DATASET' in os.environ:
            rootPath = os.path.join( os.environ['CITYSCAPES_DATASET'] , "results" )
        else:
            rootPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','..','results')

        if not os.path.isdir(rootPath):
            printError("Could not find a result root folder. Please read the instructions of this method.")

        args.predictionPath = rootPath

    # walk the prediction path, if not happened yet
    if not args.predictionWalk:
        walk = []
        for root, dirnames, filenames in os.walk(args.predictionPath):
            walk.append( (root,filenames) )
        args.predictionWalk = walk

    csFile = getCsFileInfo(groundTruthFile)
    filePattern = "{}_{}_{}*.png".format( csFile.city , csFile.sequenceNb , csFile.frameNb )

    predictionFile = None
    for root, filenames in args.predictionWalk:
        for filename in fnmatch.filter(filenames, filePattern):
            if not predictionFile:
                predictionFile = os.path.join(root, filename)
            else:
                printError("Found multiple predictions for ground truth {}".format(groundTruthFile))

    if not predictionFile:
        printError("Found no prediction for ground truth {}".format(groundTruthFile))

    return predictionFile


######################
# Parameters
######################


# A dummy class to collect all bunch of data
class CArgs(object):
    pass
# And a global object of that class
args = CArgs()

# Where to look for Cityscapes
if 'CITYSCAPES_DATASET' in os.environ:
    args.cityscapesPath = os.environ['CITYSCAPES_DATASET']
else:
    args.cityscapesPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','..')

if 'CITYSCAPES_EXPORT_DIR' in os.environ:
    export_dir = os.environ['CITYSCAPES_EXPORT_DIR']
    if not os.path.isdir(export_dir):
        raise ValueError("CITYSCAPES_EXPORT_DIR {} is not a directory".format(export_dir))
    args.exportFile = "{}/resultPixelLevelSemanticLabeling.json".format(export_dir)
else:
    args.exportFile = os.path.join(args.cityscapesPath, "evaluationResults", "resultPixelLevelSemanticLabeling.json")
# Parameters that should be modified by user
args.groundTruthSearch  = os.path.join( args.cityscapesPath , "gtFine" , "val" , "*", "*_gtFine_labelIds.png" )

# Remaining params
args.evalInstLevelScore = True
args.evalPixelAccuracy  = False
args.evalLabels         = []
args.printRow           = 5
args.normalized         = True
args.colorized          = hasattr(sys.stderr, "isatty") and sys.stderr.isatty() and platform.system()=='Linux'
args.bold               = colors.BOLD if args.colorized else ""
args.nocol              = colors.ENDC if args.colorized else ""
args.JSONOutput         = True
args.quiet              = False

args.avgClassSize       = {
    "bicycle"    :  4672.3249222261 ,
    "caravan"    : 36771.8241758242 ,
    "motorcycle" :  6298.7200839748 ,
    "rider"      :  3930.4788056518 ,
    "bus"        : 35732.1511111111 ,
    "train"      : 67583.7075812274 ,
    "car"        : 12794.0202738185 ,
    "person"     :  3462.4756337644 ,
    "truck"      : 27855.1264367816 ,
    "trailer"    : 16926.9763313609 ,
}

# store some parameters for finding predictions in the args variable
# the values are filled when the method getPrediction is first called
args.predictionPath = None
args.predictionWalk = None


#########################
# Methods
#########################


# Generate empty confusion matrix and create list of relevant labels
def generateMatrix(args):
    return np.zeros(shape=(int(args.class_num), int(args.class_num)),dtype=np.ulonglong)

def generateInstanceStats(args):
    instanceStats = {}
    instanceStats["classes"] = {}

    for label in CamVidConstants.labelsLst:
        instanceStats["classes"][label['name']] = {}
        instanceStats["classes"][label['name']]["tp"] = 0.0
        instanceStats["classes"][label['name']]["tpWeighted"] = 0.0
        instanceStats["classes"][label['name']]["fn"] = 0.0
        instanceStats["classes"][label['name']]["fnWeighted"] = 0.0

    return instanceStats


# Get absolute or normalized value from field in confusion matrix.
def getMatrixFieldValue(confMatrix, i, j, args):
    if args.normalized:
        rowSum = confMatrix[i].sum()
        if (rowSum == 0):
            return float('nan')
        return float(confMatrix[i][j]) / rowSum
    else:
        return confMatrix[i][j]

# Calculate and return IOU score for a particular label
def getIouScoreForLabel(label, confMatrix, args):
    # the number of true positive pixels for this label
    # the entry on the diagonal of the confusion matrix
    tp = np.longlong(confMatrix[label,label])

    # the number of false negative pixels for this label
    # the row sum of the matching row in the confusion matrix
    # minus the diagonal entry
    fn = np.longlong(confMatrix[label,:].sum()) - tp

    # the number of false positive pixels for this labels
    # Only pixels that are not on a pixel with ground truth label that is ignored
    # The column sum of the corresponding column in the confusion matrix
    # without the ignored rows and without the actual label of interest
    notIgnored = [l for l in args.evalLabels if not l==label]
    fp = np.longlong(confMatrix[notIgnored,label].sum())

    # the denominator of the IOU score
    denom = (tp + fp + fn)
    if denom == 0:
        return float('nan')

    # return IOU
    return float(tp) / denom

# Calculate prior for a particular class id.
def getPrior(label, confMatrix):
    return float(confMatrix[label,:].sum()) / confMatrix.sum()

# Get average of scores.
# Only computes the average over valid entries.
def getScoreAverage(scoreList, args):
    validScores = 0
    scoreSum    = 0.0
    for score in scoreList:
        if not math.isnan(scoreList[score]):
            validScores += 1
            scoreSum += scoreList[score]
    if validScores == 0:
        return float('nan')
    return scoreSum / validScores

# create a dictionary containing all relevant results
def createResultDict( confMatrix, classScores, perImageStats, args ):
    # write JSON result file
    wholeData = {}
    wholeData["confMatrix"] = confMatrix.tolist()
    wholeData["priors"] = {}
    wholeData["labels"] = {}
    for label in args.evalLabels:
        name = CamVidConstants.labelsLst[label]['name']
        wholeData["priors"][name] = getPrior(label, confMatrix)
        wholeData["labels"][name] = label
    wholeData["classScores"] = classScores
    wholeData["averageScoreClasses"] = getScoreAverage(classScores, args)

    if perImageStats:
        wholeData["perImageScores"] = perImageStats

    return wholeData

def writeJSONFile(wholeData, args):
    path = os.path.dirname(args.exportFile)
    ensurePath(path)
    writeDict2JSON(wholeData, args.exportFile)

# Print confusion matrix
def printConfMatrix(confMatrix, args):
    # print line
    print("\b{text:{fill}>{width}}".format(width=15, fill='-', text=" "), end=' ')
    for _ in args.evalLabels:
        print("\b{text:{fill}>{width}}".format(width=args.printRow + 2, fill='-', text=" "), end=' ')
    print("\b{text:{fill}>{width}}".format(width=args.printRow + 3, fill='-', text=" "))

    # print label names
    print("\b{text:>{width}} |".format(width=13, text=""), end=' ')
    for label in args.evalLabels:
        print("\b{text:^{width}} |".format(width=args.printRow, text=CamVidConstants.labelsLst[label]['name']), end=' ')
    print("\b{text:>{width}} |".format(width=6, text="Prior"))

    # print line
    print("\b{text:{fill}>{width}}".format(width=15, fill='-', text=" "), end=' ')
    for _ in args.evalLabels:
        print("\b{text:{fill}>{width}}".format(width=args.printRow + 2, fill='-', text=" "), end=' ')
    print("\b{text:{fill}>{width}}".format(width=args.printRow + 3, fill='-', text=" "))

    # print matrix
    for x in range(0, confMatrix.shape[0]):
        if (not x in args.evalLabels):
            continue
        # get prior of this label
        prior = getPrior(x, confMatrix)
        # skip if label does not exist in ground truth
        if prior < 1e-9:
            continue

        # print name
        name = CamVidConstants.labelsLst[x]['name']
        if len(name) > 13:
            name = name[:13]
        print("\b{text:>{width}} |".format(width=13,text=name), end=' ')
        # print matrix content
        for y in range(0, len(confMatrix[x])):
            if (not y in args.evalLabels):
                continue
            matrixFieldValue = getMatrixFieldValue(confMatrix, x, y, args)
            print(getColorEntry(matrixFieldValue, args) + "\b{text:>{width}.2f}  ".format(width=args.printRow, text=matrixFieldValue) + args.nocol, end=' ')
        # print prior
        print(getColorEntry(prior, args) + "\b{text:>{width}.4f} ".format(width=6, text=prior) + args.nocol)
    # print line
    print("\b{text:{fill}>{width}}".format(width=15, fill='-', text=" "), end=' ')
    for label in args.evalLabels:
        print("\b{text:{fill}>{width}}".format(width=args.printRow + 2, fill='-', text=" "), end=' ')
    print("\b{text:{fill}>{width}}".format(width=args.printRow + 3, fill='-', text=" "), end=' ')

# Print intersection-over-union scores for all classes.
def printClassScores(scoreList, args):
    if (args.quiet):
        return
    print(args.bold + "classes          IoU" + args.nocol)
    print("--------------------------------")
    for label in args.evalLabels:
        labelName = CamVidConstants.labelsLst[label]['name']
        iouStr = getColorEntry(scoreList[labelName], args) + "{val:>5.3f}".format(val=scoreList[labelName]) + args.nocol
        print("{:<14}: ".format(labelName) + iouStr)

# Evaluate image lists pairwise.
def evaluateImgLists(predictionImgList, groundTruthImgList, args):
    if len(predictionImgList) != len(groundTruthImgList):
        printError("List of images for prediction and groundtruth are not of equal size.")
    confMatrix    = generateMatrix(args)
    instStats     = generateInstanceStats(args)
    perImageStats = {}
    nbPixels      = 0
    errorPixelsTotal = 0

    if not args.quiet:
        print("Evaluating {} pairs of images...".format(len(predictionImgList)))

    # Evaluate all pairs of images and save them into a matrix
    for i in range(len(predictionImgList)):
        predictionImgFileName = predictionImgList[i]
        groundTruthImgFileName = groundTruthImgList[i]
        #print "Evaluate ", predictionImgFileName, "<>", groundTruthImgFileName
        #showDiffOfPair(predictionImgFileName, groundTruthImgFileName, args)

        notIgnoredPixels, errorPixels = evaluatePair(predictionImgFileName, groundTruthImgFileName, confMatrix, instStats, perImageStats, args)
        nbPixels+=notIgnoredPixels
        errorPixelsTotal+=errorPixels

        # sanity check
        if confMatrix.sum() != nbPixels:
            printError('Number of analyzed pixels and entries in confusion matrix disagree: contMatrix {}, pixels {}'.format(confMatrix.sum(),nbPixels))

        if not args.quiet:
            print("\rImages Processed: {}".format(i+1), end=' ')
            sys.stdout.flush()
    if not args.quiet:
        print("\n")

    # sanity check
    if confMatrix.sum() != nbPixels:
        printError('Number of analyzed pixels and entries in confusion matrix disagree: contMatrix {}, pixels {}'.format(confMatrix.sum(),nbPixels))

    # print confusion matrix
    if (not args.quiet):
        printConfMatrix(confMatrix, args)

    # Calculate IOU scores on class level from matrix
    classScoreList = {}
    for label in args.evalLabels:
        labelName = CamVidConstants.labelsLst[label]['name']
        classScoreList[labelName] = getIouScoreForLabel(label, confMatrix, args)

    # Print IOU scores
    if (not args.quiet):
        print("")
        print("")
        printClassScores(classScoreList, args)
        iouAvgStr  = getColorEntry(getScoreAverage(classScoreList, args), args) + "{avg:5.3f}".format(avg=getScoreAverage(classScoreList, args)) + args.nocol
        print("--------------------------------")
        print("Score Average : " + iouAvgStr)
        print("--------------------------------")
        print("")
        print("--------------------------------")
        print("Pixels Accuracy : " + str(1.0-(errorPixelsTotal/float(nbPixels))))
        print("--------------------------------")
        print("")

    # write result file
    allResultsDict = createResultDict( confMatrix, classScoreList, perImageStats, args )
    writeJSONFile(allResultsDict, args)

    # return confusion matrix
    return allResultsDict

# Main evaluation method. Evaluates pairs of prediction and ground truth
# images which are passed as arguments.
def evaluatePair(predictionImgFileName, groundTruthImgFileName, confMatrix, instanceStats, perImageStats, args):
    # Loading all resources for evaluation.
    try:
        predictionImg = Image.open(predictionImgFileName)
        predictionNp  = np.array(predictionImg)
    except:
        printError("Unable to load " + predictionImgFileName)
    try:
        groundTruthImg = Image.open(groundTruthImgFileName)
        groundTruthNp = np.array(groundTruthImg)
    except:
        printError("Unable to load " + groundTruthImgFileName)

    # Check for equal image sizes
    if (predictionImg.size[0] != groundTruthImg.size[0]):
        printError("Image widths of " + predictionImgFileName + " and " + groundTruthImgFileName + " are not equal.")
    if (predictionImg.size[1] != groundTruthImg.size[1]):
        printError("Image heights of " + predictionImgFileName + " and " + groundTruthImgFileName + " are not equal.")
    if ( len(predictionNp.shape) != 2 ):
        printError("Predicted image has multiple channels.")

    imgWidth  = predictionImg.size[0]
    imgHeight = predictionImg.size[1]
    nbPixels  = 0#imgWidth*imgHeight

    # Evaluate images
    if (CSUPPORT):
        # using cython
        confMatrix = addToConfusionMatrix.cEvaluatePair(predictionNp, groundTruthNp, confMatrix, args.evalLabels)
    else:
        # the slower python way
        for (groundTruthImgPixel,predictionImgPixel) in izip(groundTruthImg.getdata(),predictionImg.getdata()):
            if groundTruthImgPixel<CamVidConstants.void_labels[0]:
                confMatrix[groundTruthImgPixel][predictionImgPixel] += 1
                nbPixels+=1

    #if args.evalPixelAccuracy:
    groundTruthNp = groundTruthNp.astype(np.int)
    predictionNp = predictionNp.astype(int)
    notIgnoredLabels = [l for l in args.evalLabels]
    notIgnoredPixels = np.in1d( groundTruthNp , notIgnoredLabels ).reshape(groundTruthNp.shape)# true means not ignored
    erroneousPixels = np.logical_and( notIgnoredPixels , ( predictionNp != groundTruthNp ) )
    perImageStats[predictionImgFileName] = {}
    perImageStats[predictionImgFileName]["notIgnoredPixels"] = np.count_nonzero(notIgnoredPixels)
    perImageStats[predictionImgFileName]["notCorrectPixels"]    = np.count_nonzero(erroneousPixels)#counts the error

    if args.produceColorSegmentationResults:
        colors = CamVidConstants.cmap
        lab_color = colorEncode(groundTruthNp, groundTruthNp, colors, args)
        pred_color = colorEncode(predictionNp, groundTruthNp, colors, args)
        # aggregate images and save
        im_vis = np.concatenate((lab_color, pred_color), axis=1).astype(np.uint8)
        imsave(os.path.join(args.export_dir_color_visual, ntpath.basename(predictionImgFileName)), im_vis)

    return nbPixels, np.count_nonzero(erroneousPixels)

def showDiffOfPair(predictionImgFileName, groundTruthImgFileName, args):
    # Loading all resources for evaluation.
    try:
        predictionImg = Image.open(predictionImgFileName)
        predictionNp  = np.array(predictionImg)
    except:
        printError("Unable to load " + predictionImgFileName)
    try:
        groundTruthImg = Image.open(groundTruthImgFileName)
        groundTruthNp = np.array(groundTruthImg)
    except:
        printError("Unable to load " + groundTruthImgFileName)

    groundTruthNp = groundTruthNp.astype(np.int)
    predictionNp = predictionNp.astype(int)

    notIgnoredLabels = [l for l in args.evalLabels]
    notIgnoredPixels = np.in1d(groundTruthNp, notIgnoredLabels).reshape(groundTruthNp.shape)  # true means not ignored
    erroneousPixels = np.logical_and(notIgnoredPixels, (predictionNp != groundTruthNp))

    diffResColors = erroneousPixels[:,:]*3
    diffResColors[erroneousPixels == True] == (np.uint8(255), np.uint8(255), np.uint8(255))
    diffResColors[erroneousPixels == False] == (np.uint8(0), np.uint8(0), np.uint8(0))

    import scipy.misc
    scipy.misc.imsave('/home/rabkinda/Documents/diff/diff.png', diffResColors)

# The main method
def main(args):
    list_sample = [x.rstrip().split(' ')[0] for x in open(args.test_filepath, 'r')]

    predictionImgList = [None] * len(list_sample)
    groundTruthImgList = [None] * len(list_sample)
    i=0

    for sample in list_sample:
        predictionImgList[i] = os.path.join(args.pred_dir, sample)
        groundTruthImgList[i] = os.path.join(args.gt_dir, sample)
        i+=1

    # evaluate
    evaluateImgLists(predictionImgList, groundTruthImgList, args)

    print(args.export_dir)
    return

# call the main method
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # Model related arguments
    parser.add_argument('--test_filepath')
    parser.add_argument('--pred_dir')
    parser.add_argument('--gt_dir')
    parser.add_argument('--images_dir')
    parser.add_argument('--class_num')
    parser.add_argument('--export_dir')
    parser.add_argument('--dataset')

    args = parser.parse_args()
    args.quiet = False
    args.evalPixelAccuracy = True
    args.printRow = 5
    args.normalized = True
    args.evalLabels = range(int(args.class_num))
    args.colorized = hasattr(sys.stderr, "isatty") and sys.stderr.isatty() and platform.system() == 'Linux'
    args.bold = colors.BOLD if args.colorized else ""
    args.nocol = colors.ENDC if args.colorized else ""
    args.JSONOutput = True
    args.exportFile = "{}/resultPixelLevelSemanticLabeling.json".format(args.export_dir)
    args.produceColorSegmentationResults = True
    args.export_dir_color_visual = os.path.join(args.export_dir, 'color_result')
    if not os.path.exists(args.export_dir_color_visual):
        os.makedirs(args.export_dir_color_visual)
    main(args)
