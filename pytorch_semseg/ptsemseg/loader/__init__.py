import json

from pytorch_semseg.ptsemseg.loader.pascal_voc_loader import pascalVOCLoader
from pytorch_semseg.ptsemseg.loader.camvid_loader import camvidLoader
from pytorch_semseg.ptsemseg.loader.ade20k_loader import ADE20KLoader
from pytorch_semseg.ptsemseg.loader.mit_sceneparsing_benchmark_loader import MITSceneParsingBenchmarkLoader
from pytorch_semseg.ptsemseg.loader.cityscapes_loader import cityscapesLoader
from pytorch_semseg.ptsemseg.loader.nyuv2_loader import NYUv2Loader
from pytorch_semseg.ptsemseg.loader.sunrgbd_loader import SUNRGBDLoader


def get_loader(name):
    """get_loader

    :param name:
    """
    return {
        'pascal': pascalVOCLoader,
        'camvid': camvidLoader,
        'ade20k': ADE20KLoader,
        'mit_sceneparsing_benchmark': MITSceneParsingBenchmarkLoader,
        'cityscapes': cityscapesLoader,
        'nyuv2': NYUv2Loader,
        'sunrgbd': SUNRGBDLoader,
    }[name]


def get_data_path(name, config_file='config.json'):
    """get_data_path

    :param name:
    :param config_file:
    """
    data = json.load(open(config_file))
    return data[name]['data_path']
