non_void_nclasses = 11
void_labels = [11]

# optional arguments
mean = [0.485, 0.456, 0.406]#[0.39068785, 0.40521392, 0.41434407] #mean=[0.485, 0.456, 0.406], for resnet
std = [0.229, 0.224, 0.225]#[0.29652068, 0.30514979, 0.30080369] #std=[0.229, 0.224, 0.225]

crop_size = 720

img_original_size = (720, 960)

labelsLst = [
    {'id': 0, 'name': 'sky', 'color':(128, 128, 128), 'freq':0.1893246298},
    {'id': 1, 'name': 'building', 'color':(128, 0, 0), 'freq':0.2545827605},
    {'id': 2, 'name': 'sidewalk', 'color':(0, 0, 192), 'freq':0.0436168662},
    {'id': 3, 'name': 'column_pole', 'color':(192, 192, 128), 'freq':0.0110244563},
    {'id': 4, 'name': 'road', 'color':(128, 64, 128), 'freq':0.3322981368},
    {'id': 5, 'name': 'Tree', 'color':(128, 128, 0), 'freq':0.0988898144},
    {'id': 6, 'name': 'SignSymbol', 'color':(192, 128, 128), 'freq':0.0007279103},
    {'id': 7, 'name': 'Fence', 'color':(64, 64, 128), 'freq':0.0126420391},
    {'id': 8, 'name': 'Car', 'color':(64, 0, 128), 'freq':0.0466687414},
    {'id': 9, 'name': 'Pedestrian', 'color':(64, 64, 0), 'freq':0.0071700382},
    {'id': 10, 'name': 'Bicyclist', 'color':(0, 128, 192), 'freq':0.0030546071}
]

cmap = {
    0: (128, 128, 128),    # sky
    1: (128, 0, 0),        # building
    2: (0, 0, 192),        # sidewalk
    3: (192, 192, 128),    # column_pole
    4: (128, 64, 128),     # road
    5: (128, 128, 0),      # Tree
    6: (192, 128, 128),    # SignSymbol
    7: (64, 64, 128),      # Fence
    8: (64, 0, 128),       # Car
    9: (64, 64, 0),        # Pedestrian
    10: (0, 128, 192),     # Bicyclist
    11: (0, 0, 0)          # unlabeled
 }

freqMap = {
    0: 0.1893246298,    # sky
    1: 0.2545827605,        # building
    2: 0.0436168662,        # sidewalk
    3: 0.0110244563,    # column_pole
    4: 0.3322981368,     # road
    5: 0.0988898144,      # Tree
    6: 0.0007279103,    # SignSymbol
    7: 0.0126420391,      # Fence
    8: 0.0466687414,       # Car
    9: 0.0071700382,        # Pedestrian
    10: 0.0030546071,     # Bicyclist
 }

palette = [128, 128, 128,
           128, 0, 0,
           0, 0, 192,
           192, 192, 128,
           128, 64, 128,
           128, 128, 0,
           192, 128, 128,
           64, 64, 128,
           64, 0, 128,
           64, 64, 0,
           0, 128, 192,
           0, 0, 0]