import torch
import numpy as np

from framesBuffer.datasets.DatasetFactory import DatasetFactory

import os
import PIL as pil


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.initialized = False
        self.val = None
        self.avg = None
        self.sum = None
        self.count = None

    def initialize(self, val, weight):
        self.val = val
        self.avg = val
        self.sum = val * weight
        self.count = weight
        self.initialized = True

    def update(self, val, weight=1):
        if not self.initialized:
            self.initialize(val, weight)
        else:
            self.add(val, weight)

    def add(self, val, weight):
        self.val = val
        self.sum += val * weight
        self.count += weight
        self.avg = self.sum / (self.count+1e-10)

    def value(self):
        return self.val

    def average(self):
        return self.avg


def unique(ar, return_index=False, return_inverse=False, return_counts=False):
    ar = np.asanyarray(ar).flatten()

    optional_indices = return_index or return_inverse
    optional_returns = optional_indices or return_counts

    if ar.size == 0:
        if not optional_returns:
            ret = ar
        else:
            ret = (ar,)
            if return_index:
                ret += (np.empty(0, np.bool),)
            if return_inverse:
                ret += (np.empty(0, np.bool),)
            if return_counts:
                ret += (np.empty(0, np.intp),)
        return ret
    if optional_indices:
        perm = ar.argsort(kind='mergesort' if return_index else 'quicksort')
        aux = ar[perm]
    else:
        ar.sort()
        aux = ar
    flag = np.concatenate(([True], aux[1:] != aux[:-1]))

    if not optional_returns:
        ret = aux[flag]
    else:
        ret = (aux[flag],)
        if return_index:
            ret += (perm[flag],)
        if return_inverse:
            iflag = np.cumsum(flag) - 1#TODO
            inv_idx = np.empty(ar.shape, dtype=np.intp)
            inv_idx[perm] = iflag
            ret += (inv_idx,)
        if return_counts:
            idx = np.concatenate(np.nonzero(flag) + ([ar.size],))
            ret += (np.diff(idx),)
    return ret


# def colorEncode(labelmap, colors):
#     labelmap = labelmap.astype('int')
#     labelmap_rgb = np.zeros((labelmap.shape[0], labelmap.shape[1], 3),
#                             dtype=np.uint8)
#     for label in unique(labelmap):
#         if label < 0:
#             continue
#         # if label in CamVidConstants.void_labels:
#         #     continue
#         labelmap_rgb += (labelmap == label)[:, :, np.newaxis] * \
#             np.tile(colors[label],
#                     (labelmap.shape[0], labelmap.shape[1], 1))
#     return labelmap_rgb


def colorEncode(labelmap, groundtruth_labelmap, colors, args):
    for key, value in colors.items():
        colors[key] = (np.uint8(colors[key][0]), np.uint8(colors[key][1]), np.uint8(colors[key][2]))

    labelmap = labelmap.astype('int')
    labelmap_rgb = np.zeros((labelmap.shape[0], labelmap.shape[1], 3),
                            dtype=np.uint8)

    for label in unique(labelmap):
        if label < 0:
            continue
        if label in getDatasetConstants(args).void_labels:
            continue
        labelmap_rgb += (labelmap == label)[:, :, np.newaxis] * \
            np.tile(colors[label],
                    (labelmap.shape[0], labelmap.shape[1], 1))

    voidIndicesList = np.argwhere(groundtruth_labelmap==getDatasetConstants(args).void_labels[0])
    for xInd, yInd in voidIndicesList:
        labelmap_rgb[xInd, yInd, :] = [np.uint8(0), np.uint8(0), np.uint8(0)]

    return labelmap_rgb


def markUnlabeledPixelsInPrediction(batch_data, preds):
    (imgs, segs, infos) = batch_data
    indBatchArr, indXArr, indYArr = np.where(segs < 0)

    for i in xrange(0, indBatchArr.size, 1):
        preds[indBatchArr[i], indXArr[i], indYArr[i]] = -1

    return preds


def accuracy(batch_data, pred, args):
    (imgs, segs, infos) = batch_data
    _, preds = torch.max(pred.data.cpu(), dim=1)
    valid = (segs>=0) & (segs < getDatasetConstants(args).void_labels[0])
    acc = 1.0 * torch.sum(valid * (preds == segs)) / (torch.sum(valid) + 1e-10)
    return acc, torch.sum(valid)


def intersectionAndUnion(batch_data, pred, numClass, args):
    (imgs, segs, infos) = batch_data
    _, preds = torch.max(pred.data.cpu(), dim=1)

    # compute area intersection
    intersect = preds.clone()
    intersect[torch.ne(preds, segs)] = -1

    area_intersect = torch.histc(intersect.float(),
                                 bins=numClass,
                                 min=0,
                                 max=numClass-1)

    # compute area union:
    preds[torch.eq(segs, getDatasetConstants(args).void_labels[0])] = -1
    area_pred = torch.histc(preds.float(),
                            bins=numClass,
                            min=0,
                            max=numClass-1)
    area_lab = torch.histc(segs.float(),
                           bins=numClass,
                           min=0,
                           max=numClass-1)
    area_union = area_pred + area_lab - area_intersect
    return area_intersect, area_union


def savePrediction(infos, pred, args):

    for j in range(len(infos)):
        # prediction
        _, preds = torch.max(pred.data.cpu()[j], dim=0)

        path = os.path.join(args.root_pred, os.path.dirname(infos[j]))
        if not os.path.exists(path):
            os.makedirs(path)

        pil.Image.fromarray(preds.numpy().astype('uint8')).save(os.path.join(args.root_pred, infos[j]))


def getDatasetConstants(opt):
    return DatasetFactory.getConstants(opt.dataset)


# def test_images_accuracy(model, raw_img_folder, label_folder, label_colors, size):
#     raw_imgs_loc = list(glob.glob(raw_img_folder + "/*.png"))
#     label_len = len(label_colors)
#     iou = np.zeros((label_len))
#     mean_acc = 0
#     for i, img_loc in enumerate(raw_imgs_loc):
#         print(i, ":caculate accuracy of image:", img_loc)
#         segmap = to_segmap(model, img_loc, label_colors, size)
#         label = pil.Image.open(label_folder + "/" + img_loc.split("/")[-1][0:-4] + "_L.png")
#         label = np.array(label)
#         mean_acc += (segmap == label).mean()
#         for i, color in enumerate(label_colors):
#             real_mask = label[:, :, ] == color
#             predict_mask = segmap[:, :, ] == color
#
#
#             true_intersect_mask = (real_mask & predict_mask)
#
#             TP = true_intersect_mask.sum()  # true positive
#             FP = predict_mask.sum() - TP  # false positive
#             FN = real_mask.sum() - TP  # false negative
#             # print("TP, FP, FN", TP, FP, FN)
#             iou[i] += TP / float(TP + FP + FN)
#
#     return iou / len(raw_imgs_loc), mean_acc / len(raw_imgs_loc)