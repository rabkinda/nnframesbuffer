# System libs
import argparse
import datetime
# Our libs
import math
# Our libs
import os

# Numerical libs
import numpy as np
from scipy.ndimage import zoom
import torch
import torch.nn as nn
from scipy.misc import imsave
from torch.autograd import Variable
from tqdm import tqdm

from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.semanticSegmentation.utils import save_reindexed_colored_image, apply_flip
from pytorch_semseg.ptsemseg.models import get_model
from semanticSegmentationPytorchFineTuned.segmentation_dataset_for_caffe_pspnet import SegmentationDataset
from utils import AverageMeter, colorEncode, accuracy, intersectionAndUnion, savePrediction


# forward func for evaluation
def forward_multiscale(nets, batch_data, args, include_flip=True):

    dataset_constants = DatasetFactory.getConstants(args.dataset)
    (net, crit) = nets
    (imgs, segs, infos) = batch_data

    batch_size, _, height, width = imgs.shape
    zeros = torch.zeros((batch_size, args.num_class, height, width))
    pred = Variable(zeros, volatile=True).cuda()

    for scale in args.scales:

        imgs_scale = zoom(imgs.numpy(),
                          (1., 1., scale, scale),
                          order=1,
                          prefilter=False,
                          mode='nearest')

        if args.compute_by_crops:
            CROP_SIZE = dataset_constants.crop_size
            height_resize, width_resize = imgs_scale.shape[2:]

            if height_resize < CROP_SIZE:
                imgs_scale = np.lib.pad(imgs_scale, ((0, 0), (0, 0), (0, CROP_SIZE - height_resize), (0, 0)), 'constant', constant_values=0)
            if width_resize < CROP_SIZE:
                imgs_scale = np.lib.pad(imgs_scale, ((0, 0), (0, 0), (0, 0), (0, CROP_SIZE - width_resize)), 'constant', constant_values=0)

        # feed input data
        input_img = Variable(torch.from_numpy(imgs_scale), volatile=True).cuda()

        if args.compute_by_crops:
            STRIDE = int(math.ceil(CROP_SIZE / 1.5))#476 # 476=720/1.5
            scale_height, scale_width = input_img.size()[2:]
            h_grid = int(np.ceil((scale_height - CROP_SIZE) / (STRIDE * 1.0)) + 1)
            w_grid = int(np.ceil((scale_width - CROP_SIZE) / (STRIDE * 1.0)) + 1)
            pred_scale = Variable(torch.zeros((batch_size, args.num_class, scale_height, scale_width)), volatile=True).cuda()
            cnt_scale = Variable(torch.zeros((batch_size, args.num_class, scale_height, scale_width)), volatile=True).cuda()

            for grid_yidx in range(0, h_grid):
                for grid_xidx in range(0, w_grid):
                    s_x = (grid_xidx) * STRIDE + 1
                    s_y = (grid_yidx) * STRIDE + 1
                    e_x = np.min([s_x + CROP_SIZE - 1, scale_width])
                    e_y = np.min([s_y + CROP_SIZE - 1, scale_height])
                    s_x = e_x - CROP_SIZE + 1 - 1
                    s_y = e_y - CROP_SIZE + 1 - 1

                    # forward pass
                    input_img_slice = input_img[:, :, s_y:e_y, s_x:e_x]
                    curr_pred = net(input_img_slice, segSize=(CROP_SIZE, CROP_SIZE))

                    if include_flip:
                        input_img_slice_flipped = apply_flip(input_img_slice, dim=3)
                        curr_pred_from_flipped = net(input_img_slice_flipped, segSize=(CROP_SIZE, CROP_SIZE))
                        curr_pred_from_flipped = apply_flip(curr_pred_from_flipped, dim=3)
                        curr_pred = (curr_pred+curr_pred_from_flipped)/2.0

                    curr_pred = nn.functional.softmax(curr_pred, dim=1)
                    cnt_scale[:, :, s_y:e_y, s_x:e_x] = cnt_scale[:, :, s_y:e_y, s_x:e_x] + 1
                    pred_scale[:, :, s_y:e_y, s_x:e_x] = pred_scale[:, :, s_y:e_y, s_x:e_x] + curr_pred

            pred_scale = pred_scale / cnt_scale
            pred_scale = pred_scale[:, :, :height_resize, :width_resize]
            pred_scale = nn.functional.upsample(pred_scale, size=(height, width), mode='bilinear')
        else:
            pred_scale = net(input_img, segSize=(height, width))

            if include_flip:
                input_img_flipped = apply_flip(input_img, dim=3)
                pred_scale_from_flipped = net(input_img_flipped, segSize=(height, width))
                pred_scale_from_flipped = apply_flip(pred_scale_from_flipped, dim=3)
                pred_scale = (pred_scale + pred_scale_from_flipped) / 2.0

            pred_scale = nn.functional.softmax(pred_scale, dim=1)

        # average the probability
        pred = pred + (pred_scale / len(args.scales))

    pred = torch.log(pred)
    label_seg = Variable(segs, volatile=True).cuda()
    err = crit(pred, label_seg)
    return pred, err


def visualize_result(batch_data, pred, args):
    dataset_constants=DatasetFactory.getConstants(args.dataset)
    colors = dataset_constants.cmap
    (imgs, segs, infos) = batch_data
    for j in range(len(infos)):
        # get/recover image
        # img = imread(os.path.join(args.root_img, infos[j]))
        img = imgs[j].clone()
        for t, m, s in zip(img,
                           dataset_constants.mean,
                           dataset_constants.std):
                           #[0.485, 0.456, 0.406],
                           #[0.229, 0.224, 0.225]):
            t.mul_(s).add_(m)
        img = (img.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8)

        # segmentation
        lab = segs[j].numpy()
        lab_color = colorEncode(lab, lab, colors, args)

        # prediction
        pred_ = np.argmax(pred.data.cpu()[j].numpy(), axis=0)
        pred_color = colorEncode(pred_, lab, colors, args)

        # aggregate images and save
        im_vis = np.concatenate((img, lab_color, pred_color),
                                axis=1).astype(np.uint8)
        imsave(os.path.join(args.result,
                            infos[j].replace('/', '_')
                            .replace('.jpg', '.png')), im_vis)


def evaluate(nets, loader, args):
    loss_meter = AverageMeter()
    acc_meter = AverageMeter()
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()

    # switch to eval mode
    for net in nets:
        net.eval()

    dataset_constants = DatasetFactory.getConstants(args.dataset)

    for i, batch_data in enumerate(tqdm(loader)):
                # forward pass
        pred, err = forward_multiscale(nets, batch_data, args, args.include_flip)
        loss_meter.update(err.data[0])

        (imgs, segs, infos) = batch_data
        savePrediction(infos, pred, args)

        # for j in range(len(infos)):
        #     save_reindexed_colored_image(args, dataset_constants, infos[j], pred[j], segs[j])

        if args.calculateAccIou:
            # calculate accuracy
            acc, pix = accuracy(batch_data, pred, args)
            intersection, union = intersectionAndUnion(batch_data, pred,
                                                       args.num_class, args)
            acc_meter.update(acc, pix)
            intersection_meter.update(intersection)
            union_meter.update(union)

            print('[{}] iter {}, loss: {}, accuracy: {}'
                  .format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                          i, err.data[0], acc))
        else:
            print('[{}] iter {}, loss: {}'
                  .format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                          i, err.data[0]))

        # visualization
        # if args.visualize:
        #     visualize_result(batch_data, pred, args)

    if args.calculateAccIou:
        iou = intersection_meter.sum / (union_meter.sum + 1e-10)
        for i, _iou in enumerate(iou):
            print('class [{}], IoU: {}'.format(i, _iou))

        iouForMean = iou[union_meter.sum > 0]
        if len(iouForMean) > 0:
            print('[Eval Summary]:')
            print('Loss: {}, Mean IoU: {:.4}, Accurarcy: {:.2f}%'
                  .format(loss_meter.average(), iouForMean.mean(), acc_meter.average()*100))


def main(args):
    # Network Builders
    net = get_model('pspnet', args.num_class, version=args.dataset)#use_softmax=True
    caffemodel_path = args.checkpoint
    net.load_pretrained_model(model_path=caffemodel_path)
    net.float()

    dataset_constants = DatasetFactory.getConstants(args.dataset)

    #label_colors = utility.read_color_integer(os.path.join(args.data_folder, "label_integer.txt"))

    # build a dictionary with key = (r,g,b), value = percentage of color
    #color_info = utility.read_color_count_sorted(os.path.join(args.data_folder, 'color_count_train_sorted.txt'))
    #color_info = {(data[0], data[1], data[2]): data[4] for data in color_info}

    # build the weights matrix according to the formula 1/ln(1.02 + w)
    weights = None
    if args.weighting:
        print("set weights")
        weights = []

        for labelInd in xrange(0, len(dataset_constants.labelsLst), 1):
            w = dataset_constants.labelsLst[labelInd]['freq']
            w = 1 / math.log(1.02 + w, 2)
            weights.append(w)

        weights = np.array(weights)
        weights = torch.from_numpy(weights.astype('float32')).cuda()
    else:
        print("zero weights")

    crit = nn.NLLLoss2d(weight=weights, ignore_index=dataset_constants.void_labels[0])#-1)

    # Dataset and Loader
    dataset_val = SegmentationDataset(args.list_val, args, max_sample=args.num_val, is_train=0)
    loader_val = torch.utils.data.DataLoader(
        dataset_val,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=5,
        drop_last=False)

    nets = (net, crit)
    for net in nets:
        net.cuda()

    # Main loop
    evaluate(nets, loader_val, args)

    print('Evaluation Done!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Model related arguments
    parser.add_argument('--id', required=True,
                        help="a name for identifying the model to load")
    parser.add_argument('--checkpoint', default='checkpoint',
                        help="checkpoint")

    # Path related arguments
    parser.add_argument('--list_val',
                        default='test.txt')
    parser.add_argument('--root_img',
                        default='leftImg8bit_sequence')
    parser.add_argument('--root_seg',
                        default='gt')
    parser.add_argument('--root_pred', default='predictions')
    parser.add_argument('--root_reindexed_pred', default='reindexed_predictions')
    parser.add_argument('--root_colored_pred', default='colored_predictions')
    parser.add_argument('--data_folder', default='data/ADEChallengeData2016')

    # Data related arguments
    parser.add_argument('--num_val', default=-1, type=int,
                        help='number of images to evalutate')
    parser.add_argument('--num_class', default=19, type=int,
                        help='number of classes')
    parser.add_argument('--batch_size', default=1, type=int,
                        help='batchsize')
    parser.add_argument('--imgSize', default=-1, type=int,
                        help='input image size, -1 = keep original')
    parser.add_argument('--segSize', default=-1, type=int,
                        help='output image size, -1 = keep original')

    parser.add_argument('--weighting', type=int, default=0,
                        help='True will adopt weights on loss function and vice versa')

    # Misc arguments
    parser.add_argument('--visualize', default=1,
                        help='output visualization?')
    parser.add_argument('--result', default='./result',
                        help='folder to output visualization results')
    parser.add_argument('--multi_scale', type=int, default=0,
                        help='multi_scale')
    parser.add_argument('--full_multi_scale', type=int, default=0,
                        help='full_multi_scale')
    parser.add_argument('--compute_by_crops', type=int, default=1,
                        help='compute_by_crops')
    parser.add_argument('--crop_size', type=int, default=713, help='crop_size')
    parser.add_argument('--dataset', type=str, default='cityscapes', help='dataset')
    parser.add_argument('--calculateAccIou', type=int, default=1, help='calculateAccIou')
    parser.add_argument('--gtExists', type=int, default=1, help='gtExists')
    parser.add_argument('--include_flip', type=int, default=1, help='include_flip')

    args = parser.parse_args()
    print(args)

    # scales for evaluation
    args.scales = (1,)
    if args.multi_scale==1:
        args.scales = (0.5, 0.75, 1.0, 1.25, 1.5)#, 1.75)

    if args.full_multi_scale:
        args.scales = (0.5, 0.75, 1.0, 1.25, 1.5, 1.75)


    args.result = os.path.join(args.result, args.id)
    if not os.path.isdir(args.result):
        os.makedirs(args.result)

    main(args)
