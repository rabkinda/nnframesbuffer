import os
import torch
from scipy.ndimage import zoom
from torch.autograd import Variable
from models import ModelBuilder
from pytorch_semseg.ptsemseg.models import get_model


# forward func for evaluation
def forward(nets, imgs, args, segSize):
    # feed input data
    input_img = Variable(imgs, volatile=True)
    if args['use_cuda']:
        input_img = input_img.cuda()

    # forward
    if args['use_official_pspnet']:
        (net,) = nets
        psp_out_descriptor_final = net(input_img, segSize=segSize)
    else:
        (net_encoder, net_decoder) = nets
        psp_out_descriptor_final = net_decoder(x=net_encoder(input_img), segSize=segSize)

    return psp_out_descriptor_final


# forward func for evaluation
def forward_multiscale(nets, imgs, args, segSize):
    (net_encoder, net_decoder) = nets

    psp_out_descriptor_final = torch.zeros(imgs.size(0), args['num_class'], segSize[0], segSize[1])
    psp_out_descriptor_final = Variable(psp_out_descriptor_final, volatile=True).cuda()

    for scale in args['scales']:
        imgs_scale = zoom(imgs.numpy(),
                          (1., 1., scale, scale),
                          order=1,
                          prefilter=False,
                          mode='nearest')

        # feed input data
        input_img = Variable(torch.from_numpy(imgs_scale), volatile=True).cuda()

        # forward
        pred_scale = net_decoder(net_encoder(input_img), segSize=segSize)

        # average the probability
        psp_out_descriptor_final = psp_out_descriptor_final + (pred_scale / len(args['scales']))

    return psp_out_descriptor_final


def evaluate(nets, imgs, args, segSize):
    _segSize = segSize
    if args['use_descriptor_div_8']:
        _segSize = (_segSize[0]/8, _segSize[1]/8)

    # forward pass
    if args['multi_scale'] and (not args['use_official_pspnet']):
        middleDescriptor = forward_multiscale(nets, imgs, args, _segSize)
    else:
        middleDescriptor = forward(nets, imgs, args, _segSize)

    return middleDescriptor


def evaluateBatch(nets, batch_data, args, segSize):
    # Main loop
    middleDescriptor = evaluate(nets, batch_data, args, segSize)
    return middleDescriptor

def loadInitialSegmentationModel(args):
    if args['use_official_pspnet']:
        net = get_model('pspnet', args['num_class'], version=args['dataset'])
        caffemodel_path = args['official_pspnet_checkpoint']
        net.load_pretrained_model(model_path=caffemodel_path)
        net.float()
        nets = (net,)
    else:
        args['suffix'] = '_best.pth'

        # absolute paths of model weights
        args['weights_encoder'] = os.path.join(args['ckpt'], args['id'],
                                            'encoder' + args['suffix'])
        args['weights_decoder'] = os.path.join(args['ckpt'], args['id'],
                                            'decoder' + args['suffix'])

        # Network Builders
        builder = ModelBuilder()
        net_encoder = builder.build_encoder(arch=args['arch_encoder'],
                                            fc_dim=args['fc_dim'],
                                            weights=args['weights_encoder'])
        net_decoder = builder.build_decoder(arch=args['arch_decoder'],
                                            fc_dim=args['fc_dim'],
                                            segSize=args['segSize'],
                                            weights=args['weights_decoder'],
                                            num_class=args['num_class'],
                                            use_softmax=args['state_with_softmax'])

        nets = (net_encoder, net_decoder)

    if args['use_cuda']:
        for net in nets:
            net.cuda()

    # switch to eval mode
    for net in nets:
        net.eval()

    return nets
