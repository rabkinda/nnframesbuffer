import numpy as np


non_void_nclasses = 19
void_labels = [255]#also label should be >= 0 and < 255

# optional arguments
mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]

crop_size = 713

mean_caffe = [123.68, 116.779, 103.939]

img_original_size = (1024, 2048)

instancePixelsNum = {
    'road': 2036416525,
    'building': 1260636120,
    'vegetation': 879783988,
    'car': 386328286,
    'sidewalk': 336090793,
    'sky': 221979646,
    'pole': 68289378,
    'person': 67326424,
    'terrain': 63949536,
    'fence': 48454166,
    'wall': 36199498,
    'traffic sign': 30448193,
    'bicycle': 22861233,
    'truck': 14772328,
    'bus': 12990290,
    'train': 12863955,
    'traffic light': 11477088,
    'rider': 7463162,
    'motorcycle': 5449152
}

totalPixels = np.sum([instancePixelsNum[k] for k in instancePixelsNum.keys()])

labelsLst = [
    {'name': 'road'          ,   'id':     0 , 'category': 'flat'            , 'categoryId': 1  , 'color': (128, 64,128) },
    {'name': 'sidewalk'      ,   'id':     1 , 'category': 'flat'            , 'categoryId': 1  , 'color': (244, 35,232) },
    {'name': 'building'      ,   'id':     2 , 'category': 'construction'    , 'categoryId': 2  , 'color': ( 70, 70, 70) },
    {'name': 'wall'          ,   'id':     3 , 'category': 'construction'    , 'categoryId': 2  , 'color': (102,102,156) },
    {'name': 'fence'         ,   'id':     4 , 'category': 'construction'    , 'categoryId': 2  , 'color': (190,153,153) },
    {'name': 'pole'          ,   'id':     5 , 'category': 'object'          , 'categoryId': 3  , 'color': (153,153,153) },
    {'name': 'traffic light' ,   'id':     6 , 'category': 'object'          , 'categoryId': 3  , 'color': (250,170, 30) },
    {'name': 'traffic sign'  ,   'id':     7 , 'category': 'object'          , 'categoryId': 3  , 'color': (220,220,  0) },
    {'name': 'vegetation'    ,   'id':     8 , 'category': 'nature'          , 'categoryId': 4  , 'color': (107,142, 35) },
    {'name': 'terrain'       ,   'id':     9 , 'category': 'nature'          , 'categoryId': 4  , 'color': (152,251,152) },
    {'name': 'sky'           ,   'id':    10 , 'category': 'sky'             , 'categoryId': 5  , 'color': ( 70,130,180) },
    {'name': 'person'        ,   'id':    11 , 'category': 'human'           , 'categoryId': 6  , 'color': (220, 20, 60) },
    {'name': 'rider'         ,   'id':    12 , 'category': 'human'           , 'categoryId': 6  , 'color': (255,  0,  0) },
    {'name': 'car'           ,   'id':    13 , 'category': 'vehicle'         , 'categoryId': 7  , 'color': (  0,  0,142) },
    {'name': 'truck'         ,   'id':    14 , 'category': 'vehicle'         , 'categoryId': 7  , 'color': (  0,  0, 70) },
    {'name': 'bus'           ,   'id':    15 , 'category': 'vehicle'         , 'categoryId': 7  , 'color': (  0, 60,100) },
    {'name': 'train'         ,   'id':    16 , 'category': 'vehicle'         , 'categoryId': 7  , 'color': (  0, 80,100) },
    {'name': 'motorcycle'    ,   'id':    17 , 'category': 'vehicle'         , 'categoryId': 7  , 'color': (  0,  0,230) },
    {'name': 'bicycle'       ,   'id':    18 , 'category': 'vehicle'         , 'categoryId': 7  , 'color': (119, 11, 32) }
]

for label in labelsLst:
    label['freq'] = (1.0*instancePixelsNum[label['name']])/totalPixels

cmap = {
    0: (128, 64,128),
    1: (244, 35,232),
    2: ( 70, 70, 70),
    3: (102,102,156),
    4: (190,153,153),
    5: (153,153,153),
    6: (250,170, 30),
    7: (220,220,  0),
    8: (107,142, 35),
    9: (152,251,152),
    10: ( 70,130,180),
    11: (220, 20, 60),
    12: (255,  0,  0),
    13: (  0,  0,142),
    14: (  0,  0, 70),
    15: (  0, 60,100),
    16: (  0, 80,100),
    17: (  0,  0,230),
    18: (119, 11, 32),
    19: (0, 0, 0)
}

cIds = {0:7, 1:8, 2:11, 3:12, 4:13, 5:17, 6:19, 7:20, 8:21, 9:22, 10:23,
        11:24, 12:25, 13:26, 14:27, 15:28, 16:31, 17:32, 18:33, 19:0}

palette = [128,64,128,
           244,35,232,
           70,70,70,
           102,102,156,
           190,153,153,
           153,153,153,
           250,170,30,
           220,220,0,
           107,142,35,
           152,251,152,
           70,130,180,
           220,20,60,
           255,0,0,
           0,0,142,
           0,0,70,
           0,60,100,
           0,80,100,
           0,0,230,
           119,11,32,
           0,0,0]

#print(labelsLst)