import argparse

import sys
sys.path.append('loader/')
import blogCodes.python_deep_learning.segmentation.loader.utility as utility
import glob
import numpy as np
import PIL as pil
from cityscapesScripts.cityscapesscripts.helpers.labels import labels


parser = argparse.ArgumentParser(description = 'Hyper params')
parser.add_argument('--cityscapes_folder', type = str, default = "/media/rabkinda/DATA/cityscapes/gtFine_trainvaltest/",
                    help = 'This folder should contains the files and folders clone from https://github.com/mostafaizz/camvid')
args = vars(parser.parse_args())
cityscapes_folder = args['cityscapes_folder']


def write_color_count_sorted(save_as, color_count, total):
    """write color count will sort the color count and save it into a file with the format
    "r g b occur_number occur_probability label_of_the_color". The resutls will
    be sorted by occur number of the pixel value.

    Args:
        save_as(string): self explained
        color_count(dictionary): A dictionary with key == (r,g,b), value = occur number of pixel
        color_table(dicitonary): A dictionary with key == (r,g,b), value = label(name of object like car, bus etc) of color
        total(int,float): Total number of the pixels of the dataset

    Example:

    #read label color read the label color info from camvid dataset,it is a key with format
    #key : (r,g,b), value : occur number of pixel
    color_table = camvid_loader.read_label_color("camvid/label_colors.txt")
    color_count, total = count_imgs_pix("camvid/LabeledApproved_full")
    write_color_count_sorted("count_color.txt", color_count, color_table, total)
    """
    with open(save_as, "w") as f:
        cc = [[k[0], k[1], k[2], v] for k, v in color_count.items()]
        cc = sorted(cc, key=lambda color: color[3], reverse=True)
        for k in cc:
            #if (k[0], k[1], k[2]) in color_table:
            # r, g, b, occur number, percentage, name
            f.write("{:3d} {:3d} {:3d} {:15d} {:10f} {}\n".format(k[0], k[1], k[2], k[3], k[3] / total,
                                                                 [label.name for label in labels if label.color==(k[0], k[1], k[2])][0]))


def count_imgs_pix(img_folder, label_colors):
    """Count pixel number in label_colors of the images

    Args:
        img_folder(string) : self explained
        label_colors(list) : A list with (r,g,b) values want to count

    Example:

    label_colors = utility.read_color_integer(camvid_folder + 'label_integer.txt')
    #color_count is a dictionary with key == (r,g,b), value = occur number of pixel
    #total is the total number of the pixels
    color_count, total = count_imgs_pix("camvid/LabeledApproved_full", label_colors)
    """
    color_count = {}
    imgs_name = list(glob.glob(img_folder + "**/*_gtFine_color.png"))
    # imgs_name = imgs_name[0:10]
    total = 0
    for i, name in enumerate(imgs_name):
        print(i, ":", name)
        img = pil.Image.open(name)
        img = np.array(img)
        color_count, pix_num = utility.count_img_pix(img, color_count, label_colors)
        print("pix num:", pix_num)
        total = total + pix_num

    print("total:", total)
    sum_percentage = 0
    for k, v in color_count.items():
        print("percentage:", v / total, "pix num:", v)
        sum_percentage += v / total
    print("sum percentage:", sum_percentage)
    return color_count, total


color_count, total = count_imgs_pix(cityscapes_folder + 'train/', None)
write_color_count_sorted(cityscapes_folder + 'color_count_train_sorted.txt', color_count, total)


