import os
import random
import re

import numpy as np
import torch
import torch.utils.data as torchdata
from scipy.misc import imread, imresize
from torchvision import transforms

from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.datasets.FilenameUtils import getFilename


class SegmentationDataset(torchdata.Dataset):
    def __init__(self, txt, opt, max_sample=-1, is_train=1):
        self.dataset_constants = DatasetFactory.getConstants(opt.dataset)
        self.root_img = opt.root_img
        self.root_seg = opt.root_seg
        self.imgSize = opt.imgSize
        self.segSize = opt.segSize
        self.is_train = is_train
        self.curr_prev_ind_gap = opt.curr_prev_ind_gap

        # mean and std
        self.img_transform = transforms.Compose([
            transforms.Normalize(mean=self.dataset_constants.mean,
                                 std=self.dataset_constants.std)])

        self.list_sample = [x.rstrip().split(' ')[0] for x in open(txt, 'r')]

        self.categoriesMap = {}

        for s in self.list_sample:
            catagory = s.split(os.sep)[1]
            if not catagory in self.categoriesMap:
                self.categoriesMap[catagory] = []
            self.categoriesMap[catagory].append(s)

        if self.is_train:
            random.shuffle(self.list_sample)
        if max_sample > 0:
            self.list_sample = self.list_sample[0:max_sample]
        num_sample = len(self.list_sample)
        assert num_sample > 0
        print('# samples: {}'.format(num_sample))

        self.imgOriginalShape = (720, 960)
        self.args = opt


    def _flip(self, img, seg):
        img_flip = img[:, ::-1, :]
        seg_flip = seg[:, ::-1]
        return img_flip, seg_flip


    def __getitem__(self, index):
        img_basename = self.list_sample[index]

        scale, x1, y1 = self._get_scale_and_crop_params(self.imgOriginalShape, cropSize=self.imgSize, is_train=self.is_train)

        flip = False
        if self.is_train and random.choice([-1, 1]) > 0:
            flip = True

        img, seg = self.getImageData(img_basename, flip, scale, x1, y1)

        img_frame_ind = int(re.findall(r'\d+', img_basename.split(os.sep)[-1].split('_')[-1])[0])
        img_tm1_basename = getFilename(img_basename, img_frame_ind-self.curr_prev_ind_gap)
        img_tm1, _ = self.getImageData(img_tm1_basename, flip, scale, x1, y1)

        _, h, w = img.size()

        img = torch.cat([img_tm1.unsqueeze(0), img.unsqueeze(0)], dim=0)

        #add optical flow info
        mode = img_basename.split('/')[0]
        seq_name = img_basename.split('/')[1]
        startFrameInd = int(re.findall(r'\d+', self.categoriesMap[seq_name][0].split(os.sep)[-1].split('_')[-1])[0])

        flow = self.get_flow(img_frame_ind, mode, seq_name, startFrameInd, scale, x1, y1, flip, h, w, self.args)

        return img, seg, flow, img_basename


    def __len__(self):
        return len(self.list_sample)


    def fetch_flow_data(self, flow_path):

        with open(flow_path, mode='rb') as f:
            ftag = np.fromfile(f, dtype=np.float32, count=1)[0]
            if ftag != 202021.25:
                raise ('Error in reading flo')
            width = np.fromfile(f, dtype=np.int32, count=1)[0]
            height = np.fromfile(f, dtype=np.int32, count=1)[0]
            flo = np.fromfile(f, dtype=np.float32, count=-1)
        flo = np.reshape(flo, (height, width, 2))
        return flo


    def get_flow(self, img_ind, mode, seqName, startFrameInd, scale, x1, y1, flip, h, w, args):
        flow = None
        prev_ind = img_ind - args.curr_prev_ind_gap

        if img_ind >= startFrameInd and prev_ind >= startFrameInd:
            path = os.path.join(args.root_optical_flow, mode, seqName, str(img_ind) + '_' + str(prev_ind) + '.flo')
            if flip:
                path = os.path.join(args.root_optical_flow, mode, seqName,
                                    str(img_ind) + '_' + str(prev_ind) + '_flipped.flo')

            flow = self.fetch_flow_data(path)
            flow = self.fetch_flow(flow, scale, x1, y1, args)

        if flow is None:
            flow = np.zeros((1, 2, h, w))

        flow = torch.from_numpy(np.array(flow[0])).type(torch.FloatTensor)
        return flow


    def getImageData(self, imgFilename, flip, scale, x1, y1):

        isDummyData = False
        path_img = os.path.join(self.root_img, imgFilename)
        path_seg = os.path.join(self.root_seg, imgFilename)

        # load image
        try:
            img = imread(path_img, mode='RGB')
            assert (img.ndim == 3)
        except Exception as e:
            #print('Failed loading image/segmentation [{}]: {}'.format(path_img, e))
            # dummy data
            img = np.zeros((self.imgOriginalShape[0], self.imgOriginalShape[1], 3)).astype(np.uint8)
            isDummyData = True

        # load label
        try:
            seg = imread(path_seg)
            assert (seg.ndim == 2)
        except Exception as e:
            # print('Failed loading image/segmentation [{}]: {}'.format(path_img, e))
            # dummy data
            seg = self.dataset_constants.void_labels[0] * np.ones(self.imgOriginalShape).astype(np.uint8)

        assert (img.shape[0] == seg.shape[0])
        assert (img.shape[1] == seg.shape[1])

        # random scale, crop, flip
        if self.imgSize > 0 and self.segSize > 0:
            if flip:
                img, seg = self._flip(img, seg)

            img, seg = self._apply_scale_and_crop(img, seg, scale, x1, y1, self.imgSize)

        # image to float
        img = np.array(img).astype(np.float32) / 255.
        img = img.transpose((2, 0, 1))
        # substracted by mean and divided by std
        img = torch.from_numpy(img)

        if not isDummyData:
            img = self.img_transform(img)

        # if self.segSize > 0:
        #     seg = imresize(seg, (self.segSize, self.segSize), interp='nearest')

        seg = seg.astype(np.int)

        # to torch tensor
        seg = torch.from_numpy(seg)

        return img, seg


    def _apply_scale_and_crop(self, img, seg, scale, x1, y1, cropSize):

        img_scale = imresize(img, scale, interp='bilinear')
        seg_scale = imresize(seg, scale, interp='nearest')

        img_crop = img_scale[y1: y1 + cropSize, x1: x1 + cropSize, :]
        seg_crop = seg_scale[y1: y1 + cropSize, x1: x1 + cropSize]

        return img_crop, seg_crop


    def _get_scale_and_crop_params(self, imgShape, cropSize, is_train):
        h, w = imgShape[0], imgShape[1]

        if is_train:
            # random scale
            scale = random.random() + 0.5     # 0.5-1.5
            scale = max(scale, 1. * cropSize / (min(h, w) - 1))
        else:
            # scale to crop size
            scale = 1. * cropSize / (min(h, w) - 1)

        img_scale_shape = [int(h*scale), int(w*scale)]

        h_s, w_s = img_scale_shape[0], img_scale_shape[1]
        if is_train:
            # random crop
            x1 = random.randint(0, w_s - cropSize)
            y1 = random.randint(0, h_s - cropSize)
        else:
            # center crop
            x1 = (w_s - cropSize) // 2
            y1 = (h_s - cropSize) // 2

        return scale, x1, y1
