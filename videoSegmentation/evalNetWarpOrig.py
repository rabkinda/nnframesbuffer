# System libs
import argparse
import datetime
# Our libs
import math
import os

# Numerical libs
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from scipy.misc import imsave
from scipy.ndimage import zoom
from torch.autograd import Variable

from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from models import ModelBuilder
from utils import AverageMeter, colorEncode, accuracy, intersectionAndUnion, savePrediction
from videoSegmentation.segmentation_dataset_for_net_warp import SegmentationDataset


# forward func for evaluation
def forward_multiscale(nets, warpModule, batch_data, args):
    (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit) = nets
    (imgs, segs, optical_flows, infos) = batch_data

    segSize = (segs.size(1), segs.size(2))
    pred = torch.zeros(imgs.size(0), args.num_class, segs.size(1), segs.size(2))
    pred = Variable(pred, volatile=True).cuda()

    for scale in args.scales:
        imgs_tm1_scale = zoom(imgs[:,0,:,:,:].numpy(),
                          (1., 1., scale, scale),
                          order=1,
                          prefilter=False,
                          mode='nearest')

        imgs_t_scale = zoom(imgs[:,1,:,:,:].numpy(),
                          (1., 1., scale, scale),
                          order=1,
                          prefilter=False,
                          mode='nearest')

        optical_flows_scale = zoom(optical_flows.numpy(),
                          (1., 1., scale, scale),
                          order=1,
                          prefilter=False,
                          mode='nearest')

        # feed input data
        input_img_tm1 = Variable(torch.from_numpy(imgs_tm1_scale), volatile=True).cuda()
        input_img_t = Variable(torch.from_numpy(imgs_t_scale), volatile=True).cuda()
        input_optical_flow = Variable(torch.from_numpy(optical_flows_scale), volatile=True).cuda()

        # forward
        encoded_img_tm1 = net_encoder_pspnet(net_encoder_resnet(input_img_tm1))
        encoded_img_t = net_encoder_pspnet(net_encoder_resnet(input_img_t))

        _, _, h_scaled, w_scaled = optical_flows_scale.shape
        padding = 2 if (h_scaled%8!=0 or w_scaled%8!=0) else 0
        optical_flow = F.avg_pool2d(input_optical_flow, kernel_size=(8, 8), stride=8, padding=padding)
        warped_encoded_img_tm1 = warpModule(encoded_img_tm1, optical_flow)

        pred_scale = net_decoder(warped_encoded_img_tm1, encoded_img_t, segSize=segSize)

        # average the probability
        pred = pred + pred_scale / len(args.scales)

    pred = torch.log(pred)

    label_seg = Variable(segs, volatile=True).cuda()
    err = crit(pred, label_seg)
    return pred, err


def visualize_result(imgs, segs, infos, pred, args):
    dataset_constants = DatasetFactory.getConstants(args.dataset)
    colors = dataset_constants.cmap

    for j in range(len(infos)):
        # get/recover image
        # img = imread(os.path.join(args.root_img, infos[j]))
        img = imgs[j].clone()
        for t, m, s in zip(img,
                           dataset_constants.mean,
                           dataset_constants.std):
                           #[0.485, 0.456, 0.406],
                           #[0.229, 0.224, 0.225]):
            t.mul_(s).add_(m)
        img = (img.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8)

        # segmentation
        lab = segs[j].numpy()
        lab_color = colorEncode(lab, lab, colors, args)

        # prediction
        pred_ = np.argmax(pred.data.cpu()[j].numpy(), axis=0)
        pred_color = colorEncode(pred_, lab, colors, args)

        # aggregate images and save
        im_vis = np.concatenate((img, lab_color, pred_color),
                                axis=1).astype(np.uint8)
        imsave(os.path.join(args.result,
                            infos[j].replace('/', '_')
                            .replace('.jpg', '.png')), im_vis)


def evaluate(nets, loader, args):
    loss_meter = AverageMeter()
    acc_meter = AverageMeter()
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()

    # switch to eval mode
    for net in nets:
        net.eval()

    warpModule = GridSamplerModule()
    warpModule.eval()

    for i, batch_data in enumerate(loader):
        # forward pass
        pred, err = forward_multiscale(nets, warpModule, batch_data, args)
        loss_meter.update(err.data[0])

        (imgs, segs, optical_flows, infos) = batch_data
        savePrediction(infos, pred, args)
        # calculate accuracy
        acc, pix = accuracy(batch_data, pred, args)
        intersection, union = intersectionAndUnion(batch_data, pred,
                                                   args.num_class, args)
        acc_meter.update(acc, pix)
        intersection_meter.update(intersection)
        union_meter.update(union)

        print('[{}] iter {}, loss: {}, accuracy: {}'
              .format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                      i, err.data[0], acc*100))

        # visualization
        if args.visualize:
            visualize_result(imgs[:,1,:,:,:], segs, infos, pred, args)

    iou = intersection_meter.sum / (union_meter.sum + 1e-10)
    for i, _iou in enumerate(iou):
        print('class [{}], IoU: {}'.format(i, _iou))

    print('[Eval Summary]:')
    print('Loss: {}, Mean IoU: {:.4}, Accurarcy: {:.2f}%'
          .format(loss_meter.average(), iou.mean(), acc_meter.average()*100))


def main(args):
    # Network Builders
    builder = ModelBuilder()
    net_encoder_resnet = builder.build_encoder(arch=args.arch_encoder_resnet,
                                               fc_dim=args.fc_dim,
                                               weights=args.weights_encoder_resnet)
    net_encoder_pspnet = builder.build_encoder(arch=args.arch_encoder_pspnet,
                                               fc_dim=args.fc_dim,
                                               weights=args.weights_encoder_pspnet)
    net_decoder = builder.build_decoder(arch=args.arch_decoder,
                                        fc_dim=args.fc_dim,
                                        segSize=args.segSize,
                                        num_class=args.num_class,
                                        weights=args.weights_decoder,
                                        use_softmax=True)

    dataset_constants = DatasetFactory.getConstants(args.dataset)

    #label_colors = utility.read_color_integer(os.path.join(args.data_folder, "label_integer.txt"))

    # build a dictionary with key = (r,g,b), value = percentage of color
    #color_info = utility.read_color_count_sorted(os.path.join(args.data_folder, 'color_count_train_sorted.txt'))
    #color_info = {(data[0], data[1], data[2]): data[4] for data in color_info}

    # build the weights matrix according to the formula 1/ln(1.02 + w)
    weights = None
    if args.weighting:
        print("set weights")
        weights = []

        for labelInd in xrange(0, len(dataset_constants.labelsLst), 1):
            w = dataset_constants.labelsLst[labelInd]['freq']
            w = 1 / math.log(1.02 + w, 2)
            weights.append(w)

        weights = np.array(weights)
        weights = torch.from_numpy(weights.astype('float32')).cuda()
    else:
        print("zero weights")

    crit = nn.NLLLoss2d(weight=weights, ignore_index=dataset_constants.void_labels[0])#-1)

    # Dataset and Loader
    dataset_val = SegmentationDataset(args.list_val, args, max_sample=args.num_val, is_train=0)
    loader_val = torch.utils.data.DataLoader(
        dataset_val,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=2,
        drop_last=False)

    nets = (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit)
    for net in nets:
        net.cuda()

    # Main loop
    evaluate(nets, loader_val, args)

    print('Evaluation Done!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Model related arguments
    parser.add_argument('--id', required=True,
                        help="a name for identifying the model to load")
    parser.add_argument('--suffix', default='_best.pth',
                        help="which snapshot to load")
    parser.add_argument('--arch_encoder_resnet', default='resnet50_dilated8',
                        help="architecture of net_encoder")
    parser.add_argument('--arch_encoder_pspnet', default='net_warp_psp_bilinear_encoder',
                        help="architecture of net_encoder")
    parser.add_argument('--arch_decoder', default='net_warp_psp_bilinear_decoder',
                        help="architecture of net_decoder")
    parser.add_argument('--fc_dim', default=2048, type=int,
                        help='number of features between encoder and decoder')

    # Path related arguments
    parser.add_argument('--list_val',
                        default='./data/ADE20K_object150_val.txt')
    parser.add_argument('--root_img',
                        default='./data/ADEChallengeData2016/images')
    parser.add_argument('--root_seg',
                        default='./data/ADEChallengeData2016/annotations')
    parser.add_argument('--root_pred',
                        default='./data/ADEChallengeData2016/predictions')
    parser.add_argument('--data_folder',
                        default='data/ADEChallengeData2016')
    parser.add_argument('--root_optical_flow', default='OF_DIS_with_flips_key_frame_prev_1_5_10_15_20_25_30')

    # Data related arguments
    parser.add_argument('--num_val', default=-1, type=int,
                        help='number of images to evalutate')
    parser.add_argument('--num_class', default=150, type=int,
                        help='number of classes')
    parser.add_argument('--batch_size', default=1, type=int,
                        help='batchsize')
    parser.add_argument('--imgSize', default=-1, type=int,
                        help='input image size, -1 = keep original')
    parser.add_argument('--segSize', default=-1, type=int,
                        help='output image size, -1 = keep original')

    parser.add_argument('--weighting', type=int, default=1,
                        help='True will adopt weights on loss function and vice versa')

    parser.add_argument('--curr_prev_ind_gap', type=int, default=1,
                        help='curr_prev_ind_gap')

    # Misc arguments
    parser.add_argument('--ckpt', default='./ckpt_eval',
                        help='folder to output checkpoints')
    parser.add_argument('--visualize', default=1,
                        help='output visualization?')
    parser.add_argument('--result', default='./result_eval',
                        help='folder to output visualization results')
    parser.add_argument('--multi_scale', type=int, default=1,
                        help='multi_scale')

    args = parser.parse_args()
    print(args)

    # scales for evaluation
    args.scales = (1,)
    if args.multi_scale==1:
        args.scales = (0.5, 0.75, 1, 1.25, 1.5)

    # absolute paths of model weights
    args.weights_encoder_resnet = os.path.join(args.ckpt, args.id,
                                        'encoder_resnet' + args.suffix)
    args.weights_encoder_pspnet = os.path.join(args.ckpt, args.id,
                                               'encoder_pspnet' + args.suffix)
    args.weights_decoder = os.path.join(args.ckpt, args.id,
                                        'decoder' + args.suffix)

    args.result = os.path.join(args.result, args.id)
    if not os.path.isdir(args.result):
        os.makedirs(args.result)

    main(args)
