# System libs
import argparse
import math
import os
# import math
import random
import time

import matplotlib
# Numerical libs
import numpy as np
import torch
import torch.nn as nn
from scipy.misc import imresize, imsave
from torch.autograd import Variable
import torch.nn.functional as F

# Our libs
from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from models import ModelBuilder
from videoSegmentation.segmentation_dataset_for_net_warp import SegmentationDataset
from utils import AverageMeter, colorEncode, accuracy

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from tensorboardX import SummaryWriter


def forward_with_loss(nets, warpModule, batch_data, args, is_train=True):
    (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit) = nets
    (imgs, segs, optical_flows, infos) = batch_data

    # feed input data
    input_img = Variable(imgs, volatile=not is_train)
    label_seg = Variable(segs, volatile=not is_train)
    optical_flow = Variable(optical_flows, volatile=not is_train)
    input_img = input_img.cuda()
    label_seg = label_seg.cuda()
    optical_flow = optical_flow.cuda()

    # forward
    encoded_img_tm1 = net_encoder_pspnet(net_encoder_resnet(input_img[:,0,:,:,:]))
    encoded_img_t = net_encoder_pspnet(net_encoder_resnet(input_img[:,1,:,:,:]))

    optical_flow = F.avg_pool2d(optical_flow, kernel_size=(8, 8), stride=8)
    warped_encoded_img_tm1 = warpModule(encoded_img_tm1, optical_flow)

    pred = net_decoder(warped_encoded_img_tm1, encoded_img_t)

    err = crit(pred, label_seg)
    return pred, err


def visualize(imgs, segs, infos, pred, args):
    dataset_constants = DatasetFactory.getConstants(args.dataset)
    colors = dataset_constants.cmap
    for j in range(len(infos)):
        img = imgs[j].clone()
        for t, m, s in zip(img,
                           dataset_constants.mean,
                           dataset_constants.std):
                           #[0.485, 0.456, 0.406],
                           #[0.229, 0.224, 0.225]):
            t.mul_(s).add_(m)
        img = (img.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8)
        img = imresize(img, (args.imgSize, args.imgSize),
                       interp='bilinear')

        # segmentation
        lab = segs[j].numpy()
        lab_color = colorEncode(lab, lab, colors, args)
        lab_color = imresize(lab_color, (args.imgSize, args.imgSize),
                             interp='nearest')

        # prediction
        pred_ = np.argmax(pred.data.cpu()[j].numpy(), axis=0)
        pred_color = colorEncode(pred_, lab, colors, args)
        pred_color = imresize(pred_color, (args.imgSize, args.imgSize),
                              interp='nearest')

        # aggregate images and save
        im_vis = np.concatenate((img, lab_color, pred_color),
                                axis=1).astype(np.uint8)
        imsave(os.path.join(args.vis,
                            infos[j].replace('/', '_')
                            .replace('.jpg', '.png')), im_vis)


# train one epoch
def train(nets, warpModule, loader, optimizers, history, epoch, writer, iterNum, args):
    batch_time = AverageMeter()
    data_time = AverageMeter()

    # switch to train mode
    for net in nets:
        if not args.fix_bn:
            net.train()
        else:
            net.eval()

    # main loop
    tic = time.time()
    for i, batch_data in enumerate(loader):
        data_time.update(time.time() - tic)
        for net in nets:
            net.zero_grad()

        # forward pass
        pred, err = forward_with_loss(nets, warpModule, batch_data, args, is_train=True)

        # Backward
        err.backward()
        for optimizer in optimizers:
            optimizer.step()

        acc, _ = accuracy(batch_data, pred, args)
        writer.add_scalar('Train/Loss', err, iterNum)
        writer.add_scalar('Train/Precision', acc*100, iterNum)
        iterNum += 1

        # measure elapsed time
        batch_time.update(time.time() - tic)
        tic = time.time()

        # calculate accuracy, and display
        if i % args.disp_iter == 0:
            print('Epoch: [{}][{}/{}], Time: {:.2f}, Data: {:.2f}, '
                  'lr_encoder: {}, lr_decoder: {}, '
                  'Accurarcy: {:4.2f}%, Loss: {}'
                  .format(epoch, i, args.epoch_iters,
                          batch_time.average(), data_time.average(),
                          args.lr_encoder, args.lr_decoder,
                          acc*100, err.data[0]))

            fractional_epoch = epoch - 1 + 1. * i / args.epoch_iters
            history['train']['epoch'].append(fractional_epoch)
            history['train']['err'].append(err.data[0])
            history['train']['acc'].append(acc)

    (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit) = nets
    writeParametersValuesAndGrads(net_encoder_resnet.named_parameters(), writer, epoch, 'net_encoder_resnet')
    writeParametersValuesAndGrads(net_encoder_pspnet.named_parameters(), writer, epoch, 'net_encoder_pspnet')
    writeParametersValuesAndGrads(net_decoder.named_parameters(), writer, epoch, 'net_decoder')
    return iterNum


def writeParametersValuesAndGrads(parameters, writer, epoch, network_name):
    for name, param in parameters:
        writer.add_histogram(network_name + '/' + name, param.data.cpu().numpy(), epoch)

        if param.grad is not None:
            writer.add_histogram(network_name + '/' + name + '/gradient', param.grad.data.cpu().numpy(), epoch)


def evaluate(nets, warpModule, loader, history, epoch, writer, iterNum, args):
    print('Evaluating at {} epochs...'.format(epoch))
    loss_meter = AverageMeter()
    acc_meter = AverageMeter()

    # switch to eval mode
    for net in nets:
        net.eval()

    for i, batch_data in enumerate(loader):
        # forward pass
        pred, err = forward_with_loss(nets, warpModule, batch_data, args, is_train=False)
        loss_meter.update(err.data[0])
        print('[Eval] iter {}, loss: {}'.format(i, err.data[0]))

        # calculate accuracy
        acc, pix = accuracy(batch_data, pred, args)
        acc_meter.update(acc, pix)

        # visualization
        #(imgs, segs, optical_flows, infos) = batch_data
        #visualize(imgs[:,1,:,:,:], segs, infos, pred, args)

        writer.add_scalar('Val/Loss', err, iterNum)
        writer.add_scalar('Val/Precision', acc*100, iterNum)
        iterNum += 1

    history['val']['epoch'].append(epoch)
    history['val']['err'].append(loss_meter.average())
    history['val']['acc'].append(acc_meter.average())
    print('[Eval Summary] Epoch: {}, Loss: {}, Accurarcy: {:4.2f}%'
          .format(epoch, loss_meter.average(), acc_meter.average()*100))

    # Plot figure
    if epoch > 0:
        print('Plotting loss figure...')
        fig = plt.figure()
        plt.plot(np.asarray(history['train']['epoch']),
                 np.log(np.asarray(history['train']['err'])),
                 color='b', label='training')
        plt.plot(np.asarray(history['val']['epoch']),
                 np.log(np.asarray(history['val']['err'])),
                 color='c', label='validation')
        plt.legend()
        plt.xlabel('Epoch')
        plt.ylabel('Log(loss)')
        fig.savefig('{}/loss.png'.format(args.ckpt), dpi=200)
        plt.close('all')

        fig = plt.figure()
        plt.plot(history['train']['epoch'], history['train']['acc'],
                 color='b', label='training')
        plt.plot(history['val']['epoch'], history['val']['acc'],
                 color='c', label='validation')
        plt.legend()
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        fig.savefig('{}/accuracy.png'.format(args.ckpt), dpi=200)
        plt.close('all')

    return iterNum


def checkpoint(nets, history, epoch, trainIter, valIter, args):
    print('Saving checkpoints...')
    (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit) = nets
    suffix_latest = 'latest.pth'
    suffix_best = 'best.pth'

    if args.num_gpus > 1:
        dict_encoder_resnet = net_encoder_resnet.module.state_dict()
        dict_encoder_pspnet = net_encoder_pspnet.module.state_dict()
        dict_decoder = net_decoder.module.state_dict()
    else:
        dict_encoder_resnet = net_encoder_resnet.state_dict()
        dict_encoder_pspnet = net_encoder_pspnet.state_dict()
        dict_decoder = net_decoder.state_dict()

    torch.save([history, epoch, trainIter, valIter, args],
               '{}/history_{}'.format(args.ckpt, suffix_latest))
    torch.save(dict_encoder_resnet,
               '{}/encoder_resnet_{}'.format(args.ckpt, suffix_latest))
    torch.save(dict_encoder_pspnet,
               '{}/encoder_pspnet_{}'.format(args.ckpt, suffix_latest))
    torch.save(dict_decoder,
               '{}/decoder_{}'.format(args.ckpt, suffix_latest))

    try:
        cur_err = history['val']['err'][-1]
    except:
        cur_err = args.best_err - 1

    if cur_err < args.best_err:
        args.best_err = cur_err
        torch.save([history, epoch, trainIter, valIter, args],
                   '{}/history_{}'.format(args.ckpt, suffix_best))
        torch.save(dict_encoder_resnet,
                   '{}/encoder_resnet_{}'.format(args.ckpt, suffix_best))
        torch.save(dict_encoder_pspnet,
                   '{}/encoder_pspnet_{}'.format(args.ckpt, suffix_best))
        torch.save(dict_decoder,
                   '{}/decoder_{}'.format(args.ckpt, suffix_best))


def create_optimizers(nets, args):
    (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit) = nets
    optimizer_encoder_resnet = torch.optim.SGD(
        net_encoder_resnet.parameters(),
        lr=args.lr_encoder,
        momentum=args.beta1,
        weight_decay=args.weight_decay)
    optimizer_encoder_pspnet = torch.optim.SGD(
        net_encoder_pspnet.parameters(),
        lr=args.lr_decoder,
        momentum=args.beta1,
        weight_decay=args.weight_decay)
    optimizer_decoder = torch.optim.SGD(
        net_decoder.parameters(),
        lr=args.lr_decoder,
        momentum=args.beta1,
        weight_decay=args.weight_decay)
    return (optimizer_encoder_resnet, optimizer_encoder_pspnet, optimizer_decoder)


def adjust_learning_rate(optimizers, epoch, args):
    drop_ratio = (1. * (args.num_epoch-epoch) / (args.num_epoch-epoch+1)) \
                 ** args.lr_pow
    args.lr_encoder *= drop_ratio
    args.lr_decoder *= drop_ratio
    (optimizer_encoder_resnet, optimizer_encoder_pspnet, optimizer_decoder) = optimizers
    for param_group in optimizer_encoder_resnet.param_groups:
        param_group['lr'] = args.lr_encoder
    for param_group in optimizer_encoder_pspnet.param_groups:
        param_group['lr'] = args.lr_decoder
    for param_group in optimizer_decoder.param_groups:
        param_group['lr'] = args.lr_decoder


def main(args):
    writer = SummaryWriter(args.ckpt)
    start_epoch = 1
    trainIterNum = 1
    valIterNum = 1

    # Network Builders
    builder = ModelBuilder()
    net_encoder_resnet = builder.build_encoder(arch=args.arch_encoder_resnet,
                                        fc_dim=args.fc_dim,
                                        weights=args.weights_encoder_resnet)
    net_encoder_pspnet = builder.build_encoder(arch=args.arch_encoder_pspnet,
                                               fc_dim=args.fc_dim,
                                               weights=args.weights_encoder_pspnet)
    net_decoder = builder.build_decoder(arch=args.arch_decoder,
                                        fc_dim=args.fc_dim,
                                        segSize=args.segSize,
                                        num_class=args.num_class,
                                        weights=args.weights_decoder)

    if args.checkpoint != None:
        checkpoint_args_path = os.path.dirname(args.checkpoint) + '/history_best.pth'
        checkpoint_args = torch.load(checkpoint_args_path)

        start_epoch = checkpoint_args[1]
        trainIterNum = checkpoint_args[2]
        valIterNum = checkpoint_args[3]

    dataset_constants = DatasetFactory.getConstants(args.dataset)

    #label_colors = utility.read_color_integer(os.path.join(args.data_folder, "label_integer.txt"))

    # build a dictionary with key = (r,g,b), value = percentage of color
    #color_info = utility.read_color_count_sorted(os.path.join(args.data_folder, 'color_count_train_sorted.txt'))
    #color_info = {(data[0], data[1], data[2]): data[4] for data in color_info}

    # build the weights matrix according to the formula 1/ln(1.02 + w)
    weights = None
    if args.weighting:
        print("set weights")
        weights = []
        for labelInd in xrange(0, len(dataset_constants.labelsLst), 1):
            w = dataset_constants.labelsLst[labelInd]['freq']
            w = 1 / math.log(1.02 + w, 2)
            weights.append(w)

        weights = np.array(weights)
        weights = torch.from_numpy(weights.astype('float32')).cuda()
    else:
        print("zero weights")

    crit = nn.NLLLoss2d(weight=weights, ignore_index=dataset_constants.void_labels[0])  # -1)

    # Dataset and Loader
    dataset_train = SegmentationDataset(args.list_train, args, is_train=1)
    dataset_val = SegmentationDataset(args.list_val, args, max_sample=args.num_val, is_train=0)
    loader_train = torch.utils.data.DataLoader(
        dataset_train,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=int(args.workers),
        drop_last=True)
    loader_val = torch.utils.data.DataLoader(
        dataset_val,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=int(args.workers),
        drop_last=True)
    args.epoch_iters = int(len(dataset_train) / args.batch_size)
    print('1 Epoch = {} iters'.format(args.epoch_iters))

    # load nets into gpu
    if args.num_gpus > 1:
        net_encoder_resnet = nn.DataParallel(net_encoder_resnet,
                                      device_ids=range(args.num_gpus))
        net_encoder_pspnet = nn.DataParallel(net_encoder_pspnet,
                                             device_ids=range(args.num_gpus))
        net_decoder = nn.DataParallel(net_decoder,
                                      device_ids=range(args.num_gpus))

    nets = (net_encoder_resnet, net_encoder_pspnet, net_decoder, crit)
    for net in nets:
        net.cuda()

    # Set up optimizers
    optimizers = create_optimizers(nets, args)

    warpModule = GridSamplerModule()

    # Main loop
    history = {split: {'epoch': [], 'err': [], 'acc': []}
               for split in ('train', 'val')}
    # initial eval
    #evaluate(nets, loader_val, history, 0, args)
    #for epoch in range(1, args.num_epoch + 1):
    for epoch in range(start_epoch, start_epoch + args.num_epoch):
        trainIterNum = train(nets, warpModule, loader_train, optimizers, history, epoch, writer, trainIterNum, args)

        # Evaluation and visualization
        if epoch % args.eval_epoch == 0:
            valIterNum = evaluate(nets, warpModule, loader_val, history, epoch, writer, valIterNum, args)

        # checkpointing
        checkpoint(nets, history, epoch, trainIterNum, valIterNum, args)

        # adjust learning rate
        adjust_learning_rate(optimizers, epoch, args)

    writer.close()
    print('Training Done!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Model related arguments
    parser.add_argument('--id', default='baseline',
                        help="a name for identifying the model")
    parser.add_argument('--arch_encoder_resnet', default='resnet34_dilated8',
                        help="architecture of net_encoder")
    parser.add_argument('--arch_decoder', default='c1_bilinear',
                        help="architecture of net_decoder")
    parser.add_argument('--arch_encoder_pspnet', default='net_warp_psp_bilinear_encoder',
                        help="architecture of net_encoder")
    parser.add_argument('--weights_encoder_resnet', default='',
                        help="weights to finetune net_encoder")
    parser.add_argument('--weights_decoder', default='',
                        help="weights to finetune net_decoder")
    parser.add_argument('--weights_encoder_pspnet', default='',
                        help="weights to finetune net_encoder_temporal")
    parser.add_argument('--checkpoint', type=str, help='checkpoint')
    parser.add_argument('--fc_dim', default=512, type=int,
                        help='number of features between encoder and decoder')

    #TODO- need to be adapted
    # Path related arguments
    parser.add_argument('--list_train',
                        default='./data/ADE20K_object150_train.txt')
    parser.add_argument('--list_val',
                        default='./data/ADE20K_object150_val.txt')
    parser.add_argument('--root_img',
                        default='./data/ADEChallengeData2016/images')
    parser.add_argument('--root_seg',
                        default='./data/ADEChallengeData2016/annotations')
    parser.add_argument('--data_folder',
                        default='data/ADEChallengeData2016')
    parser.add_argument('--root_optical_flow', default='OF_DIS_with_flips_key_frame_prev_1_5_10_15_20_25_30')

    # optimization related arguments
    parser.add_argument('--num_gpus', default=1, type=int,
                        help='number of gpus to use')
    parser.add_argument('--batch_size_per_gpu', default=16, type=int,
                        help='input batch size')
    parser.add_argument('--num_epoch', default=100, type=int,
                        help='epochs to train for')
    parser.add_argument('--optim', default='SGD', help='optimizer')
    parser.add_argument('--lr_encoder', default=1e-3, type=float, help='LR')
    parser.add_argument('--lr_decoder', default=1e-2, type=float, help='LR')
    parser.add_argument('--lr_pow', default=0.9, type=float,
                        help='power in poly to drop LR')
    parser.add_argument('--beta1', default=0.9, type=float,
                        help='momentum for sgd, beta1 for adam')
    parser.add_argument('--weight_decay', default=1e-4, type=float,
                        help='weights regularizer')
    parser.add_argument('--fix_bn', default=0, type=int,
                        help='fix bn params')

    # TODO- need to be adapted
    # Data related arguments
    parser.add_argument('--num_val', default=128, type=int,
                        help='number of images to evalutate')
    parser.add_argument('--num_class', default=150, type=int,
                        help='number of classes')
    parser.add_argument('--workers', default=16, type=int,
                        help='number of data loading workers')
    parser.add_argument('--imgSize', default=720, type=int,
                        help='input image size')
    parser.add_argument('--segSize', default=720, type=int,
                        help='output image size')

    # Misc arguments
    parser.add_argument('--seed', default=1234, type=int, help='manual seed')
    parser.add_argument('--ckpt', default='./ckpt',
                        help='folder to output checkpoints')
    parser.add_argument('--vis', default='./vis',
                        help='folder to output visualization during training')
    parser.add_argument('--disp_iter', type=int, default=20,
                        help='frequency to display')
    parser.add_argument('--eval_epoch', type=int, default=1,
                        help='frequency to evaluate')

    parser.add_argument('--weighting', type=int, default=1,
                        help='True will adopt weights on loss function and vice versa')

    parser.add_argument('--curr_prev_ind_gap', type=int, default=1,
                        help='curr_prev_ind_gap')

    args = parser.parse_args()

    #args.root_optical_flow = os.path.join(args.data_folder, args.root_optical_flow)

    print("Input arguments:")
    for key, val in vars(args).items():
        print("{:16} {}".format(key, val))

    args.batch_size = args.num_gpus * args.batch_size_per_gpu
    if args.num_val < args.batch_size:
        args.num_val = args.batch_size

    args.id += '-' + str(args.arch_encoder_resnet)
    args.id += '-' + str(args.arch_encoder_pspnet)
    args.id += '-' + str(args.arch_decoder)
    args.id += '-ngpus' + str(args.num_gpus)
    args.id += '-batchSize' + str(args.batch_size)
    args.id += '-imgSize' + str(args.imgSize)
    args.id += '-segSize' + str(args.segSize)
    args.id += '-lr_encoder' + str(args.lr_encoder)
    args.id += '-lr_decoder' + str(args.lr_decoder)
    args.id += '-epoch' + str(args.num_epoch)
    args.id += '-decay' + str(args.weight_decay)
    args.id += '-curr_prev_ind_gap' + str(args.curr_prev_ind_gap)
    print('Model ID: {}'.format(args.id))

    args.ckpt = os.path.join(args.ckpt, args.id)
    args.vis = os.path.join(args.vis, args.id)
    if not os.path.isdir(args.ckpt):
        os.makedirs(args.ckpt)
    if not os.path.exists(args.vis):
        os.makedirs(args.vis)

    args.best_err = 2.e10   # initialize with a big number

    random.seed(args.seed)
    torch.manual_seed(args.seed)

    main(args)
