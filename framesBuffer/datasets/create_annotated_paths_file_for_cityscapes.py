from pathlib import Path
import argparse


parser = argparse.ArgumentParser(description='PyTorch Loop')

parser.add_argument('--annotation_dir', type=str, help='annotation_dir')
parser.add_argument('--include_coarse', type=int, default=1, help='include_coarse')
args = parser.parse_args()

coarseName = '_with_coarse' if args.include_coarse else '_without_coarse'
train_file = open(args.annotation_dir+'../train'+coarseName+'.txt', 'w')
val_file = open(args.annotation_dir+'../val'+'.txt', 'w')
test_file = open(args.annotation_dir+'../test'+'.txt', 'w')


def getFileForWriting(train_val_test_dir_name):
    if train_val_test_dir_name=='train':
        return train_file
    if train_val_test_dir_name=='val':
        return val_file
    if train_val_test_dir_name=='test':
        return test_file
    else:
        raise Exception('no such mode')


file_identifier = '_labelTrainIds.png' if args.include_coarse else '_gtFine_labelTrainIds.png'
img_suffix = 'leftImg8bit'
pathlist = Path(args.annotation_dir).glob('**/**/*'+file_identifier)
pathlist = [str(path) for path in pathlist]
pathlist.sort()

for path in pathlist:
    relative_annotation_path = path.replace(args.annotation_dir, '')
    relative_img_path = relative_annotation_path.replace('gtFine_labelTrainIds', img_suffix)\
                                                .replace('gtCoarse_labelTrainIds', img_suffix)
    getFileForWriting(relative_annotation_path.split('/')[0]).write(relative_img_path+'\t'+relative_annotation_path+'\n')

train_file.close()
val_file.close()
test_file.close()
