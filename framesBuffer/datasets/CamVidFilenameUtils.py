import os
import re
from framesBuffer.datasets.FilenameUtils import FilenameUtils


class CamVidFilenameUtils(FilenameUtils):

    def __init__(self, txt):
        super(CamVidFilenameUtils, self).__init__(txt)


    def getExamplePaths(self, seqName):
        catagory = [k for k in self.categoriesMap.keys() if k in seqName][0]
        example_paths = self.categoriesMap[catagory][1]
        return example_paths


    def getCatagory(self, frame_path):
        catagory = frame_path.split(os.sep)[1]
        return catagory


    def getFrameIndexStrPart(self, frame_path):
        return frame_path.split(os.sep)[-1].split('_')[-1].split('.')[0]


    def getStartFrameInd(self, seqName):
        return self.getFrameIndex(self.categoriesMap[seqName][0].getImgPath())


    def getFramePath(self, example_path, dst_frame_index):
        example_frame_index_str_part = self.getFrameIndexStrPart(example_path)
        example_frame_index = int(re.findall(r'\d+', example_frame_index_str_part)[0])

        dst_frame_index_str_part = example_frame_index_str_part \
            .replace('0' * (len(str(dst_frame_index)) - len(str(example_frame_index))) + str(example_frame_index),
                     '0' * (len(str(example_frame_index)) - len(str(dst_frame_index))) + str(dst_frame_index))

        path_prefix = example_path.split('_')[0] + '_'
        dst_frame_path = path_prefix + dst_frame_index_str_part + '.png'
        return dst_frame_path