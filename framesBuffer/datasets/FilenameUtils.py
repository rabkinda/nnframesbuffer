import os
import re

from framesBuffer.datasets.DatasetPaths import DatasetPaths


class FilenameUtils(object):

    def __init__(self, txt):
        self.list_sample = [DatasetPaths(x.rstrip().split(' ')[0].split('\t')[0], x.rstrip().split(' ')[0].split('\t')[-1]) for x in open(txt, 'r')]
        self.categoriesMap = {}

        for s in self.list_sample:
            catagory = self.getCatagory(s.getImgPath())
            if catagory not in self.categoriesMap:
                self.categoriesMap[catagory] = []
            self.categoriesMap[catagory].append(s)


    def getCategoriesMap(self):
        return self.categoriesMap


    def getListSample(self):
        return self.list_sample


    def getMode(self):
        example_path = self.categoriesMap[self.categoriesMap.keys()[0]][0].getImgPath()
        mode = example_path.split(os.sep)[0]
        return mode


    def getFrameIndexStrPart(self, frame_path):
        pass


    def getFrameIndex(self, frame_path):
        frameIndexStrPart = self.getFrameIndexStrPart(frame_path)
        frame_ind = int(re.findall(r'\d+', frameIndexStrPart)[0])
        return frame_ind

    def getFramePath(self, example_path, dst_frame_index):
        pass

    def getExamplePaths(self, seqName):
        pass

    def getFramePaths(self, seqName, dst_frame_index):
        example_paths = self.getExamplePaths(seqName)
        return DatasetPaths(self.getFramePath(example_paths.getImgPath(), dst_frame_index),
                            self.getFramePath(example_paths.getGTPath(), dst_frame_index))

    def getEndFrameInd(self, seqName):
        return self.getFrameIndex(self.categoriesMap[seqName][-1].getImgPath())