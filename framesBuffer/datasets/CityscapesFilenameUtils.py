import os
from framesBuffer.datasets.FilenameUtils import FilenameUtils


class CityscapesFilenameUtils(FilenameUtils):

    def __init__(self, txt):
        super(CityscapesFilenameUtils, self).__init__(txt)


    def getExamplePaths(self, seqName):
        example_paths = self.categoriesMap[seqName][0]
        return example_paths


    def getCatagory(self, frame_path):
        names = frame_path.split(os.sep)[-1].split('_')
        catagory = names[0]+'_'+names[1]
        return catagory


    def getFrameIndexStrPart(self, frame_path):
        return frame_path.split(os.sep)[-1].split('_')[2]


    def getStartFrameInd(self, seqName):
        return 0


    def getFramePath(self, example_path, dst_frame_index):
        separator = '_'
        frameIndDigitsNum = 6

        example_path_parts = example_path.split(separator)
        dst_frame_index_str_part = '0' * (frameIndDigitsNum - len(str(dst_frame_index))) + str(dst_frame_index)
        prefix_path = separator.join(example_path_parts[0:2])
        post_path = separator.join(example_path_parts[3:])
        dst_frame_path = prefix_path + separator + dst_frame_index_str_part + separator + post_path
        return dst_frame_path