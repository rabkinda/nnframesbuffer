import os
import argparse
from cityscapesScripts.cityscapesscripts.preparation import createTrainIdLabelImgs

parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--annotation_dir', type=str, help='annotation_dir')
args = parser.parse_args()

os.environ['CITYSCAPES_DATASET'] = args.annotation_dir#'/media/rabkinda/Gal_Backup/cityscapes/gt'

createTrainIdLabelImgs.main()