import random
import numpy as np
import torch.utils.data as torchdata
from framesBuffer.datasets.DatasetFactory import DatasetFactory


class DatasetOuter(torchdata.Dataset):
    def __init__(self, txt, opt, max_sample=-1, is_train=1):
        self.imgSize = (opt.imgSize, opt.imgSize)
        self.segSize = (opt.segSize, opt.segSize)
        self.use_flip = opt.use_flip
        self.opt = opt
        self.is_train = is_train
        self.imgOriginalShape = DatasetFactory.getConstants(opt.dataset).img_original_size
        self.filenameUtils = DatasetFactory.getFilenameUtils(opt.dataset, txt)
        self.epoch = 1

        self.list_sample = self.filenameUtils.getListSample()
        self.categoriesMap = self.filenameUtils.getCategoriesMap()
        self.categories = self.categoriesMap.keys()

        self.ranges = {}

        for c in self.categories:
            startInd = self.filenameUtils.getStartFrameInd(c)
            endInd = self.filenameUtils.getEndFrameInd(c)

            self.ranges[c] = (startInd, endInd)

        if self.is_train:
            random.shuffle(self.list_sample)
        if max_sample > 0:
            self.list_sample = self.list_sample[0:max_sample]
        num_sample = len(self.list_sample)
        assert num_sample > 0
        print('# samples: {}'.format(num_sample))


    def _get_scale_and_crop_params(self, imgShape, cropSize, is_train):
        h, w = imgShape[0], imgShape[1]

        if is_train:
            # random scale
            scale = random.random() + 0.5     # 0.5-1.5
            scale = max(scale, 1. * min(cropSize) / min(h, w))
        else:
            # scale to crop size
            scale = 1. * min(cropSize) / min(h, w)

        img_scale_shape = [int(h*scale), int(w*scale)]

        h_s, w_s = img_scale_shape[0], img_scale_shape[1]
        if is_train:
            # random crop
            x1 = random.randint(0, w_s - cropSize[1])
            y1 = random.randint(0, h_s - cropSize[0])
        else:
            # center crop
            x1 = (w_s - cropSize[1]) // 2
            y1 = (h_s - cropSize[0]) // 2

        return scale, x1, y1

    def setEpoch(self, epoch):
        self.epoch = epoch

    def getPrevFramesNum(self):
        if self.opt.prev_seq_len_type == 'random':
            return random.randint(1, self.opt.prev_frames_num)
        elif self.opt.prev_seq_len_type == 'curriculum':
            return min(max(self.epoch//self.opt.curriculum_length_increase_every, 1), self.opt.prev_frames_num)
        else:
            return self.opt.prev_frames_num


    def __getitem__(self, index):
        keyframe_relpath = self.list_sample[index]
        seqName = self.filenameUtils.getCatagory(keyframe_relpath.getImgPath())
        seqRangeTuple = self.ranges[seqName]
        keyframe_ind = self.filenameUtils.getFrameIndex(keyframe_relpath.getImgPath())
        curr_prev_frames_num = self.getPrevFramesNum()
        rangeTuple = (max(keyframe_ind-curr_prev_frames_num, seqRangeTuple[0]), keyframe_ind)
        flip = False

        if self.imgSize[0] < 0:
            self.imgSize = self.imgOriginalShape

        scale, x1, y1 = self._get_scale_and_crop_params(self.imgOriginalShape, cropSize=self.imgSize, is_train=self.is_train)
        if self.use_flip and self.is_train and random.choice([-1, 1]) > 0:
            flip = True

        length = np.abs(rangeTuple[1] - rangeTuple[0]) + 1
        return keyframe_relpath.toList(), seqName, scale, x1, y1, flip, rangeTuple, length


    def __len__(self):
        return len(self.list_sample)
