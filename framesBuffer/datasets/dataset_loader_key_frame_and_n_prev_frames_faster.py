# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
import math
import os
import numpy as np
import torch
from PIL import Image
from scipy.misc import imresize, imread
from torchvision import transforms
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.datasets.DatasetPaths import DatasetPaths
from framesBuffer.datasets.ModifiedColorJitter import ColorJitter
from framesBuffer.semanticSegmentation.fetch_and_transform_img_flow_data import fetch_flo_data, fetch_flo


class TBPTTIter(object):
    """
    Iterator for truncated batch propagation through time(tbptt) training.
    Target sequence is segmented while input sequence remains the same.
    """
    def __init__(self, txt, opt, keyframe_relpath, seqNameBatch, scale, x1, y1, flip, rangeTuple, length, seq_len, pool):
        self.dataset_constants = DatasetFactory.getConstants(opt.dataset)
        self.imgOriginalShape = self.dataset_constants.img_original_size
        self.seq_len = seq_len
        self.start = True
        self.end = False

        self.keyframe_relpath = keyframe_relpath
        self.scaleBatch = scale
        self.x1Batch = x1
        self.y1Batch = y1
        self.flipBatch = flip
        self.lengthBatch = length
        self.rangeTupleBatch = rangeTuple
        self.seqNameBatch = seqNameBatch

        self.filenameUtils = DatasetFactory.getFilenameUtils(opt.dataset, txt)

        # split batch
        batch_size = len(self.seqNameBatch)
        arr = np.array([torch.max(self.lengthBatch)*[-1]]*batch_size)
        for b in xrange(0, batch_size, 1):
            arr[b,:self.lengthBatch[b]] = np.array(range(self.rangeTupleBatch[0][b], self.rangeTupleBatch[1][b] + 1, 1))

        self.dataBatch = torch.from_numpy(arr.T)
        self.dataBatch = list(torch.split(self.dataBatch, self.seq_len, 0))
        self.dataBatch.reverse()
        self.len = len(self.dataBatch)

        # split length list
        batch_seq_len = len(self.dataBatch)
        self.dataLengths = [self.split_length(l, batch_seq_len) for l in self.lengthBatch]
        self.dataLengths = torch.stack(self.dataLengths)
        self.dataLengths = list(torch.split(self.dataLengths, 1, 1))
        self.dataLengths = [x.squeeze() for x in self.dataLengths]
        self.dataLengths.reverse()

        assert len(self.dataLengths) == len(self.dataBatch)

        self.opt = opt
        self.root_img = opt.root_img
        self.root_seg = opt.root_seg
        self.imgSize = self.getCropSize()
        self.segSize = self.getCropSize()
        self.num_class = opt.num_class

        if opt.use_my_cj:
            self.cj = ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1)
        else:
            self.cj = transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1)

        self.seed_for_cj = [np.random.randint(0,2**32) for _ in xrange(0, batch_size, 1)]

        # mean and std for resnet image
        self.img_transform = transforms.Compose([
            transforms.Normalize(mean=self.dataset_constants.mean,
                                 std=self.dataset_constants.std)])

        self.mode = self.filenameUtils.getMode()
        self.seqNameWithPrefixBatch = [os.path.join(self.mode, seq) for seq in self.seqNameBatch]

        self.pool = pool


    def getCropSize(self):
        cropSize = np.array(self.imgOriginalShape)

        if self.opt.imgSize > 0:
            cropSize = np.array([self.opt.imgSize, self.opt.imgSize])

        if self.opt.use_fixed_crop_buffer:
            cropSize = np.array([self.dataset_constants.crop_size, self.dataset_constants.crop_size])

        return cropSize


    def _apply_scale_and_crop(self, img, seg, scale, x1, y1, cropSize):

        img_scale = imresize(img, scale, interp='bilinear')
        seg_scale = imresize(seg, scale, interp='nearest')

        img_crop = img_scale[y1: y1 + cropSize[0], x1: x1 + cropSize[1], :]
        seg_crop = seg_scale[y1: y1 + cropSize[0], x1: x1 + cropSize[1]]

        return img_crop, seg_crop


    def _flip(self, img, seg):
        img_flip = img[:, ::-1, :]
        seg_flip = seg[:, ::-1]
        return img_flip, seg_flip


    def split_length(self, seq_size, batch_seq_len):
        seq = [self.seq_len] * (seq_size / self.seq_len)
        if seq_size % self.seq_len != 0:
            seq += [seq_size % self.seq_len]
        seq += [0] * (batch_seq_len - len(seq))
        return torch.LongTensor(seq)


    def getImageData(self, imgFilenames, i):

        isDummyData = False
        label_exist = 1
        path_img = os.path.join(self.root_img, imgFilenames.getImgPath())
        path_seg = os.path.join(self.root_seg, imgFilenames.getGTPath())

        add_noise_cj = self.mode in 'training' and self.opt.use_data_augmentation

        # load image
        try:
            img = Image.open(path_img)#imread(path_img, mode='RGB')

            if self.opt.use_cj and add_noise_cj:
                np.random.seed(self.seed_for_cj[i])
                img = self.cj(img)

            img = np.array(img)
            assert (img.ndim == 3)
        except:
            # dummy data
            img = np.zeros((self.imgOriginalShape[0], self.imgOriginalShape[1], 3)).astype(np.uint8)
            isDummyData = True

        # load label
        try:
            seg = imread(path_seg)
            assert (seg.ndim == 2)
        except:
            # dummy data
            seg = self.dataset_constants.void_labels[0] * np.ones(self.imgOriginalShape).astype(np.uint8)
            label_exist = 0

        assert (img.shape[0] == seg.shape[0])
        assert (img.shape[1] == seg.shape[1])

        # random scale, crop, flip
        if self.opt.use_data_augmentation and min(self.imgSize) > 0 and min(self.segSize) > 0:
            if self.flipBatch[i]:
                img, seg = self._flip(img, seg)

            img, seg = self._apply_scale_and_crop(img, seg, self.scaleBatch[i], self.x1Batch[i], self.y1Batch[i], self.imgSize)

        # image to float
        img = img.transpose((2, 0, 1))
        img = img.astype(np.float32)

        if self.opt.use_official_pspnet:
            if (not self.opt.isDummyDataMatter) or (not isDummyData):
                img -= np.array(self.dataset_constants.mean_caffe)[:, None, None]
            img = np.copy(img[::-1, :, :])
        else:
            img = img / 255.

        # substracted by mean and divided by std
        img = torch.from_numpy(img)

        if not isDummyData:
            if not self.opt.use_official_pspnet:
                img = self.img_transform(img)

            if self.opt.use_noise and add_noise_cj:
                # add noise to image
                std = torch.std(img) * 0.005
                noise = np.random.normal(loc=0, scale=std, size=list(img.size()))
                img += torch.from_numpy(noise).float()

        seg = seg.astype(np.int)

        # to torch tensor
        seg = torch.from_numpy(seg)

        return img.unsqueeze(0), seg.unsqueeze(0), label_exist


    def get_scaled_flow(self, frame_t_ind, frame_tmi_ind, seqName, scale, x1, y1, flip, rangeTuple, args):
        flow_path = os.path.join(args.root_optical_flow, seqName, str(frame_t_ind) + '_' + str(frame_tmi_ind) + '.flo')
        if flip:
            flow_path = os.path.join(args.root_optical_flow, seqName, str(frame_t_ind) + '_' + str(frame_tmi_ind) + '_flipped.flo')

        if frame_tmi_ind >= rangeTuple[0]:
            flo = fetch_flo_data(flow_path, frame_t_ind, frame_tmi_ind, seqName.split(os.sep)[1], flip, self.filenameUtils, self.opt)
        else:
            shape = (self.imgOriginalShape[0], self.imgOriginalShape[1], 2)
            if args.use_descriptor_div_8:
                shape = (shape[0]/8, shape[1]/8, 2)
            flo = np.zeros(shape)

        flo = fetch_flo(flo, scale, x1, y1, self.imgSize, self.opt)
        flo = torch.from_numpy(flo).type(torch.FloatTensor)
        return flo.unsqueeze(0)


    def __next__(self):
        if len(self.dataBatch) == 0:
            raise StopIteration()

        if self.len > len(self.dataBatch):
            self.start = False

        currDataBatch = self.dataBatch.pop().t()

        self.end = len(self.dataBatch)==0

        batch_size = len(self.seqNameBatch)
        curr_seq_len = len(currDataBatch[0])
        srcBatch = torch.zeros(batch_size, curr_seq_len, 3, self.imgSize[0], self.imgSize[1])
        prevImgBatch = torch.zeros(batch_size, curr_seq_len, 3, self.imgSize[0], self.imgSize[1])
        tgtBatch = torch.zeros(batch_size, curr_seq_len, self.imgSize[0], self.imgSize[1]).long()
        labelExistanceBatch = torch.zeros(batch_size, curr_seq_len)
        opticalFlowBatch = torch.zeros(batch_size, curr_seq_len, 2, self.imgSize[0], self.imgSize[1])
        imgFilenameBatch = np.array([[None]*curr_seq_len] * batch_size)

        def data_func(param):
            b, ind, _ = param
            seq_name = self.seqNameBatch[b]
            seq_name_with_prefix = self.seqNameWithPrefixBatch[b]
            curr_keyframe_relpath = [p[b] for p in self.keyframe_relpath]
            curr_keyframe_relpath = DatasetPaths(curr_keyframe_relpath[0], curr_keyframe_relpath[-1])

            if ind>=0:
                imgFilenames = self.filenameUtils.getFramePaths(seq_name, ind)
            else:
                #fill with 0 for img and 11 for seg- dummy data
                imgFilenames = DatasetPaths('', '')

            img, seg, seg_exist = self.getImageData(imgFilenames, b)
            seg_exist = seg_exist*(self.filenameUtils.getFrameIndex(curr_keyframe_relpath.getImgPath()) == ind)

            currRangeTuple = [r[b] for r in self.rangeTupleBatch]

            frame_t_ind = ind
            frame_tm1_ind = -1 if currRangeTuple[0]==ind else ind-1

            img_tm1, _, _ = self.getImageData(self.filenameUtils.getFramePaths(seq_name, frame_tm1_ind), b)

            flow = self.get_scaled_flow(frame_t_ind, frame_tm1_ind, seq_name_with_prefix,
                                 self.scaleBatch[b], self.x1Batch[b], self.y1Batch[b], self.flipBatch[b]
                                        , currRangeTuple, self.opt)

            height_resize, width_resize = img.size()[2:]
            CROP_SIZE = self.imgSize

            if height_resize < CROP_SIZE[0] or width_resize < CROP_SIZE[1]:
                img = img.numpy()
                img_tm1 = img_tm1.numpy()
                seg = seg.numpy()
                flow = flow.numpy()

                if height_resize < CROP_SIZE[0]:
                    img = np.lib.pad(img, ((0, 0), (0, 0), (0, CROP_SIZE[0] - height_resize), (0, 0)), 'constant', constant_values=0)
                    img_tm1 = np.lib.pad(img_tm1, ((0, 0), (0, 0), (0, CROP_SIZE[0] - height_resize), (0, 0)), 'constant', constant_values=0)
                    seg = np.lib.pad(seg, ((0, 0), (0, CROP_SIZE[0] - height_resize), (0, 0)), 'constant', constant_values=0)
                    flow = np.lib.pad(flow, ((0, 0), (0, 0), (0, CROP_SIZE[0] - height_resize), (0, 0)), 'constant', constant_values=0)
                if width_resize < CROP_SIZE[1]:
                    img = np.lib.pad(img, ((0, 0), (0, 0), (0, 0), (0, CROP_SIZE[1] - width_resize)), 'constant', constant_values=0)
                    img_tm1 = np.lib.pad(img_tm1, ((0, 0), (0, 0), (0, 0), (0, CROP_SIZE[1] - width_resize)), 'constant', constant_values=0)
                    seg = np.lib.pad(seg, ((0, 0), (0, 0), (0, CROP_SIZE[1] - width_resize)), 'constant', constant_values=0)
                    flow = np.lib.pad(flow, ((0, 0), (0, 0), (0, 0), (0, CROP_SIZE[1] - width_resize)), 'constant', constant_values=0)

                img = torch.from_numpy(img)
                img_tm1 = torch.from_numpy(img_tm1)
                seg = torch.from_numpy(seg)
                flow = torch.from_numpy(flow)

            return img, img_tm1, seg, flow, seg_exist, imgFilenames.getImgPath()

        param_list = [(b, s, s_index)
                    for b in xrange(0, batch_size, 1)
                    for s_index, s in enumerate(currDataBatch[b])]
        res = self.pool.map(data_func, param_list)

        for index, (b, _, s_index) in enumerate(param_list):
            srcBatch[b, s_index] = res[index][0].squeeze(0)
            prevImgBatch[b, s_index] = res[index][1].squeeze(0)
            tgtBatch[b, s_index] = res[index][2].squeeze(0)
            opticalFlowBatch[b, s_index] = res[index][3].squeeze(0)
            labelExistanceBatch[b, s_index] = res[index][4]
            imgFilenameBatch[b, s_index] = res[index][5]

        srcBatch = srcBatch.transpose(0, 1)
        prevImgBatch = prevImgBatch.transpose(0, 1)
        tgtBatch = tgtBatch.transpose(0, 1)
        opticalFlowBatch = opticalFlowBatch.transpose(0, 1)
        labelExistanceBatch = labelExistanceBatch.transpose(0, 1).type(torch.FloatTensor)

        return srcBatch, tgtBatch, prevImgBatch, labelExistanceBatch, opticalFlowBatch, self.dataLengths.pop(), currDataBatch, imgFilenameBatch, self.seqNameWithPrefixBatch, self.start, self.end

    next = __next__

    def __iter__(self):
        return self

    def __len__(self):
        return self.len


class TBPTTIterOuter(object):
    """
    Iterator for truncated batch propagation through time(tbptt) training.
    Target sequence is segmented while input sequence remains the same.
    """
    def __init__(self, txt, opt, keyframe_relpath, seqNameBatch, scale, x1, y1, flip, rangeTuple, length, seq_len, pool):
        self.curr_iter = None
        self.iter_num = 0
        self.real_batch = opt.batch_size
        full_batch_size = len(seqNameBatch)
        self.end_iter = int(math.ceil(full_batch_size*1.0/self.real_batch))

        self.txt = txt
        self.opt = opt
        self.keyframe_relpath = keyframe_relpath
        self.seqNameBatch = seqNameBatch
        self.scale = scale
        self.x1 = x1
        self.y1 = y1
        self.flip = flip
        self.rangeTuple = rangeTuple
        self.length = length
        self.seq_len = seq_len
        self.pool = pool

    @staticmethod
    def _get_by_ind(l, beg_ind, end_ind):
        return [p[beg_ind:end_ind] for p in l]

    def _get_current_iter(self):
        beg_ind = self.iter_num * self.real_batch
        end_ind = beg_ind + self.real_batch
        return TBPTTIter(self.txt, self.opt,
                         self._get_by_ind(self.keyframe_relpath, beg_ind,  end_ind),
                         self.seqNameBatch[beg_ind: end_ind],
                         self.scale[beg_ind: end_ind],
                         self.x1[beg_ind: end_ind],
                         self.y1[beg_ind: end_ind],
                         self.flip[beg_ind: end_ind],
                         self._get_by_ind(self.rangeTuple, beg_ind, end_ind),
                         self.length[beg_ind: end_ind],
                         self.seq_len, self.pool)

    def __next__(self):
        try:
            if self.curr_iter is None:
                self.curr_iter = self._get_current_iter()
            item = self.curr_iter.next()
            internal_batch_index = (self.iter_num * self.real_batch,)
            return internal_batch_index + item
        except StopIteration:
            self.iter_num += 1
            if self.iter_num < self.end_iter:
                self.curr_iter = None
                return self.__next__()
            else:
                raise StopIteration()

    next = __next__

    def __iter__(self):
        return self
