
class DatasetPaths(object):

    def __init__(self, img_path, gt_path):
        self.img_path = img_path
        self.gt_path = gt_path

    def getGTPath(self):
        return self.gt_path

    def getImgPath(self):
        return self.img_path

    def toList(self):
        return [self.img_path, self.gt_path]