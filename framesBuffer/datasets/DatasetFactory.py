from framesBuffer.datasets.CamVidFilenameUtils import CamVidFilenameUtils
from framesBuffer.datasets.CityscapesFilenameUtils import CityscapesFilenameUtils
from semanticSegmentationPytorchFineTuned import CamVidConstants, CityscapesConstants
from enum import Enum


class DatasetFactory(object):

    DATASET_TYPES = Enum('DATASET_TYPES', 'camvid cityscapes')

    @staticmethod
    def getFilenameUtils(dataset, txt):
        if (dataset == DatasetFactory.DATASET_TYPES.camvid.name):
            return CamVidFilenameUtils(txt)
        elif (dataset == DatasetFactory.DATASET_TYPES.cityscapes.name):
            return CityscapesFilenameUtils(txt)
        else:
            return None


    @staticmethod
    def getConstants(dataset):
        if (dataset == DatasetFactory.DATASET_TYPES.camvid.name):
            return CamVidConstants
        elif (dataset == DatasetFactory.DATASET_TYPES.cityscapes.name):
            return CityscapesConstants
        else:
            return None