# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import math
from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.semanticSegmentation.utils import getFinalSegSize, isUpsampleNeeded
from framesBuffer.utils import wrap


def getConv3DSeq(mem_feat_depth, mem_size, spatial_kernel_size, spatial_padding, dropoutProb, opt):
    time_kernel_conv1 = int(math.ceil((1.0*mem_size) / opt.time_division))
    time_kernel_conv2 = (mem_size+1 - (time_kernel_conv1 - 1)) if opt.use_padding else (mem_size - (time_kernel_conv1 - 1))
    i=1

    conv = nn.Sequential(
                  nn.Conv3d(mem_feat_depth, mem_feat_depth
                    , kernel_size=(time_kernel_conv1, spatial_kernel_size, spatial_kernel_size)
                    , padding=(0, spatial_padding, spatial_padding)))

    if opt.use_batch_norm:
        conv.add_module(name=str(i), module=nn.BatchNorm3d(mem_feat_depth))
        i += 1

    conv.add_module(name=str(i), module=nn.ReLU(inplace=True))
    i += 1

    if opt.use_dropout:
        conv.add_module(name=str(i), module=nn.Dropout(p=dropoutProb, inplace=True))
        i += 1

    conv.add_module(name=str(i),
                    module=nn.Conv3d(mem_feat_depth, mem_feat_depth
                            , kernel_size=(time_kernel_conv2, spatial_kernel_size, spatial_kernel_size)
                            , padding=(0, spatial_padding, spatial_padding)))
    return conv


def getConv2DSeq(mem_feat_depth, mem_size, spatial_kernel_size, spatial_padding, dropoutProb, opt):
    reduce_factor = min(mem_size / opt.time_division, 1)

    conv = nn.Sequential(
                  nn.Conv2d(mem_feat_depth*mem_size, reduce_factor*mem_feat_depth
                    , kernel_size=spatial_kernel_size
                    , padding=spatial_padding),
                  nn.ReLU(inplace=True))

    if opt.use_dropout:
        conv.add_module(name='2', module=nn.Dropout(p=dropoutProb, inplace=True))

    conv.add_module(name='3' if opt.use_dropout else '2',
                    module=nn.Conv2d(reduce_factor*mem_feat_depth, mem_feat_depth
                            , kernel_size=spatial_kernel_size
                            , padding=spatial_padding))
    return conv


class Aggr_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size, dropoutProb):
        super(Aggr_Module, self).__init__()

        self.opt = opt
        self.spatial_kernel_size = opt.spatial_kernel_size
        self.spatial_padding = self.spatial_kernel_size // 2

        if self.opt.use_conv3d:
            self.convSeq = getConv3DSeq(mem_feat_depth, mem_size, self.spatial_kernel_size, self.spatial_padding, dropoutProb, opt)
        else:
            self.convSeq = getConv2DSeq(mem_feat_depth, mem_size, self.spatial_kernel_size, self.spatial_padding, dropoutProb, opt)

    def forward(self, S_t):
        # [batch_size, channels, h, w, mem_size]->[batch_size, channels, mem_size, h, w] - for 3D
        convBufferInput = S_t.permute(0, 1, 4, 2, 3).contiguous()

        if self.opt.use_conv3d:
            if self.opt.use_padding:
                padding = wrap(self.opt.use_cuda, torch.zeros(convBufferInput.size()[:2]+convBufferInput.size()[3:]).unsqueeze(2))
                convBufferInput = torch.cat([padding, convBufferInput], dim=2)
            u = self.convSeq(convBufferInput)
            u = u.squeeze(2)
        else:
            batch_size, channels, mem_size, h, w = convBufferInput.size()
            u = self.convSeq(convBufferInput.view(batch_size, channels * mem_size, h, w))

        return u


class N_u_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(N_u_Module, self).__init__()

        self.opt = opt
        dropoutProb = 0
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size, dropoutProb)

    def forward(self, S_t):
        u = self.aggr_module(S_t)

        if self.opt.state_with_softmax:
            u = nn.functional.softmax(u, dim=1)

        return u


class N_o_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size, use_softmax=False):
        super(N_o_Module, self).__init__()

        self.use_softmax = use_softmax
        self.opt = opt
        dropoutProb = 0.2
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size, dropoutProb)
        self.segSize = getFinalSegSize(opt)

    def forward(self, S_t):
        u = self.aggr_module(S_t)

        if isUpsampleNeeded(self.opt, self.segSize, u.size()):
            u = nn.functional.upsample(u, size=self.segSize, mode='bilinear')

        if self.use_softmax:
            u = nn.functional.softmax(u, dim=1)
        else:
            u = nn.functional.log_softmax(u, dim=1)

        return u


class Decoder(nn.Module):
    def __init__(self, opt, use_softmax):
        super(Decoder, self).__init__()
        self.mem_feat_depth = opt.num_class
        self.mem_size = opt.mem_size
        self.Warp = GridSamplerModule()
        self.N_u = N_u_Module(opt, self.mem_feat_depth, self.mem_size)
        self.N_o = N_o_Module(opt, self.mem_feat_depth, self.mem_size, use_softmax=use_softmax)
        self.num_class = opt.num_class
        self.use_cuda = opt.use_cuda
        self.opt = opt

        if self.opt.include_optical_flow_confidence:
            self.kernel_size = opt.spatial_kernel_size
            self.ConvRtGate = nn.Conv2d(3, self.num_class, kernel_size=self.kernel_size, padding=self.kernel_size//2)


    def init_buffer(self, batch_size, h, w, start=True):
        if start:
            self.S_t = Variable(torch.zeros((batch_size, self.mem_feat_depth, h, w, self.mem_size)))
            if self.use_cuda:
                self.S_t = self.S_t.cuda()


    def update_buffer(self, S_tm1, c_t, prev_img_var, flow_var, image_t):
        if self.opt.use_descriptor_div_8:
            image_t = F.avg_pool2d(image_t, 8)
            prev_img_var = F.avg_pool2d(prev_img_var, 8)
            #flow_var = F.avg_pool2d(flow_var, 8)
            #flow_var = flow_var*(1.0/8.0)

        #apply optical flow on each buffer entry- to make buffer represent time t
        Sp = self.apply_opticalflow_warp_on_buffer(S_tm1[:, :, :, :, :-1], flow_var)

        if self.opt.include_optical_flow_confidence:
            opticalFlowRememberGate = self.getOpticalFlowConfidence(image_t, prev_img_var, flow_var)
            Sp = torch.mul(opticalFlowRememberGate.unsqueeze(4), Sp)

        z_t = c_t.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([z_t, Sp], 4)
        else:
            S = z_t

        # update S
        u = self.N_u(S)
        u = u.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([u, Sp], 4)
        else:
            S = u

        return S


    def apply_opticalflow_warp_on_buffer(self, S_tm1, flow):
        _S_tm1_after_applying_warp = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_buffer_descriptor = S_tm1[:, :, :, :, i].contiguous()

            curr_buffer_descriptor_warped = self.Warp(curr_buffer_descriptor, flow).unsqueeze(4)

            if _S_tm1_after_applying_warp is None:
                _S_tm1_after_applying_warp = curr_buffer_descriptor_warped
            else:
                _S_tm1_after_applying_warp = torch.cat([_S_tm1_after_applying_warp, curr_buffer_descriptor_warped], 4)

        return _S_tm1_after_applying_warp


    def getOpticalFlowConfidence(self, img_t, prev_img, optical_flow):

        prev_img_warped = self.Warp(prev_img, optical_flow)
        optical_flow_confidence = img_t - prev_img_warped
        rt = self.ConvRtGate(optical_flow_confidence)
        opticalFlowRememberGate = 1 - F.tanh(torch.abs(rt))

        if self.opt.use_op_conf_file:
            with open("op_conf_conv_buffer.txt", "a") as op_conf_file:
                op_conf_file.write(str(torch.mean(opticalFlowRememberGate).data.cpu().numpy()[0])+'\n')

        return opticalFlowRememberGate


    def forward(self, context, inputImg, prevImg, inputOpticalFlow, start=True):
        out = []

        seq_len, batch_size, _, h, w = context.size()
        self.init_buffer(batch_size, h, w, start)

        for seqInd in xrange(0, seq_len, 1):
            # update buffer
            c_t = context[seqInd]

            self.S_t = self.update_buffer(self.S_t, c_t, prevImg[seqInd], inputOpticalFlow[seqInd], inputImg[seqInd])

            # predict next time step based on buffer content
            o_t = self.N_o(self.S_t)
            out += [o_t]

        out_seq = torch.stack(out)
        return out_seq


class Loop(nn.Module):
    def __init__(self, opt, use_softmax=False):
        super(Loop, self).__init__()
        self.decoder = Decoder(opt, use_softmax)

    def get_params_lr_arr(self):
        params_lr_arr = [
            {'params': self.decoder.N_u.parameters(), 'lr': 1e-4},
            {'params': self.decoder.N_o.parameters(), 'lr': 1e-4},
        ]
        return params_lr_arr

    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)

    def forward(self, inputDescriptor, inputImg, prevImg, inputOpticalFlow, target, target_exist, imgFilename
                , img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, start=True):
        out = self.decoder(inputDescriptor, inputImg, prevImg, inputOpticalFlow, start)
        return out