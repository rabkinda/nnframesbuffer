# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.semanticSegmentation.fetch_and_transform_img_flow_data import get_scaled_img_flo_array
from framesBuffer.ConvGRU import ConvFlowBufferGRUCell


class Aggr_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(Aggr_Module, self).__init__()

        self.opt = opt
        self.convFlowBufferGRUCell = ConvFlowBufferGRUCell(opt, mem_feat_depth, mem_size, kernel_size=opt.spatial_kernel_size, cuda_flag=opt.use_cuda)

    def forward(self, S_t, optical_flow_confidence):
        # [batch_size, channels, h, w, mem_size]->[batch_size, channels, mem_size, h, w] - for 3D
        buffer = S_t.permute(0, 1, 4, 2, 3).contiguous()
        batch_size, channels, mem_size, h, w = buffer.size()

        input = buffer[:, :, 0, :, :]
        hiddenBuffer = buffer[:, :, 1:, :, :].contiguous()

        hiddenBuffer = hiddenBuffer.view(batch_size, channels * (mem_size-1), h, w)
        _optical_flow_confidence = optical_flow_confidence.view(batch_size, 3 * (mem_size-1), h, w)

        u = self.convFlowBufferGRUCell(input, hiddenBuffer, _optical_flow_confidence)

        return u


class N_u_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(N_u_Module, self).__init__()

        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t, optical_flow_confidence):
        u = self.aggr_module(S_t, optical_flow_confidence)

        if self.opt.state_with_softmax:
            u = nn.functional.softmax(u, dim=1)

        return u


class N_o_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size, use_softmax=False):
        super(N_o_Module, self).__init__()

        self.use_softmax = use_softmax
        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t, optical_flow_confidence):
        u = self.aggr_module(S_t, optical_flow_confidence)

        if self.use_softmax:
            u = nn.functional.softmax(u, dim=1)
        else:
            u = nn.functional.log_softmax(u, dim=1)

        return u


class Decoder(nn.Module):
    def __init__(self, opt, use_softmax):
        super(Decoder, self).__init__()
        self.mem_feat_depth = opt.num_class
        self.mem_size = opt.mem_size
        self.Warp = GridSamplerModule()
        self.N_u = N_u_Module(opt, self.mem_feat_depth, self.mem_size)
        self.N_o = N_o_Module(opt, self.mem_feat_depth, self.mem_size, use_softmax=use_softmax)
        self.num_class = opt.num_class
        self.use_cuda = opt.use_cuda
        self.opt = opt


    def init_buffer(self, batch_size, h, w, start=True):
        if start:
            self.S_t = Variable(torch.zeros((batch_size, self.mem_feat_depth, h, w, self.mem_size)))
            if self.use_cuda:
                self.S_t = self.S_t.cuda()


    def update_buffer(self, S_tm1, c_t, flow_var, optical_flow_confidence):
        #apply optical flow on each buffer entry- to make buffer represent time t
        Sp = self.apply_opticalflow_warp_on_buffer(S_tm1[:, :, :, :, :-1], flow_var)

        z_t = c_t.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([z_t, Sp], 4)
        else:
            S = z_t

        # update S
        u = self.N_u(S, optical_flow_confidence)
        u = u.unsqueeze(4)

        if self.mem_size > 1:
            S_t_after_warp = torch.cat([u, Sp], 4)
            S_t = torch.cat([u, S_tm1[:, :, :, :, :-1]], 4)
        else:
            S_t_after_warp = u
            S_t = u

        return S_t, S_t_after_warp


    def apply_opticalflow_warp_on_buffer(self, S_tm1, flow):
        _S_tm1_after_applying_warp = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_buffer_descriptor = S_tm1[:, :, :, :, i].contiguous()
            currFlow = flow[:, :, :, :, i]

            curr_buffer_descriptor_warped = self.Warp(curr_buffer_descriptor, currFlow).unsqueeze(4)

            if _S_tm1_after_applying_warp is None:
                _S_tm1_after_applying_warp = curr_buffer_descriptor_warped
            else:
                _S_tm1_after_applying_warp = torch.cat([_S_tm1_after_applying_warp, curr_buffer_descriptor_warped], 4)

        return _S_tm1_after_applying_warp


    def getOpticalFlowConfidence(self, img_t, prev_img, optical_flow):
        opticalFlowConfidence = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_prev_img = prev_img[:, :, :, :, i]
            currFlow = optical_flow[:, :, :, :, i]

            curr_prev_img_warped = self.Warp(curr_prev_img, currFlow)
            curr_optical_flow_confidence = img_t - curr_prev_img_warped
            curr_optical_flow_confidence = curr_optical_flow_confidence.unsqueeze(4)

            if opticalFlowConfidence is None:
                opticalFlowConfidence = curr_optical_flow_confidence
            else:
                opticalFlowConfidence = torch.cat([opticalFlowConfidence, curr_optical_flow_confidence], 4)

        # [batch_size, channels, h, w, mem_size]->[batch_size, channels, mem_size, h, w] - for 3D
        opticalFlowConfidence = opticalFlowConfidence.permute(0, 1, 4, 2, 3).contiguous()
        return opticalFlowConfidence


    def forward(self, context, inputImg, img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start=True):
        self.opt.include_optical_flow_confidence = 0
        out = []

        seq_len, batch_size, _, h, w = context.size()
        self.init_buffer(batch_size, h, w, start)

        for seqInd in xrange(0, seq_len, 1):
            # update buffer
            c_t = context[seqInd]

            # upload optical flow between each buffer location and frame time t
            flow_var, img_var = get_scaled_img_flo_array(img_ind[:, seqInd], seqNameWithPrefix, scale, x1, y1, flip
                                                         , self.mem_size, rangeTuple, not self.training, filenameUtils, self.opt)

            optical_flow_confidence = self.getOpticalFlowConfidence(inputImg[seqInd], img_var, flow_var)
            self.S_t, S_t_after_warp = self.update_buffer(self.S_t, c_t, flow_var, optical_flow_confidence)

            # predict next time step based on buffer content
            o_t = self.N_o(S_t_after_warp, optical_flow_confidence)
            out += [o_t]

        out_seq = torch.stack(out)
        return out_seq


class Loop(nn.Module):
    def __init__(self, opt, use_softmax=False):
        super(Loop, self).__init__()
        self.decoder = Decoder(opt, use_softmax)

    def get_params_lr_arr(self):
        params_lr_arr = [
            {'params': self.decoder.N_u.parameters(), 'lr': 1e-4},
            {'params': self.decoder.N_o.parameters(), 'lr': 1e-4},
        ]
        return params_lr_arr

    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)

    def forward(self, inputDescriptor, inputImg, prevImg, inputOpticalFlow, target, target_exist, imgFilename
                , img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start=True):
        out = self.decoder(inputDescriptor, inputImg, img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start)
        return out