# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
import torch
import torch.nn as nn
from framesBuffer.ConvGRU import ConvFlowGRUCell
from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.utils import wrap
import numpy as np


class Loop(nn.Module):
    def __init__(self, opt, use_softmax=False):
        super(Loop, self).__init__()
        self.convFlowGRUCell = ConvFlowGRUCell(opt=opt, input_size=opt.num_class, hidden_size=opt.num_class,
                                               kernel_size=opt.spatial_kernel_size, cuda_flag=opt.use_cuda)
        self.Warp = GridSamplerModule()
        self.external_use_softmax = use_softmax
        self.internal_use_softmax = True
        self.opt = opt


    def get_params_lr_arr(self):
        params_lr_arr = [
            {'params': self.convFlowGRUCell.parameters(), 'lr': 1e-4}
        ]
        return params_lr_arr


    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)


    def apply_opticalflow_warp(self, hidden_state, flow):
        hidden_state_warped = self.Warp(hidden_state, flow)
        return hidden_state_warped


    def getOpticalFlowConfidence(self, img_t, img_tm1, optical_flow):
        img_tm1_warped = self.apply_opticalflow_warp(img_tm1, optical_flow)
        return img_t - img_tm1_warped


    def forward(self, inputDescriptor, inputImg, prev_img, inputOpticalFlow, target, target_exist, imgFilename
                , img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, start=True):

        seq_len, batch_size, _, h, w = inputDescriptor.size()
        out = []
        self.initialize_states(inputDescriptor, start)

        for seqInd in xrange(0, seq_len, 1):
            optical_flow_confidence = self.getOpticalFlowConfidence(inputImg[seqInd], prev_img[seqInd], inputOpticalFlow[seqInd])
            self.state = self.apply_opticalflow_warp(self.state, inputOpticalFlow[seqInd])
            self.state = self.convFlowGRUCell(inputDescriptor[seqInd], self.state, optical_flow_confidence)

            if self.internal_use_softmax:
                o_t = nn.functional.softmax(self.state, dim=1)
            else:
                o_t = nn.functional.log_softmax(self.state, dim=1)

            if self.opt.state_with_softmax:
                self.state = o_t

            out += [o_t]

        out_seq = torch.stack(out)
        if self.internal_use_softmax and (not self.external_use_softmax):
            out_seq = torch.log(out_seq)
        return out_seq


    def initialize_states(self, inputDescriptor, start):
        if start:
            size_h = [inputDescriptor.size()[1], self.opt.num_class] + list(inputDescriptor.size()[3:])
            self.state = wrap(self.opt.use_cuda, torch.zeros(size_h))