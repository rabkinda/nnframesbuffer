# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import math

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.utils import wrap


class Aggr_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(Aggr_Module, self).__init__()

        self.opt = opt
        self.spatial_kernel_size = opt.spatial_kernel_size

        dilation_depth = int(math.ceil(math.log(mem_size, 2)))
        self.dilations = [2**i for i in range(0, dilation_depth, 1)]   #[1, 2, 4]

        if self.opt.include_optical_flow_confidence:
            self.conv_sigmoid = nn.ModuleList(
                [nn.Conv3d(in_channels=self.opt.num_class, out_channels=self.opt.num_class
                           , kernel_size=(2, self.spatial_kernel_size, self.spatial_kernel_size)
                           , padding=(0, self.getSpatialPadding(d), self.getSpatialPadding(d))
                           , dilation=(d, self.getSpatialDilation(d), self.getSpatialDilation(d)))
                 for d in self.dilations])

        self.conv_elu = nn.ModuleList(
            [nn.Conv3d(in_channels=self.opt.num_class, out_channels=self.opt.num_class
                       , kernel_size=(2, self.spatial_kernel_size, self.spatial_kernel_size)
                       , padding=(0, self.getSpatialPadding(d), self.getSpatialPadding(d))
                       , dilation=(d, self.getSpatialDilation(d), self.getSpatialDilation(d)))
             for d in self.dilations])

        self.skip_scale = nn.ModuleList(
            [nn.Conv3d(in_channels=self.opt.num_class, out_channels=self.opt.num_class, kernel_size=1)
             for d in self.dilations])
        self.residue_scale = nn.ModuleList(
            [nn.Conv3d(in_channels=self.opt.num_class, out_channels=self.opt.num_class, kernel_size=1)
             for d in self.dilations])

        self.conv_post_1 = nn.Conv3d(in_channels=self.opt.num_class, out_channels=self.opt.num_class, kernel_size=1)


    def getSpatialDilation(self, d):
        return d if self.opt.include_spatial_dilation else 1

    def getSpatialPadding(self, d):
        # (dilation*(size-1)//2)
        return d*(self.spatial_kernel_size-1)//2 if self.opt.include_spatial_dilation else self.spatial_kernel_size//2


    def forward(self, S_t, opticalFlowRememberGate):
        # [batch_size, channels, h, w, mem_size]->[batch_size, channels, mem_size, h, w] - for 3D
        convBufferInput = S_t.permute(0, 1, 4, 2, 3).contiguous()
        opticalFlowRememberGateInput = opticalFlowRememberGate.permute(0, 1, 4, 2, 3).contiguous()
        output = convBufferInput

        skip_connections = []  # save for generation purposes
        for s, e, skip_scale, residue_scale in zip(self.conv_sigmoid, self.conv_elu, self.skip_scale, self.residue_scale):
            output, skip = self.residue_forward(output, s, e, skip_scale, residue_scale, opticalFlowRememberGateInput)
            skip_connections.append(skip)
        # sum up skip connections
        output = sum([s[:, :, :output.size(2), :, :] for s in skip_connections])
        output = self.postprocess(output)

        return output


    def residue_forward(self, input, conv_sigmoid, conv_elu, skip_scale, residue_scale, opticalFlowRememberGateInput):
        output = input
        output_elu = conv_elu(output)

        if self.opt.include_optical_flow_confidence:
            output_sigmoid = conv_sigmoid(output)
            output = nn.functional.sigmoid(output_sigmoid) * nn.functional.elu(output_elu)
        else:
            output = opticalFlowRememberGateInput[:, :, :output_elu.size(2), :, :] * nn.functional.elu(output_elu)
            
        skip = skip_scale(output)
        output = residue_scale(output)
        output = output + input[:, :, :output.size(2), :, :]
        return output, skip


    def postprocess(self, input):
        output = nn.functional.elu(input)
        output = self.conv_post_1(output).squeeze(2)
        return output


class N_u_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(N_u_Module, self).__init__()

        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t, opticalFlowRememberGate):
        output = self.aggr_module(S_t, opticalFlowRememberGate)

        if self.opt.state_with_softmax:
            output = nn.functional.softmax(output, dim=1)
        return output


class N_o_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size, use_softmax=False):
        super(N_o_Module, self).__init__()

        self.use_softmax = use_softmax
        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t, opticalFlowRememberGate):
        output = self.aggr_module(S_t, opticalFlowRememberGate)

        if self.use_softmax:
            output = nn.functional.softmax(output, dim=1)
        else:
            output = nn.functional.log_softmax(output, dim=1)

        return output


class Decoder(nn.Module):
    def __init__(self, opt, use_softmax):
        super(Decoder, self).__init__()
        self.mem_feat_depth = opt.num_class
        self.mem_size = opt.mem_size
        self.Warp = GridSamplerModule()
        self.N_u = N_u_Module(opt, self.mem_feat_depth, self.mem_size)
        self.N_o = N_o_Module(opt, self.mem_feat_depth, self.mem_size, use_softmax=use_softmax)
        self.num_class = opt.num_class
        self.use_cuda = opt.use_cuda
        self.opt = opt

        self.kernel_size = opt.spatial_kernel_size
        self.ConvRtGate = nn.Conv2d(3, self.num_class, kernel_size=self.kernel_size, padding=self.kernel_size // 2)


    def init_buffer(self, batch_size, h, w, start=True):
        if start:
            self.S_t = Variable(torch.zeros((batch_size, self.mem_feat_depth, h, w, self.mem_size)))
            if self.use_cuda:
                self.S_t = self.S_t.cuda()


    def update_buffer(self, S_tm1, c_t, prev_img, flow, image_t):
        #apply optical flow on each buffer entry- to make buffer represent time t
        Sp = self.apply_opticalflow_warp_on_buffer(S_tm1[:, :, :, :, :-1], flow)

        opticalFlowRememberGate = self.getOpticalFlowConfidence(image_t, prev_img, flow).unsqueeze(4)

        if self.opt.include_optical_flow_confidence:
            Sp = torch.mul(opticalFlowRememberGate, Sp)
        else:
            opticalFlowRememberGateCellSize = opticalFlowRememberGate.size()
            opticalFlowRememberGate = torch.cat([wrap(self.opt.use_cuda, torch.ones(opticalFlowRememberGateCellSize))
                                                    , opticalFlowRememberGate.repeat(1, 1, 1, 1, self.mem_size - 1)], dim=4)

        z_t = c_t.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([z_t, Sp], 4)
        else:
            S = z_t

        # update S
        u = self.N_u(S, opticalFlowRememberGate)
        u = u.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([u, Sp], 4)
        else:
            S = u

        return S, opticalFlowRememberGate


    def apply_opticalflow_warp_on_buffer(self, S_tm1, flow):
        _S_tm1_after_applying_warp = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_buffer_descriptor = S_tm1[:, :, :, :, i].contiguous()

            curr_buffer_descriptor_warped = self.Warp(curr_buffer_descriptor, flow).unsqueeze(4)

            if _S_tm1_after_applying_warp is None:
                _S_tm1_after_applying_warp = curr_buffer_descriptor_warped
            else:
                _S_tm1_after_applying_warp = torch.cat([_S_tm1_after_applying_warp, curr_buffer_descriptor_warped], 4)

        return _S_tm1_after_applying_warp


    def getOpticalFlowConfidence(self, img_t, prev_img, optical_flow):

        prev_img_warped = self.Warp(prev_img, optical_flow)
        optical_flow_confidence = img_t - prev_img_warped
        rt = self.ConvRtGate(optical_flow_confidence)
        opticalFlowRememberGate = 1 - F.tanh(torch.abs(rt))
        return opticalFlowRememberGate


    def forward(self, context, inputImg, prevImg, inputOpticalFlow, S_t, start=True):
        out = []
        self.S_t = S_t

        seq_len, batch_size, _, h, w = context.size()

        for seqInd in xrange(0, seq_len, 1):
            # update buffer
            c_t = context[seqInd]

            self.S_t, opticalFlowRememberGate = self.update_buffer(self.S_t, c_t
                                                                       , prevImg[seqInd], inputOpticalFlow[seqInd], inputImg[seqInd])

            # predict next time step based on buffer content
            o_t = self.N_o(self.S_t, opticalFlowRememberGate)
            out += [o_t]

        out_seq = torch.stack(out)
        return out_seq, self.S_t


class Loop(nn.Module):
    def __init__(self, opt, use_softmax=False):
        super(Loop, self).__init__()
        self.decoder = Decoder(opt, use_softmax)

    def get_params_lr_arr(self):
        params_lr_arr = [
            {'params': self.decoder.N_u.parameters(), 'lr': 1e-4},
            {'params': self.decoder.N_o.parameters(), 'lr': 1e-4},
        ]
        return params_lr_arr

    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)

    def forward(self, inputDescriptor, inputImg, prevImg, inputOpticalFlow, S_t, start=True):
        inputDescriptor = inputDescriptor.transpose(0, 1)
        inputImg = inputImg.transpose(0, 1)
        prevImg = prevImg.transpose(0, 1)
        inputOpticalFlow = inputOpticalFlow.transpose(0, 1)

        out, S_t = self.decoder(inputDescriptor, inputImg, prevImg, inputOpticalFlow, S_t, start)

        out = out.transpose(0, 1)
        return out, S_t