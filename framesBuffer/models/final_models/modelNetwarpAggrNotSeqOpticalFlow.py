# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from framesBuffer.GridSamplerModule import GridSamplerModule
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.semanticSegmentation.fetch_and_transform_img_flow_data import get_scaled_img_flo_array
from framesBuffer.utils import wrap


class Aggr_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(Aggr_Module, self).__init__()

        self.mem_feat_depth = mem_feat_depth

        h_orig, w_orig = DatasetFactory.getConstants(opt.dataset).img_original_size
        if opt.use_data_augmentation and opt.imgSize > 0:
            h_orig, w_orig = opt.imgSize, opt.imgSize

        self.dummy_data = wrap(opt.use_cuda, torch.ones((1, 1, h_orig, w_orig)))
        self.conv_w = nn.Conv2d(1, self.mem_feat_depth * mem_size, kernel_size=1)

        if opt.checkpoint == '':
            self.conv_w.weight.data.fill_(0.5)
            self.conv_w.bias.data.fill_(0)

    def forward(self, S_t):
        _w = self.conv_w(self.dummy_data)
        _w = nn.functional.elu(_w)
        _w = torch.stack(torch.split(_w, split_size=self.mem_feat_depth, dim=1), dim=4)
        _S_t = _w * S_t
        u = torch.sum(_S_t, dim=4)
        return u


class N_u_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(N_u_Module, self).__init__()

        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t):
        u = self.aggr_module(S_t)

        if self.opt.state_with_softmax:
            u = nn.functional.softmax(u, dim=1)
        return u


class N_o_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size, use_softmax=False):
        super(N_o_Module, self).__init__()

        self.use_softmax = use_softmax
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t):
        u = self.aggr_module(S_t)

        if self.use_softmax:
            u = nn.functional.softmax(u, dim=1)
        else:
            u = nn.functional.log_softmax(u, dim=1)

        return u


class Decoder(nn.Module):
    def __init__(self, opt, use_softmax):
        super(Decoder, self).__init__()
        self.mem_feat_depth = opt.num_class
        self.mem_size = opt.mem_size
        self.Warp = GridSamplerModule()
        self.N_u = N_u_Module(opt, self.mem_feat_depth, self.mem_size)
        self.N_o = N_o_Module(opt, self.mem_feat_depth, self.mem_size, use_softmax=use_softmax)
        self.num_class = opt.num_class
        self.use_cuda = opt.use_cuda
        self.opt = opt

        if self.opt.include_optical_flow_confidence:
            self.kernel_size = opt.spatial_kernel_size
            self.ConvRtGate = nn.Conv3d(3, self.num_class, (1, self.kernel_size, self.kernel_size),
                                        padding=(0, self.kernel_size//2, self.kernel_size//2))


    def init_buffer(self, batch_size, h, w, start=True):
        if start:
            self.S_t = Variable(torch.zeros((batch_size, self.mem_feat_depth, h, w, self.mem_size)))
            if self.use_cuda:
                self.S_t = self.S_t.cuda()


    def update_buffer(self, S_tm1, c_t, prev_img_var, flow_var, image_t):
        #apply optical flow on each buffer entry- to make buffer represent time t
        Sp = self.apply_opticalflow_warp_on_buffer(S_tm1[:, :, :, :, :-1], flow_var)

        if self.opt.include_optical_flow_confidence:
            opticalFlowRememberGate = self.getOpticalFlowConfidence(image_t, prev_img_var, flow_var)
            Sp = torch.mul(opticalFlowRememberGate, Sp)

        z_t = c_t.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([z_t, Sp], 4)
        else:
            S = z_t

        # update S
        u = self.N_u(S)
        u = u.unsqueeze(4)

        if self.mem_size > 1:
            S_t_after_warp = torch.cat([u, Sp], 4)
            S_t = torch.cat([u, S_tm1[:, :, :, :, :-1]], 4)
        else:
            S_t_after_warp = u
            S_t = u

        return S_t, S_t_after_warp


    def apply_opticalflow_warp_on_buffer(self, S_tm1, flow):
        _S_tm1_after_applying_warp = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_buffer_descriptor = S_tm1[:, :, :, :, i].contiguous()
            currFlow = flow[:, :, :, :, i]

            curr_buffer_descriptor_warped = self.Warp(curr_buffer_descriptor, currFlow).unsqueeze(4)

            if _S_tm1_after_applying_warp is None:
                _S_tm1_after_applying_warp = curr_buffer_descriptor_warped
            else:
                _S_tm1_after_applying_warp = torch.cat([_S_tm1_after_applying_warp, curr_buffer_descriptor_warped], 4)

        return _S_tm1_after_applying_warp


    def getOpticalFlowConfidence(self, img_t, prev_img, optical_flow):
        opticalFlowConfidence = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_prev_img = prev_img[:, :, :, :, i]
            currFlow = optical_flow[:, :, :, :, i]

            curr_prev_img_warped = self.Warp(curr_prev_img, currFlow)
            curr_optical_flow_confidence = img_t - curr_prev_img_warped
            curr_optical_flow_confidence = curr_optical_flow_confidence.unsqueeze(4)

            if opticalFlowConfidence is None:
                opticalFlowConfidence = curr_optical_flow_confidence
            else:
                opticalFlowConfidence = torch.cat([opticalFlowConfidence, curr_optical_flow_confidence], 4)

        # [batch_size, channels, h, w, mem_size]->[batch_size, channels, mem_size, h, w] - for 3D
        convOpticalFlowConfidenceInput = opticalFlowConfidence.permute(0, 1, 4, 2, 3).contiguous()
        rt = self.ConvRtGate(convOpticalFlowConfidenceInput)
        opticalFlowRememberGate = 1 - F.tanh(torch.abs(rt))
        # [batch_size, channels, mem_size, h, w] -> [batch_size, channels, h, w, mem_size]
        opticalFlowRememberGate = opticalFlowRememberGate.permute(0, 1, 3, 4, 2).contiguous()
        return opticalFlowRememberGate


    def forward(self, context, inputImg, img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start=True):
        out = []

        seq_len, batch_size, _, h, w = context.size()
        self.init_buffer(batch_size, h, w, start)

        for seqInd in xrange(0, seq_len, 1):
            # update buffer
            c_t = context[seqInd]

            # upload optical flow between each buffer location and frame time t
            flow_var, img_var = get_scaled_img_flo_array(img_ind[:, seqInd], seqNameWithPrefix, scale, x1, y1, flip
                                                         , self.mem_size, rangeTuple, not self.training, filenameUtils, self.opt)

            self.S_t, S_t_after_warp = self.update_buffer(self.S_t, c_t, img_var, flow_var, inputImg[seqInd])

            # predict next time step based on buffer content
            o_t = self.N_o(S_t_after_warp)
            out += [o_t]

        out_seq = torch.stack(out)
        return out_seq


class Loop(nn.Module):
    def __init__(self, opt, use_softmax=False):
        super(Loop, self).__init__()
        self.decoder = Decoder(opt, use_softmax)

    def get_params_lr_arr(self):
        params_lr_arr = [
            {'params': self.decoder.N_u.parameters(), 'lr': 1e-4},
            {'params': self.decoder.N_o.parameters(), 'lr': 1e-4},
        ]
        return params_lr_arr

    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)

    def forward(self, inputDescriptor, inputImg, prevImg, inputOpticalFlow, target, target_exist, imgFilename
                , img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start=True):
        out = self.decoder(inputDescriptor, inputImg, img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start)
        return out