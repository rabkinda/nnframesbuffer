# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from framesBuffer.ConvGRU import ConvFlowBufferGRUCell
from framesBuffer.GridSamplerModule import GridSamplerModule


class Aggr_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(Aggr_Module, self).__init__()

        self.opt = opt
        self.convFlowBufferGRUCell = ConvFlowBufferGRUCell(opt, mem_feat_depth, mem_size, kernel_size=opt.spatial_kernel_size, cuda_flag=opt.use_cuda)

    def forward(self, S_t, optical_flow_confidence):
        # [batch_size, channels, h, w, mem_size]->[batch_size, channels, mem_size, h, w] - for 3D
        buffer = S_t.permute(0, 1, 4, 2, 3).contiguous()
        batch_size, channels, mem_size, h, w = buffer.size()

        input = buffer[:, :, 0, :, :]
        hiddenBuffer = buffer[:, :, 1:, :, :].contiguous()

        hiddenBuffer = hiddenBuffer.view(batch_size, channels * (mem_size-1), h, w)

        u = self.convFlowBufferGRUCell(input, hiddenBuffer, optical_flow_confidence)

        return u


class N_u_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size):
        super(N_u_Module, self).__init__()

        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t, optical_flow_confidence):
        u = self.aggr_module(S_t, optical_flow_confidence)

        if self.opt.state_with_softmax:
            u = nn.functional.softmax(u, dim=1)

        return u


class N_o_Module(nn.Module):
    def __init__(self, opt, mem_feat_depth, mem_size, use_softmax=False):
        super(N_o_Module, self).__init__()

        self.use_softmax = use_softmax
        self.opt = opt
        self.aggr_module = Aggr_Module(opt, mem_feat_depth, mem_size)

    def forward(self, S_t, optical_flow_confidence):
        u = self.aggr_module(S_t, optical_flow_confidence)

        if self.use_softmax:
            u = nn.functional.softmax(u, dim=1)
        else:
            u = nn.functional.log_softmax(u, dim=1)

        return u


class Decoder(nn.Module):
    def __init__(self, opt, use_softmax):
        super(Decoder, self).__init__()
        self.mem_feat_depth = opt.num_class
        self.mem_size = opt.mem_size
        self.Warp = GridSamplerModule()
        self.N_u = N_u_Module(opt, self.mem_feat_depth, self.mem_size)
        self.N_o = N_o_Module(opt, self.mem_feat_depth, self.mem_size, use_softmax=use_softmax)
        self.num_class = opt.num_class
        self.use_cuda = opt.use_cuda
        self.opt = opt


    def init_buffer(self, batch_size, h, w, start=True):
        if start:
            self.S_t = Variable(torch.zeros((batch_size, self.mem_feat_depth, h, w, self.mem_size)))
            if self.use_cuda:
                self.S_t = self.S_t.cuda()


    def update_buffer(self, S_tm1, c_t, flow_var, optical_flow_confidence):
        #apply optical flow on each buffer entry- to make buffer represent time t
        Sp = self.apply_opticalflow_warp_on_buffer(S_tm1[:, :, :, :, :-1], flow_var)

        z_t = c_t.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([z_t, Sp], 4)
        else:
            S = z_t

        # update S
        u = self.N_u(S, optical_flow_confidence)
        u = u.unsqueeze(4)

        if self.mem_size > 1:
            S = torch.cat([u, Sp], 4)
        else:
            S = u

        return S


    def apply_opticalflow_warp_on_buffer(self, S_tm1, flow):
        _S_tm1_after_applying_warp = None

        for i in xrange(0, self.mem_size - 1, 1):
            curr_buffer_descriptor = S_tm1[:, :, :, :, i].contiguous()

            curr_buffer_descriptor_warped = self.Warp(curr_buffer_descriptor, flow).unsqueeze(4)

            if _S_tm1_after_applying_warp is None:
                _S_tm1_after_applying_warp = curr_buffer_descriptor_warped
            else:
                _S_tm1_after_applying_warp = torch.cat([_S_tm1_after_applying_warp, curr_buffer_descriptor_warped], 4)

        return _S_tm1_after_applying_warp


    def apply_opticalflow_warp(self, hidden_state, flow):
        hidden_state_warped = self.Warp(hidden_state, flow)
        return hidden_state_warped


    def getOpticalFlowConfidence(self, img_t, img_tm1, optical_flow):
        img_tm1_warped = self.apply_opticalflow_warp(img_tm1, optical_flow)
        return img_t - img_tm1_warped


    def forward(self, context, inputImg, prevImg, inputOpticalFlow, start=True):
        self.opt.include_optical_flow_confidence = 0
        out = []

        seq_len, batch_size, _, h, w = context.size()
        self.init_buffer(batch_size, h, w, start)

        for seqInd in xrange(0, seq_len, 1):
            # update buffer
            c_t = context[seqInd]

            optical_flow_confidence = self.getOpticalFlowConfidence(inputImg[seqInd], prevImg[seqInd], inputOpticalFlow[seqInd])
            self.S_t = self.update_buffer(self.S_t, c_t, inputOpticalFlow[seqInd], optical_flow_confidence)

            # predict next time step based on buffer content
            o_t = self.N_o(self.S_t, optical_flow_confidence)
            out += [o_t]

        out_seq = torch.stack(out)
        return out_seq


class Loop(nn.Module):
    def __init__(self, opt, use_softmax=False):
        super(Loop, self).__init__()
        self.decoder = Decoder(opt, use_softmax)

    def get_params_lr_arr(self):
        params_lr_arr = [
            {'params': self.decoder.N_u.parameters(), 'lr': 1e-4},
            {'params': self.decoder.N_o.parameters(), 'lr': 1e-4},
        ]
        return params_lr_arr

    def cuda(self, device_id=None):
        nn.Module.cuda(self, device_id)

    def forward(self, inputDescriptor, inputImg, prevImg, inputOpticalFlow, target, target_exist, imgFilename
                , img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, filenameUtils, start=True):
        out = self.decoder(inputDescriptor, inputImg, prevImg, inputOpticalFlow, start)
        return out