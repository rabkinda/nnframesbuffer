# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import argparse
import os

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import visdom
from tqdm import tqdm

from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.datasets.dataset_loader_key_frame_and_n_prev_frames import TBPTTIter
from framesBuffer.datasets.dataset_outer_key_frame_and_n_prev_frames import DatasetOuter
from framesBuffer.semanticSegmentation.utils import accuracy, loadClassWeights, getImageDescriptors, loadInitialSegmentationModel
from framesBuffer.utils import create_output_dir, wrap, check_grad, my_import
from semanticSegmentationPytorchFineTuned.utils import AverageMeter

plt.switch_backend('agg')

from tensorboardX import SummaryWriter
from datetime import datetime
import json


parser = argparse.ArgumentParser(description='PyTorch Loop')
# Env options:
parser.add_argument('--epochs', type=int, default=100, metavar='N',
                    help='number of epochs to train (default: 92)')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--expName', type=str, default='vctk', metavar='E',
                    help='Experiment name')
parser.add_argument('--data', default='data/vctk',
                    metavar='D', type=str, help='Data path')
parser.add_argument('--checkpoint', default='',
                    metavar='C', type=str, help='Checkpoint path')
parser.add_argument('--gpu', default=0,
                    metavar='G', type=int, help='GPU device ID')
parser.add_argument('--visualize', action='store_true', #default=1,
                    help='Visualize train and validation loss.')
# Data options
parser.add_argument('--seq-len', type=int, default=100,
                    help='Sequence length for tbptt')
parser.add_argument('--batch-size', type=int, default=64,
                    help='Batch size')
parser.add_argument('--lr', type=float, default=1e-4,
                    help='Learning rate')
parser.add_argument('--clip-grad', type=float, default=0.5,
                    help='maximum norm of gradient clipping')
parser.add_argument('--ignore-grad', type=float, default=10000.0,
                    help='ignore grad before clipping')
# Model options
parser.add_argument('--mem-size', type=int, default=20,
                    help='Memory number of segments')

# Path related arguments
parser.add_argument('--list_train', default='train.txt')
parser.add_argument('--list_val', default='val.txt')
parser.add_argument('--root_img', default='images_step_1_png')
parser.add_argument('--root_img_flipped', default='images_step_1_png_flipped')
parser.add_argument('--root_seg', default='annotations')
parser.add_argument('--root_optical_flow', default='OF_DIS_1_step_frames_with_flips')
parser.add_argument('--netwarp_build_dir', default='/home/rabkinda/workspace/duchki/netwarp_public/build/external/OF_DIS/run_OF_RGB')
parser.add_argument('--remove_generated_OF_files', type=int, default=1)

# Data related arguments
parser.add_argument('--num_val', default=128, type=int,
                    help='number of images to evalutate')
parser.add_argument('--num_class', default=11, type=int,
                    help='number of classes')
parser.add_argument('--workers', default=16, type=int,
                    help='number of data loading workers')
parser.add_argument('--imgSize', default=720, type=int,
                    help='input image size')
parser.add_argument('--segSize', default=720, type=int,
                    help='output image size')
parser.add_argument('--weighting', type=int, default=1,
                        help='True will adopt weights on loss function and vice versa')

parser.add_argument('--disp_iter', type=int, default=20,
                    help='frequency to display')
parser.add_argument('--vis', default='../vis',
                    help='folder to output visualization during training')

#initial segmentation parameters
parser.add_argument('--init_seg_id', help='init_seg_id', type=str)
parser.add_argument('--init_seg_ckpt', help='init_seg_ckpt', type=str)

parser.add_argument('--loop_model', help='loop_model', type=str)
parser.add_argument('--adjust_lr_manually', help='adjust_lr_manually', type=int, default=1)
parser.add_argument('--use_cuda', help='use_cuda', type=int, default=1)
parser.add_argument('--use_flip', help='use_flip', type=int, default=1)

parser.add_argument('--frame_ind_gap', type=int, default=1, help='frame_ind_gap')

parser.add_argument('--prev_frames_num', type=int, default=5, help='prev_frames_num')
parser.add_argument('--use_data_augmentation', type=int, default=1, help='use_data_augmentation')

parser.add_argument('--state_with_softmax', type=int, default=0, help='state_with_softmax')
parser.add_argument('--include_optical_flow_confidence', type=int, default=1, help='include_optical_flow_confidence')
parser.add_argument('--include_spatial_dilation', type=int, default=0, help='include_spatial_dilation')
parser.add_argument('--time_division', type=int, default=2, help='time_division')
parser.add_argument('--use_conv3d', type=int, default=1, help='use_conv3d')
parser.add_argument('--use_dropout', type=int, default=0, help='use_dropout')
parser.add_argument('--use_noise', type=int, default=1, help='use_noise')
parser.add_argument('--use_weight_decay', type=int, default=1, help='use_weight_decay')

parser.add_argument('--sequential_model', type=int, default=1, help='sequential_model')

parser.add_argument('--learning_rate_decay_start', type=int, default=-1,
help='at what iteration to start decaying learning rate? (-1 = dont) (in epoch)')
parser.add_argument('--learning_rate_decay_every', type=int, default=3,
help='every how many iterations thereafter to drop LR?(in epoch)')
parser.add_argument('--learning_rate_decay_rate', type=float, default=0.8,
help='every how many iterations thereafter to drop LR?(in epoch)')

parser.add_argument('--prev_seq_len_type', type=str, default='random', help='prev_seq_len_type')
parser.add_argument('--curriculum_length_increase_every', type=int, default=2, help='curriculum_length_increase_every')

parser.add_argument('--max_not_improving_epochs', type=int, default=10, help='max_not_improving_epochs')

parser.add_argument('--spatial_kernel_size', type=int, default=7, help='spatial_kernel_size')

parser.add_argument('--use_batch_norm', type=int, default=0, help='use_batch_norm')

parser.add_argument('--use_descriptor_div_8', type=int, default=0, help='use_descriptor_div_8')
parser.add_argument('--pspnet_chunk_batch_size', type=int, default=5, help='pspnet_chunk_batch_size')

parser.add_argument('--use_my_cj', type=int, default=0, help='use_my_cj')
parser.add_argument('--use_cj', type=int, default=0, help='use_cj')

parser.add_argument('--num_gpus', default=1, type=int, help='number of gpus to use')

parser.add_argument('--dataset', type=str, default='camvid', help='dataset')

# init
args = parser.parse_args()
args.expName = os.path.join('../checkpoints', args.expName)
args.list_train = os.path.join(args.data, args.list_train)
args.list_val = os.path.join(args.data, args.list_val)
args.root_img = os.path.join(args.data, args.root_img)
args.root_img_flipped = os.path.join(args.data, args.root_img_flipped)
args.root_seg = os.path.join(args.data, args.root_seg)
#args.root_optical_flow = os.path.join(args.data, args.root_optical_flow)

args.scales = (1,)
args.multi_scale = 0

#torch.cuda.set_device(args.gpu)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
logging = create_output_dir(args)
vis = visdom.Visdom(env=args.expName)

args.batch_size = args.batch_size*args.num_gpus

# data
logging.info("Building dataset.")
train_dataset = DatasetOuter(args.list_train, args, is_train=1)
train_loader = torch.utils.data.DataLoader(train_dataset,
                         batch_size=args.batch_size,
                         num_workers=4,
                         pin_memory=True,
                         shuffle=True)

valid_dataset = DatasetOuter(args.list_val, args, max_sample=args.num_val, is_train=0)
valid_loader = torch.utils.data.DataLoader(valid_dataset,
                         batch_size=args.batch_size,
                         num_workers=4,
                         pin_memory=True)

logging.info("Dataset ready!")
args.epoch_iters = int(len(train_dataset) / args.batch_size)


def train(model, criterion, optimizer, epoch, train_losses, history, writer, args, initial_seg_nets, semanticSegmentationArgs, iterNum):
    model.train()
    total = 0   # Reset every plot_every
    acc_meter = AverageMeter()
    train_enum = tqdm(train_loader, desc='Train epoch %d' % epoch)
    i=0

    for keyframe_relpath, seqName, scale, x1, y1, flip, rangeTuple, length in train_enum:

        batch_iter = TBPTTIter(args.list_train, args, keyframe_relpath, seqName, scale, x1, y1, flip, rangeTuple, length, args.seq_len)
        batch_total = 0
        acc_meter_per_batch = AverageMeter()
        grad_check_negative = False
        loss_calculation_num = 1e-6

        for img, seg, prev_img, label_exist, optical_flow, curr_length, img_ind, imgFilename, seqNameWithPrefix, start, end in batch_iter:
            img = img.contiguous()
            _,batch_size,_,h,w = img.size()
            imgDescriptor = getImageDescriptors(img, initial_seg_nets, semanticSegmentationArgs, args)

            inputDescriptor = wrap(args.use_cuda, imgDescriptor)
            inputImg = wrap(args.use_cuda, img)
            prevImg = wrap(args.use_cuda, prev_img)
            inputOpticalFlow = wrap(args.use_cuda, optical_flow)
            target = wrap(args.use_cuda, seg).contiguous()
            target_exist = wrap(args.use_cuda, label_exist)

            # Zero gradients
            if start:
                S_t = wrap(args.use_cuda, torch.zeros((batch_size, args.num_class, h, w, args.mem_size)))
                optimizer.zero_grad()

            # Forward
            inputDescriptor = inputDescriptor.transpose(0, 1)
            inputImg = inputImg.transpose(0, 1)
            prevImg = prevImg.transpose(0, 1)
            inputOpticalFlow = inputOpticalFlow.transpose(0, 1)
            output, S_t = model(inputDescriptor, inputImg, prevImg, inputOpticalFlow, S_t, start)
            output = output.transpose(0, 1)
            indices_for_loss = (target_exist == 1).nonzero()

            if len(indices_for_loss) > 0:
                batches_for_loss = indices_for_loss[:, 1]
                seqs_for_loss = indices_for_loss[:, 0]
                target_for_loss = target[seqs_for_loss, batches_for_loss, :, :]
                output_for_loss = output[seqs_for_loss, batches_for_loss, :, :, :]

                loss = criterion(output_for_loss, target_for_loss)

                # Backward
                loss.backward(retain_graph=True if not end else False)
                grad_check_negative = check_grad(model.parameters(), args.clip_grad, args.ignore_grad)
                if grad_check_negative:
                    logging.info('Not a finite gradient or too big, ignoring.')
                    optimizer.zero_grad()
                    continue
                optimizer.step()

                # Keep track of loss
                batch_total += loss.data[0]
                loss_calculation_num += 1

                # calculate accuracy
                acc, pix = accuracy(target_for_loss, output_for_loss, args)
                acc_meter.update(acc, pix)
                acc_meter_per_batch.update(acc, pix)

        batch_total = batch_total / loss_calculation_num
        batch_acc = acc_meter_per_batch.average() * 100 if acc_meter_per_batch.average()!=None else 0
        total += batch_total

        if i % args.disp_iter == 0 and (not grad_check_negative):
            writer.add_scalar('Loss/Train', batch_total, iterNum)
            writer.add_scalar('Precision/Train', batch_acc, iterNum)

            # calculate accuracy, and display
            train_enum.set_description('Train (batch_loss %.2f, batch_acc %.2f) epoch %d' % (batch_total, batch_acc, epoch))
            fractional_epoch = epoch - 1 + 1. * i / args.epoch_iters
            history['train']['epoch'].append(fractional_epoch)
            history['train']['err'].append(batch_total)
            history['train']['acc'].append(batch_acc)

        i+=1
        iterNum += 1

    for name, param in model.named_parameters():
        writer.add_histogram(name, param.data.cpu().numpy(), epoch)

        if param.grad is not None:
            writer.add_histogram(name + '/gradient', param.grad.data.cpu().numpy(), epoch)

    loss_avg = total / len(train_loader)
    acc_avg = acc_meter.average() * 100

    writer.add_scalar('Loss_Avg/Train', loss_avg, epoch)
    writer.add_scalar('Precision_Avg/Train', acc_avg, epoch)

    train_losses.append(loss_avg)
    if args.visualize:
        vis.line(Y=np.asarray(train_losses),
                 X=torch.arange(1, 1 + len(train_losses)),
                 opts=dict(title="Train"),
                 win='Train loss ' + args.expName)

    logging.info('====> Total train set loss: {:.4f}, acc: {:.4f}'.format(loss_avg, acc_avg))
    return iterNum, loss_avg, acc_avg


def evaluate(model, criterion, epoch, eval_losses, history, writer, args, initial_seg_nets, semanticSegmentationArgs, iterNum):
    model.eval()
    total = 0
    acc_meter = AverageMeter()
    valid_enum = tqdm(valid_loader, desc='Valid epoch %d' % epoch)
    i=0

    for keyframe_relpath, seqName, scale, x1, y1, flip, rangeTuple, length in valid_enum:

        batch_iter = TBPTTIter(args.list_val, args, keyframe_relpath, seqName, scale, x1, y1, flip, rangeTuple, length, args.seq_len)
        batch_total = 0
        acc_meter_per_batch = AverageMeter()
        loss_calculation_num = 1e-6

        for img, seg, prev_img, label_exist, optical_flow, curr_length, img_ind, imgFilename, seqNameWithPrefix, start, end in batch_iter:
            img = img.contiguous()
            _, batch_size, _, h, w = img.size()
            imgDescriptor = getImageDescriptors(img, initial_seg_nets, semanticSegmentationArgs, args)

            inputDescriptor = wrap(args.use_cuda, imgDescriptor)
            inputImg = wrap(args.use_cuda, img)
            prevImg = wrap(args.use_cuda, prev_img)
            inputOpticalFlow = wrap(args.use_cuda, optical_flow)
            target = wrap(args.use_cuda, seg).contiguous()
            target_exist = wrap(args.use_cuda, label_exist)

            if start:
                S_t = wrap(args.use_cuda, torch.zeros((batch_size, args.num_class, h, w, args.mem_size)))

            # Forward
            inputDescriptor = inputDescriptor.transpose(0, 1)
            inputImg = inputImg.transpose(0, 1)
            prevImg = prevImg.transpose(0, 1)
            inputOpticalFlow = inputOpticalFlow.transpose(0, 1)
            output, S_t = model(inputDescriptor, inputImg, prevImg, inputOpticalFlow, S_t, start)
            output = output.transpose(0, 1)
            indices_for_loss = (target_exist == 1).nonzero()

            if len(indices_for_loss) > 0:
                batches_for_loss = indices_for_loss[:, 1]
                seqs_for_loss = indices_for_loss[:, 0]
                target_for_loss = target[seqs_for_loss, batches_for_loss, :, :]
                output_for_loss = output[seqs_for_loss, batches_for_loss, :, :, :]

                loss = criterion(output_for_loss, target_for_loss)
                batch_total += loss.data[0]
                loss_calculation_num+=1

                # calculate accuracy
                acc, pix = accuracy(target_for_loss, output_for_loss, args)
                acc_meter.update(acc, pix)
                acc_meter_per_batch.update(acc, pix)

        batch_total = batch_total / loss_calculation_num
        total += batch_total
        batch_acc = acc_meter_per_batch.average()*100
        #print('[Eval] iter {}, batch_loss: {}, batch_acc: {}'.format(i, batch_total, batch_acc))

        if i % (args.disp_iter/2) == 0:
            valid_enum.set_description('Valid (batch_loss %.2f, batch_acc %.2f) epoch %d' % (batch_total, batch_acc, epoch))
            writer.add_scalar('Loss/Val', batch_total, iterNum)
            writer.add_scalar('Precision/Val', batch_acc, iterNum)

        iterNum+=1
        i += 1

    #visualization
    # batch_data = (img, target, imgBasenameSeq)  # (imgs, segs, infos)
    # im_vis_arr = visualize(batch_data, output, args)
    # im_vis_arr = np.array(im_vis_arr)
    # im_vis_arr = im_vis_arr.reshape((-1, args.seq_len, im_vis_arr.shape[1], im_vis_arr.shape[2], im_vis_arr.shape[3]))

    # curr_batch_im_vis_arr = im_vis_arr[0]#random.randint(0, batch_size - 1)]
    # for s in xrange(0, args.seq_len, 1):
    #     writer.add_image('Val/ImageResult/' + str(s), curr_batch_im_vis_arr[s], epoch)

    loss_avg = total / len(valid_loader)
    acc_avg = acc_meter.average()*100

    writer.add_scalar('Loss_Avg/Val', loss_avg, epoch)
    writer.add_scalar('Precision_Avg/Val', acc_avg, epoch)

    history['val']['epoch'].append(epoch)
    history['val']['err'].append(loss_avg)
    history['val']['acc'].append(acc_avg)
    #print('[Eval Summary] Epoch: {}, Loss: {}, Accuracy: {:4.2f}%'.format(epoch, loss_avg, acc_avg))

    # Plot figure
    if epoch > 0:
        fig = plt.figure()
        plt.plot(np.asarray(history['train']['epoch']),
                 np.log(np.asarray(history['train']['err'])),
                 color='b', label='training')
        plt.plot(np.asarray(history['val']['epoch']),
                 np.log(np.asarray(history['val']['err'])),
                 color='c', label='validation')
        plt.legend()
        plt.xlabel('Epoch')
        plt.ylabel('Log(loss)')
        fig.savefig('{}/loss.png'.format(args.expName), dpi=200)
        plt.close('all')

        fig = plt.figure()
        plt.plot(history['train']['epoch'], history['train']['acc'],
                 color='b', label='training')
        plt.plot(history['val']['epoch'], history['val']['acc'],
                 color='c', label='validation')
        plt.legend()
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        fig.savefig('{}/accuracy.png'.format(args.expName), dpi=200)
        plt.close('all')

    eval_losses.append(loss_avg)
    if args.visualize:
        vis.line(Y=np.asarray(eval_losses),
                 X=torch.arange(1, 1 + len(eval_losses)),
                 opts=dict(title="Eval"),
                 win='Eval loss ' + args.expName)

    logging.info('====> Total validation set loss: {:.4f}, acc: {:.4f}'.format(loss_avg, acc_avg))
    return iterNum, loss_avg, acc_avg


def adjust_learning_rate(optimizer, shrink_factor):
    """
    Shrinks learning rate by a specified factor.
    """

    print("\nDECAYING learning rate.")
    for param_group in optimizer.param_groups:
        param_group['lr'] = param_group['lr'] * shrink_factor
    print("The new learning rate is %.8f\n" % (optimizer.param_groups[0]['lr'],))


def update_learning_rate(optimizer, epoch):
    if args.adjust_lr_manually:
        # Assign the learning rate
        if epoch > args.learning_rate_decay_start and args.learning_rate_decay_start >= 0:
            frac = (epoch - args.learning_rate_decay_start) // args.learning_rate_decay_every
            decay_factor = args.learning_rate_decay_rate ** frac
            args.current_lr = args.lr * decay_factor
            set_lr(optimizer, args.current_lr)  # set the decayed rate


def set_lr(optimizer, lr):
    for group in optimizer.param_groups:
        group['lr'] = lr


def main():
    startTime = datetime.now()
    start_epoch = 1
    trainIter = 1
    valIter = 1

    __import__(args.loop_model)
    Loop = my_import(args.loop_model + '.Loop')

    model = Loop(args)
    if args.use_cuda:
        model.cuda()

        if args.num_gpus > 1:
            print(os.environ["CUDA_VISIBLE_DEVICES"])
            model = nn.DataParallel(model, device_ids=map(int, os.environ["CUDA_VISIBLE_DEVICES"].split(',')))

    #model = nn.DataParallel(model, device_ids=map(int, '0'.split(',')))#TODO: DELETE
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-5 if args.use_weight_decay else 0)
    args.current_lr = args.lr

    if args.checkpoint != '':
        checkpoint_args_path = os.path.dirname(args.checkpoint) + '/args.pth'
        checkpoint_args = torch.load(checkpoint_args_path)

        start_epoch = checkpoint_args[3]
        trainIter = checkpoint_args[4]
        valIter = checkpoint_args[5]

        lr = checkpoint_args[6]
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr

        model.load_state_dict(torch.load(args.checkpoint))

    weights = None
    if args.weighting:
        weights = wrap(args.use_cuda, loadClassWeights(args))

    criterion = nn.NLLLoss2d(weight=weights, ignore_index=DatasetFactory.getConstants(args.dataset).void_labels[0])
    if args.use_cuda:
        criterion = criterion.cuda()

    # Keep track of losses
    train_losses = []
    eval_losses = []
    best_eval = float('inf')

    # Main loop
    history = {split: {'epoch': [], 'err': [], 'acc': []} for split in ('train', 'val')}

    (initial_seg_nets, semanticSegmentationArgs) = loadInitialSegmentationModel(args)

    writer = SummaryWriter(args.expName)
    epochs_since_improvement=0

    # Begin!
    for epoch in range(start_epoch, start_epoch + args.epochs):
        train_dataset.setEpoch(epoch)
        valid_dataset.setEpoch(epoch)
        update_learning_rate(optimizer, epoch)
        writer.add_scalar('lr', args.current_lr, epoch)

        if args.adjust_lr_manually:
            # Halve learning rate if there is no improvement for 3 consecutive epochs, and terminate training after 8
            if epochs_since_improvement == args.max_not_improving_epochs:
                break
            if args.learning_rate_decay_start < 0 and epochs_since_improvement > 0 and epochs_since_improvement % 3 == 0:
                adjust_learning_rate(optimizer, 0.6)

        trainIter, train_avg_loss, train_avg_acc = train(model, criterion, optimizer, epoch, train_losses, history, writer, args, initial_seg_nets, semanticSegmentationArgs, trainIter)
        valIter, val_avg_loss, val_avg_acc = evaluate(model, criterion, epoch, eval_losses, history, writer, args, initial_seg_nets, semanticSegmentationArgs, valIter)
        model_state = getModelState(model)

        if val_avg_loss < best_eval:
            torch.save(model_state, '%s/bestmodel.pth' % (args.expName))
            best_eval = val_avg_loss
            epochs_since_improvement = 0
            args.best_val_err_full_info = {'epoch': epoch, 'train_avg_loss': train_avg_loss, 'train_avg_acc': train_avg_acc,
                                           'val_avg_loss': val_avg_loss, 'val_avg_acc': val_avg_acc}
        else:
            epochs_since_improvement += 1
            print("\nEpochs since last improvement: %d\n" % (epochs_since_improvement,))

        torch.save(model_state, '%s/lastmodel.pth' % (args.expName))
        torch.save([args, train_losses, eval_losses, epoch, trainIter, valIter, optimizer.param_groups[0]['lr']],
                   '%s/args.pth' % (args.expName))

    writer.close()
    print json.dumps(args.best_val_err_full_info, indent=4, sort_keys=True)
    print('startTime=' + str(startTime))
    print('endTime=' + str(datetime.now()))


def getModelState(model):
    if args.num_gpus > 1:
        model_state = model.module.state_dict()
    else:
        model_state = model.state_dict()
    return model_state


if __name__ == '__main__':
    main()
