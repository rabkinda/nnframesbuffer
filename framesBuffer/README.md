# Employing Temporal Information in Deep Learning of Video #
## Enhance temporal coherency and improve the segmentation estimates without additional annotation cost ##

Using PSPNet as single frame segmentation (https://arxiv.org/abs/1612.01105).

## Dependencies ###

* Python 2.7
* Pytorch 0.3

## Datasets ###

* CamVid
* Cityscapes

![Buffer for Video Segmentation](https://bitbucket.org/rabkinda/nnframesbuffer/raw/master/framesBuffer/4README/BufferVideoSegmentation.PNG)

![Methodology_Summary](https://bitbucket.org/rabkinda/nnframesbuffer/raw/master/framesBuffer/4README/Video_Segmentation_by_Buffer-Methodology_Summary.PNG)

![Camvid Results](https://bitbucket.org/rabkinda/nnframesbuffer/raw/master/framesBuffer/4README/CamvidResults.PNG)
![Cityscapes Results](https://bitbucket.org/rabkinda/nnframesbuffer/raw/master/framesBuffer/4README/CityscapesResults.PNG)
![Cityscapes Visual Results](https://bitbucket.org/rabkinda/nnframesbuffer/raw/master/framesBuffer/4README/CityscapesVisualResults.png)