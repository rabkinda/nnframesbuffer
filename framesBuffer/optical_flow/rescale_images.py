# Copyright 2017 Max Planck Society
# Distributed under the BSD-3 Software license,
# (See accompanying file LICENSE.txt or copy at
# https://opensource.org/licenses/BSD-3-Clause)

# extract and save optical flow
# python extract_opticalflow.py VAL
# python extract_opticalflow.py TEST

import argparse
import os
import sys

from PIL import Image

sys.path.insert(0,'.')

parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--key', type=str, help='key')
parser.add_argument('--txt', type=str, help='txt')
parser.add_argument('--root_img', type=str, help='root_img')
parser.add_argument('--root_img_flipped', type=str, help='root_img_flipped')
args = parser.parse_args()

list_sample = [x.rstrip().split(' ')[0] for x in open(args.txt, 'r')]

categories = [c for c in os.listdir(os.path.join(args.root_img, args.key))]
categoriesMap = {}
for c in categories:
    categoriesMap[c] = []

for s in list_sample:
    catagory = s.split(os.sep)[1]
    categoriesMap[catagory].append(s)


def reduce_img_size(frame_path, root_img, catagory_frame_list, frameInd):
    img = Image.open(frame_path)
    size = img.size  # get the size of the input image
    ratio = 1.0 / 8.0  # reduced the size to 90% of the input image
    reduced_size = int(size[0] * ratio), int(size[1] * ratio)
    img_resized = img.resize(reduced_size, Image.ANTIALIAS)
    save_path = os.path.join(root_img + '_reduced', catagory_frame_list[frameInd])
    save_dir = os.path.dirname(save_path)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    img_resized.save(save_path)


for i in range(0, len(categories)):
    catagory_name = categories[i]
    catagory_frame_list = categoriesMap[catagory_name]

    for frameInd in xrange(0, len(catagory_frame_list), 1):

        curr_frame_path = os.path.join(args.root_img, catagory_frame_list[frameInd])
        reduce_img_size(curr_frame_path, args.root_img, catagory_frame_list, frameInd)

        curr_frame_flipped_path = os.path.join(args.root_img_flipped, catagory_frame_list[frameInd])
        reduce_img_size(curr_frame_flipped_path, args.root_img_flipped, catagory_frame_list, frameInd)

        print('Done with image number '+str(frameInd)+' out of '+str(len(catagory_frame_list))+'\n')