import os


def calculate_flow(frame_t_path, frame_tmi_path, flow_path, netwarp_build_dir, expName):
    returnCode = -1
    cmd=''
    try:
        if not os.path.exists(os.path.dirname(flow_path)):
            os.makedirs(os.path.dirname(flow_path))

        if not os.path.exists(flow_path):
            cmd = netwarp_build_dir + ' ' + frame_t_path + ' ' + frame_tmi_path + ' ' + flow_path  # + ' ' + flow_run_params
            returnCode = os.system(cmd + ' > '+expName+'/opticalFlowCalculationsLog.txt')
    except Exception as e:
        print('cmd command:\n')
        print(cmd+'\n')
        print(str(returnCode) + '\n')
        raise e