# Copyright 2017 Max Planck Society
# Distributed under the BSD-3 Software license,
# (See accompanying file LICENSE.txt or copy at
# https://opensource.org/licenses/BSD-3-Clause)

# extract and save optical flow
# python extract_opticalflow.py VAL
# python extract_opticalflow.py TEST

import argparse
import os
import sys
from multiprocessing import Pool
from threading import Lock
from PIL import Image
from pathlib import Path
from scipy.misc import imread

sys.path.insert(0,'.')

parser = argparse.ArgumentParser(description='PyTorch Loop')
#parser.add_argument('--key', type=str, help='key')
#parser.add_argument('--txt', type=str, help='txt')
parser.add_argument('--root_img', type=str, help='root_img')
parser.add_argument('--root_img_flipped', type=str, help='root_img_flipped')
args = parser.parse_args()


def save_flipped_img(path):
    save_path = path.replace(args.root_img, args.root_img_flipped)
    save_dir = os.path.dirname(save_path)

    lock.acquire()
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    lock.release()

    img = imread(path, mode='RGB')
    img_flip = img[:, ::-1, :]

    result = Image.fromarray(img_flip)
    result.save(save_path)
    print('Done with image ' + path + '\n')


pathlist = Path(args.root_img).glob('**/**/*.png')
pathlist = [str(path) for path in pathlist]
pathlist.sort()

lock = Lock()
p = Pool(7)
p.map(save_flipped_img, pathlist)

# for path in pathlist:
#     save_flipped_img(path)


# list_sample = [x.rstrip().split(' ')[0] for x in open(args.txt, 'r')]
#
# categories = [c for c in os.listdir(os.path.join(args.root_img, args.key))]
# categoriesMap = {}
# for c in categories:
#     categoriesMap[c] = []
#
# for s in list_sample:
#     catagory = s.split(os.sep)[1]
#     categoriesMap[catagory].append(s)
#
# for i in range(0, len(categories)):
#     catagory_name = categories[i]
#     catagory_frame_list = categoriesMap[catagory_name]
#
#     for frameInd in xrange(0, len(catagory_frame_list), 1):
#
#         curr_frame_path = os.path.join(args.root_img, catagory_frame_list[frameInd])
#         img = imread(curr_frame_path, mode='RGB')
#         img_flip = img[:, ::-1, :]
#         save_path = os.path.join(args.root_img_flipped, catagory_frame_list[frameInd])
#         save_dir = os.path.dirname(save_path)
#
#         if not os.path.exists(save_dir):
#             os.makedirs(save_dir)
#
#         result = Image.fromarray(img_flip)
#         result.save(save_path)
#         print('Done with image number '+str(frameInd)+' out of '+str(len(catagory_frame_list))+'\n')