# Copyright 2017 Max Planck Society
# Distributed under the BSD-3 Software license,
# (See accompanying file LICENSE.txt or copy at
# https://opensource.org/licenses/BSD-3-Clause)

# extract and save optical flow
# python extract_opticalflow.py VAL
# python extract_opticalflow.py TEST
import argparse
import itertools
import multiprocessing
import os
import sys
from PIL import Image
from scipy.misc import imread
from framesBuffer.datasets.DatasetFactory import DatasetFactory

sys.path.insert(0,'.')

parser = argparse.ArgumentParser(description='PyTorch Loop')

parser.add_argument('--key', type=str, help='key')
parser.add_argument('--txt', type=str, help='txt')
parser.add_argument('--root_img', type=str, help='root_img')
parser.add_argument('--root_img_flipped', type=str, help='root_img_flipped')
parser.add_argument('--root_data', type=str, help='root_data')
parser.add_argument('--netwarp_build_dir', type=str, help='netwarp_build_dir')
parser.add_argument('--dataset', type=str, help='dataset')
args = parser.parse_args()

#flow_run_params = '5 0 25 25 0.05 0.95 0 8 0.80 0 1 1 1 10 10 5 10 3 1.6 2'

filenameUtils = DatasetFactory.getFilenameUtils(args.dataset, args.txt)

flow_dir_name = 'OF_DIS_sequential_key_frame_n_prev_frames'
prev_frames_num = 6

categoriesMap = filenameUtils.getCategoriesMap()
categories = categoriesMap.keys()


def create_OF_dirs():
    currPath = os.path.join(args.root_data, flow_dir_name, args.key)
    if not os.path.exists(currPath):
        os.makedirs(currPath)

    for catagory in categories:
        currPath = os.path.join(args.root_data, flow_dir_name, args.key, catagory)
        if not os.path.exists(currPath):
            os.makedirs(currPath)


def create_flipped_imgs_dirs():
    currPath = os.path.join(args.root_img_flipped, args.key)
    if not os.path.exists(currPath):
        os.makedirs(currPath)

    for catagory in categories:
        currPath = os.path.join(args.root_img_flipped, args.key, catagory.split('_')[0])
        if not os.path.exists(currPath):
            os.makedirs(currPath)


create_OF_dirs()
create_flipped_imgs_dirs()


def save_flipped_img(path):
    save_path = path.replace(args.root_img, args.root_img_flipped)

    if not os.path.exists(save_path):
        img = imread(path, mode='RGB')
        img_flip = img[:, ::-1, :]

        result = Image.fromarray(img_flip)
        result.save(save_path)


def calculate_flow(frame_t_path, frame_tm1_path, frame_t_ind, frame_tm1_ind, flip, catagory_name):
    flow_suffix = '_flipped.flo' if flip else '.flo'
    flow_path = os.path.join(args.root_data, flow_dir_name, args.key, catagory_name, str(frame_t_ind) + '_' + str(frame_tm1_ind) + flow_suffix)

    if not os.path.exists(flow_path):
        cmd = args.netwarp_build_dir + ' ' + frame_t_path + ' ' + frame_tm1_path + ' ' + flow_path  # + ' ' + flow_run_params
        os.system(cmd)


def getKeyFrameNPrevFramesOpticalFlow(args1):
    catagory_name, ind = args1
    print('catagory_name: '+catagory_name+', ind: '+str(ind))

    catagory_frame_list = categoriesMap[catagory_name]
    startInd = filenameUtils.getStartFrameInd(catagory_name)

    key_frame_ind = filenameUtils.getFrameIndex(catagory_frame_list[ind].getImgPath())

    for k in xrange(0, prev_frames_num, 1):
        frame_t_ind = key_frame_ind - k
        frame_tm1_ind = frame_t_ind - 1

        if frame_tm1_ind >= startInd:
            frame_t_path = os.path.join(args.root_img, filenameUtils.getFramePaths(catagory_name, frame_t_ind).getImgPath())
            frame_tm1_path = os.path.join(args.root_img, filenameUtils.getFramePaths(catagory_name, frame_tm1_ind).getImgPath())

            if os.path.exists(frame_t_path) and os.path.exists(frame_tm1_path):
                calculate_flow(frame_t_path, frame_tm1_path, frame_t_ind, frame_tm1_ind, 0, catagory_name)

                frame_t_flipped_path = os.path.join(args.root_img_flipped, filenameUtils.getFramePaths(catagory_name, frame_t_ind).getImgPath())
                frame_tm1_flipped_path = os.path.join(args.root_img_flipped, filenameUtils.getFramePaths(catagory_name, frame_tm1_ind).getImgPath())

                #add flipped images if they do not exist
                save_flipped_img(frame_t_path)
                save_flipped_img(frame_tm1_path)

                calculate_flow(frame_t_flipped_path, frame_tm1_flipped_path, frame_t_ind, frame_tm1_ind, 1, catagory_name)


pool = multiprocessing.Pool(multiprocessing.cpu_count()-1)

for i in range(0, len(categories), 1):
    catagory_name = categories[i]
    catagory_frame_list = categoriesMap[catagory_name]

    currIterable = itertools.izip(itertools.repeat(catagory_name), range(0, len(catagory_frame_list), 1))

    if i==0:
        iterable = currIterable
    else:
        iterable = itertools.chain(iterable, currIterable)

pool.map(getKeyFrameNPrevFramesOpticalFlow, iterable)

print('Done!!')