###################################################
# Nicolo Savioli, 2017 -- Conv-GRU pytorch v 1.0  #
###################################################
import torch
from torch import nn
import torch.nn.functional as f
from torch.autograd import Variable
import numpy as np
from framesBuffer.utils import wrap


class ConvFlowGRUCell(nn.Module):

    def __init__(self, opt, input_size, hidden_size, kernel_size, cuda_flag):
        super(ConvFlowGRUCell, self).__init__()
        self.opt = opt
        self.input_size = input_size
        self.cuda_flag = cuda_flag
        self.hidden_size = hidden_size
        self.kernel_size = kernel_size
        self.ConvGates = nn.Conv2d(self.input_size + self.hidden_size, self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)
        self.ConvRtGate = nn.Conv2d(3, self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)
        self.Conv_ct = nn.Conv2d(self.input_size + self.hidden_size, self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)

    def forward(self, input, hidden, optical_flow_confidence):
        if hidden is None:
            size_h = [input.size()[0], self.hidden_size] + list(input.size()[2:])
            if self.cuda_flag:
                hidden = Variable(torch.zeros(size_h)).cuda()
            else:
                hidden = Variable(torch.zeros(size_h))

        ut = self.ConvGates(torch.cat((input, hidden), 1))
        rt = self.ConvRtGate(optical_flow_confidence)
        remember_gate = 1 - f.tanh(torch.abs(rt))

        if self.opt.use_op_conf_file:
            with open("op_conf_gru.txt", "a") as op_conf_file:
                op_conf_file.write(str(torch.mean(remember_gate).data.cpu().numpy()[0])+'\n')

        update_gate = f.sigmoid(ut)
        gated_hidden = torch.mul(remember_gate, hidden)
        ct = self.Conv_ct(torch.cat((input, gated_hidden), 1))

        if self.opt.state_with_softmax:
            ct = f.softmax(ct, dim=1)#TODO: not sure

        lambdaScaling = torch.abs(torch.mean(ct)/torch.mean(hidden)) if (torch.mean(hidden)!=0).data[0] else 1.0#11.0
        next_h = (lambdaScaling*torch.mul(1 - update_gate, hidden)) + (update_gate * ct)
        return next_h


class ConvFlowBufferGRUCell(nn.Module):

    def __init__(self, opt, mem_feat_depth, mem_size, kernel_size, cuda_flag):
        super(ConvFlowBufferGRUCell, self).__init__()
        self.opt = opt
        self.mem_feat_depth = mem_feat_depth
        self.mem_size = mem_size
        self.input_size = mem_feat_depth
        self.cuda_flag = cuda_flag
        self.hidden_size = (mem_size-1)*mem_feat_depth
        self.kernel_size = kernel_size
        self.ConvGates = nn.Conv2d(self.input_size + self.hidden_size, self.input_size + self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)
        self.ConvRtGate = nn.Conv2d(3 if opt.sequential_model else 3*(mem_size-1), self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)
        self.Conv_ct = nn.Conv2d(self.input_size + self.hidden_size, self.input_size, self.kernel_size, padding=self.kernel_size // 2)

    def forward(self, input, hidden, optical_flow_confidence):

        ut = self.ConvGates(torch.cat((input, hidden), 1))
        rt = self.ConvRtGate(optical_flow_confidence)
        remember_gate = 1 - f.tanh(torch.abs(rt))
        update_gate = ut#f.sigmoid(ut)
        gated_hidden = torch.mul(remember_gate, hidden)
        ct = self.Conv_ct(torch.cat((input, gated_hidden), 1))

        if self.opt.state_with_softmax:
            ct = f.softmax(ct, dim=1)#TODO: not sure

        batch_size, _, h, w = hidden.size()
        _hidden = hidden.view(batch_size, self.mem_feat_depth, self.mem_size-1, h, w)
        update_gate = update_gate.view(batch_size, self.mem_feat_depth, self.mem_size, h, w)
        update_gate = f.softmax(update_gate, dim=2)

        lambdaScaling = self.getLambdaScaling(ct, _hidden)

        hidden_state = torch.mul(update_gate[:,:,1:,:,:].contiguous(), _hidden)
        hidden_state = lambdaScaling*hidden_state

        hidden_state = torch.sum(hidden_state, dim=2)

        next_h = hidden_state + (update_gate[:,:,0,:,:] * ct)
        return next_h

    def getLambdaScaling(self, ct, hidden):
        hidden_mean = torch.mean(torch.mean(torch.mean(torch.mean(hidden, -1), -1), 1), 0)
        lambdaScaling=None

        for h in hidden_mean:
            if h.data[0]!=0:
                currValue = torch.abs(torch.mean(ct) / h)
            else:
                currValue = wrap(self.opt.use_cuda, torch.ones((1)))

            if type(lambdaScaling)!=type(None):
                lambdaScaling = torch.cat([lambdaScaling, currValue], dim=0)
            else:
                lambdaScaling = currValue

        lambdaScaling = lambdaScaling.unsqueeze(0).unsqueeze(0).unsqueeze(3).unsqueeze(4)
        return lambdaScaling


class ConvGRUCell(nn.Module):

    def __init__(self, input_size, hidden_size, kernel_size, cuda_flag):
        super(ConvGRUCell, self).__init__()
        self.input_size = input_size
        self.cuda_flag = cuda_flag
        self.hidden_size = hidden_size
        self.kernel_size = kernel_size
        self.ConvGates = nn.Conv2d(self.input_size + self.hidden_size, 2 * self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)
        self.Conv_ct = nn.Conv2d(self.input_size + self.hidden_size, self.hidden_size, self.kernel_size, padding=self.kernel_size // 2)

    def forward(self, input, hidden):
        if hidden is None:
            size_h = [input.size()[0], self.hidden_size] + list(input.size()[2:])
            if self.cuda_flag:
                hidden = Variable(torch.zeros(size_h)).cuda()
            else:
                hidden = Variable(torch.zeros(size_h))
        convGates = self.ConvGates(torch.cat((input, hidden), 1))
        (rt, ut) = convGates.chunk(2, 1)
        remember_gate = f.sigmoid(rt)
        update_gate = f.sigmoid(ut)
        gated_hidden = torch.mul(remember_gate, hidden)
        p1 = self.Conv_ct(torch.cat((input, gated_hidden), 1))
        ct = f.tanh(p1)
        next_h = torch.mul(1 - update_gate, hidden) + (update_gate * ct)
        return next_h


def _main():
    """
    Run some basic tests on the API
    """

    # define batch_size, channels, height, width
    b, c, h, w = 1, 3, 4, 8
    d = 5           # hidden state size
    lr = 1e-1       # learning rate
    T = 6           # sequence length
    max_epoch = 20  # number of epochs

    # set manual seed
    torch.manual_seed(0)

    print('Instantiate model')
    model = ConvGRUCell(input_size=c, hidden_size=d, kernel_size=3, cuda_flag=1)#channels_img, hidden_size, kernel_size, cuda_flag
    model = model.cuda()
    print(repr(model))

    print('Create input and target Variables')
    x = Variable(torch.rand(T, b, c, h, w)).cuda()
    y = Variable(torch.randn(T, b, d, h, w)).cuda()

    print('Create a MSE criterion')
    loss_fn = nn.MSELoss()

    print('Run for', max_epoch, 'iterations')
    for epoch in range(0, max_epoch):
        state = None
        loss = 0
        for t in range(0, T):
            state = model(x[t], state)
            loss += loss_fn(state[0], y[t])

        print(' > Epoch {:2d} loss: {:.3f}'.format((epoch+1), loss.data[0]))

        # zero grad parameters
        model.zero_grad()

        # compute new grad parameters through time!
        loss.backward()

        # learning_rate step against the gradient
        for p in model.parameters():
            p.data.sub_(p.grad.data * lr)

    print('Input size:', list(x.data.size()))
    print('Target size:', list(y.data.size()))
    print('Last hidden state size:', list(state[0].size()))


if __name__ == '__main__':
    _main()
