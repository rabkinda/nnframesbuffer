import os

import matplotlib.pyplot as plt
import numpy as np
import torch

from blogCodes.python_deep_learning.segmentation.loader import utility
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from semanticSegmentationPytorchFineTuned import descriptorGetter
from semanticSegmentationPytorchFineTuned.utils import colorEncode

plt.switch_backend('agg')
from scipy.misc import imresize
import math
import PIL as pil
from PIL import Image


def accuracy(segs, pred, args):
    _, preds = torch.max(pred.data, dim=1)
    segs_val = segs.data
    valid = (segs_val >= 0) & (segs_val < getDatasetConstants(args).void_labels[0])
    acc = 1.0 * torch.sum(valid * (preds == segs_val)) / (torch.sum(valid) + 1e-10)
    return acc, torch.sum(valid)


def savePrediction(info, pred, args):
    _pred = np.argmax(pred.data.cpu().numpy(), axis=1)[0]
    path = os.path.join(args.root_pred, info)

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    pil.Image.fromarray(_pred.astype('uint8')).save(path)


def visualize(batch_data, pred, args):
    colors = getDatasetConstants(args).cmap
    (imgs, segs, infos) = batch_data

    if type(infos[0]) is list:
        infos = [item for sublist in infos for item in sublist]

    im_vis_arr = [None]*len(infos)

    for j in range(len(infos)):
        # get/recover image
        img = imgs[j].clone()
        for t, m, s in zip(img,
                           [0.485, 0.456, 0.406],
                           [0.229, 0.224, 0.225]):
            t.mul_(s).add_(m)
        img = (img.numpy().transpose((1, 2, 0)) * 255).astype(np.uint8)
        if args.imgSize>0:
            img = imresize(img, (args.imgSize, args.imgSize),
                           interp='bilinear')

        # segmentation
        lab = segs[j].data.cpu().numpy()
        lab_color = colorEncode(lab, lab, colors, args)
        if args.imgSize > 0:
            lab_color = imresize(lab_color, (args.imgSize, args.imgSize),
                                 interp='nearest')

        # prediction
        pred_ = np.argmax(pred.data.cpu()[j].numpy(), axis=0)
        pred_color = colorEncode(pred_, lab, colors, args)
        if args.imgSize > 0:
            pred_color = imresize(pred_color, (args.imgSize, args.imgSize),
                                  interp='nearest')

        # aggregate images and save
        im_vis = np.concatenate((img, lab_color, pred_color),
                                axis=1).astype(np.uint8)
        # imsave(os.path.join(args.vis,
        #                     infos[j].replace('/', '_')
        #                     .replace('.jpg', '.png')), im_vis)
        im_vis_arr[j] = im_vis
    return im_vis_arr


def loadClassWeights(args):
    # build the weights matrix according to the formula 1/ln(1.02 + w)

    #label_colors = utility.read_color_integer(os.path.join(data_dir_path, "label_integer.txt"))

    # build a dictionary with key = (r,g,b), value = percentage of color
    #color_info = utility.read_color_count_sorted(os.path.join(data_dir_path, 'color_count_train_sorted.txt'))
    #color_info = {(data[0], data[1], data[2]): data[4] for data in color_info}

    dataset_constants = getDatasetConstants(args)
    weights = []
    for labelInd in xrange(0, len(dataset_constants.labelsLst), 1):
        w = dataset_constants.labelsLst[labelInd]['freq']
        w = 1 / math.log(1.02 + w, 2)
        weights.append(w)

    weights = np.array(weights)
    weights = torch.from_numpy(weights.astype('float32'))
    return weights


def loadClassFrequencies(data_dir_path):
    label_colors = utility.read_color_integer(os.path.join(data_dir_path, "label_integer.txt"))

    #build a dictionary with key = (r,g,b), value = percentage of color
    color_info = utility.read_color_count_sorted(os.path.join(data_dir_path, 'color_count_train_sorted.txt'))
    color_info = {(data[0], data[1], data[2]): data[4] for data in color_info}

    frequencies = []
    for color in label_colors:
        w = color_info[color]
        frequencies.append(w)

    frequencies = np.array(frequencies)
    return frequencies


def intersectionAndUnion(segsTrg, pred, numClass, args):
    _, preds = torch.max(pred.data.cpu(), dim=1)
    segs_value = segsTrg.data.cpu()

    # compute area intersection
    intersect = preds.clone()
    intersect[torch.ne(preds, segs_value)] = -1

    area_intersect = torch.histc(intersect.float(),
                                 bins=numClass,
                                 min=0,
                                 max=numClass-1)

    # compute area union:
    preds[torch.eq(segs_value, getDatasetConstants(args).void_labels[0])] = -1
    area_pred = torch.histc(preds.float(),
                            bins=numClass,
                            min=0,
                            max=numClass-1)
    area_lab = torch.histc(segs_value.float(),
                           bins=numClass,
                           min=0,
                           max=numClass-1)
    area_union = area_pred + area_lab - area_intersect
    return area_intersect, area_union


def getImageDescriptors(image, initial_seg_nets, semanticSegmentationArgs, args, segSize=None):
    seq_len, batch_size, depth, h, w = image.size()
    if segSize==None:
        segSize=(h, w)
    image = image.view(seq_len*batch_size, depth, h, w)

    chunkBatchSize = min(seq_len*batch_size, args.pspnet_chunk_batch_size)
    semanticSegmentationArgs['batch_size'] = chunkBatchSize

    imageDescriptors=None

    for bIter in xrange(0, int(math.ceil((seq_len*batch_size)/(chunkBatchSize*1.0))), 1):
        startInd = bIter*chunkBatchSize
        endInd = startInd + min(chunkBatchSize, (seq_len*batch_size)-startInd)
        curr_image = image[startInd:endInd]
        semanticSegmentationArgs['batch_size'] = endInd-startInd
        # call other network
        descriptorGetterResult = descriptorGetter.evaluateBatch(initial_seg_nets, curr_image, semanticSegmentationArgs, segSize)#use_softmax=True
        imageDescriptor = descriptorGetterResult.data

        if type(imageDescriptors)==type(None):
            imageDescriptors = imageDescriptor
        else:
            imageDescriptors = torch.cat([imageDescriptors, imageDescriptor], dim=0)

    _, new_depth, new_h, new_w = imageDescriptors.size()
    imageDescriptors = imageDescriptors.view(seq_len, batch_size, new_depth, new_h, new_w)
    return imageDescriptors


def loadInitialSegmentationModel(args):
    semanticSegmentationArgs = \
            {'id': args.init_seg_id, 'arch_encoder': 'resnet50_dilated8', 'arch_decoder': 'psp_bilinear_descriptor', 'fc_dim': 2048,
             'num_class': args.num_class, 'batch_size': args.seq_len*args.batch_size, 'imgSize': args.imgSize, 'segSize': args.segSize,
             'ckpt': args.init_seg_ckpt, 'use_cuda': args.use_cuda, 'state_with_softmax': args.state_with_softmax,
             'multi_scale': args.multi_scale, 'scales': args.scales, 'use_descriptor_div_8': args.use_descriptor_div_8,
             'official_pspnet_checkpoint': args.official_pspnet_checkpoint, 'use_official_pspnet': args.use_official_pspnet, 'dataset': args.dataset}

    initial_seg_nets = descriptorGetter.loadInitialSegmentationModel(semanticSegmentationArgs)
    return initial_seg_nets, semanticSegmentationArgs


def getFinalSegSize(opt):
    return (opt.imgSize, opt.imgSize) if (opt.use_data_augmentation and opt.imgSize > 0) else getDatasetConstants(opt).img_original_size

def isUpsampleNeeded(opt, segSize, u_size):
    return opt.use_descriptor_div_8 and (not (u_size[2] == segSize[0] and u_size[3] == segSize[1]))


def getDatasetConstants(opt):
    return DatasetFactory.getConstants(opt.dataset)


def get_colored_image(result_labels, palette):
    result_image = Image.fromarray(np.uint8(result_labels))
    result_image.putpalette(palette)
    return result_image

def get_reindexed_image(result_labels, cIds):
    result_regularIds = np.copy(result_labels)
    for k, v in cIds.iteritems():
        result_regularIds[result_labels==k] = v
    result_image = Image.fromarray(result_regularIds.astype(np.uint8))
    return result_image


def save_reindexed_colored_image(args, dataset_constants, imgFilename, pred, target):
    _, _pred = torch.max(pred.data, dim=0)

    unlabeled_cond = torch.eq(target, dataset_constants.void_labels[0])
    if args.gtExists and (not unlabeled_cond.min()):
        _pred[unlabeled_cond] = dataset_constants.non_void_nclasses

    if args.dataset == DatasetFactory.DATASET_TYPES.cityscapes.name and 'test' in args.list_test:
        seg_map = get_reindexed_image(_pred, dataset_constants.cIds)
        index_output_path = os.path.join(args.root_reindexed_pred, imgFilename)
        if not os.path.exists(os.path.dirname(index_output_path)):
            os.makedirs(os.path.dirname(index_output_path))
        seg_map.save(index_output_path)

    seg_map = get_colored_image(_pred, dataset_constants.palette)
    color_output_path = os.path.join(args.root_colored_pred, imgFilename)
    if not os.path.exists(os.path.dirname(color_output_path)):
        os.makedirs(os.path.dirname(color_output_path))
    seg_map.save(color_output_path)


def apply_flip(x, dim):
    indices = [slice(None)] * x.dim()
    indices[dim] = torch.arange(x.size(dim) - 1, -1, -1).long()
    indices[dim] = indices[dim].cuda() if x.is_cuda else indices[dim]
    return x[tuple(indices)]