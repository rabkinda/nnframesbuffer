# Copyright 2017 Max Planck Society
# Distributed under the BSD-3 Software license,
# (See accompanying file LICENSE.txt or copy at
# https://opensource.org/licenses/BSD-3-Clause)

import os
import cv2
import numpy as np
import torch
from scipy.misc import imread
from torchvision import transforms
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.optical_flow.opticalFlowUtils import calculate_flow
from framesBuffer.utils import wrap


def _apply_scale_and_crop(flo, scale, x1, y1, cropSize, args):
    H, W = DatasetFactory.getConstants(args.dataset).img_original_size
    arr = cv2.resize(flo, (int(np.round(scale*W)), int(np.round(scale*H))))

    if cropSize[0]>0:
        arr = arr[y1: y1 + cropSize[0], x1: x1 + cropSize[1], :]

    return arr


def _flip_img(img):
    img_flip = img[:, ::-1, :]
    return img_flip


def fetch_flo(flo, scale, x1, y1, crop_size, args):
    _flo = flo
    _x1 = x1
    _y1 = y1
    _crop_size = crop_size

    if args.use_descriptor_div_8:
        _x1 = x1/8
        _y1 = y1/8
        _crop_size = (crop_size[0]/8, crop_size[1]/8)

    if args.use_data_augmentation:
        # random scale, crop, flip
        _flo = _apply_scale_and_crop(_flo, scale, _x1, _y1, _crop_size, args)
        _flo = scale * _flo

    height, width = _flo.shape[0:2]
    _flo = _flo.transpose([2,0,1])

    # CROP_SIZE = _crop_size
    # if height < CROP_SIZE[0]:
    #     _flo = np.lib.pad(_flo, ((0, 0), (0, CROP_SIZE[0] - height), (0, 0)), 'constant', constant_values=0)
    # if width < CROP_SIZE[1]:
    #     _flo = np.lib.pad(_flo, ((0, 0), (0, 0), (0, CROP_SIZE[1] - width)), 'constant', constant_values=0)

    #_flo = _flo*(-1)#change flow apply direction
    return _flo


def fetch_img(img, scale, x1, y1, flip, isDummyData, cropSize, args):
    # mean and std for resnet image
    dataset_constants = DatasetFactory.getConstants(args.dataset)
    img_transform = transforms.Compose([transforms.Normalize(mean=dataset_constants.mean, std=dataset_constants.std)])

    # random scale, crop, flip
    if args.use_data_augmentation:
        if flip:
            img = _flip_img(img)

        img = _apply_scale_and_crop(img, scale, x1, y1, cropSize, args)

    # image to float
    img = np.array(img).astype(np.float32) / 255.
    img = img.transpose((2, 0, 1))

    img = torch.from_numpy(img)

    # substracted by mean and divided by std
    if not isDummyData:
        img = img_transform(img)

    return img


def fetch_img_data(img_path):
    img = imread(img_path, mode='RGB')
    return img


def fetch_flo_data(flo_path, frame_t_ind, frame_tmi_ind, seqName, flip, filenameUtils, opt):
    try:
        flo = readOpticalFlowFromFile(flo_path)
    except:
        try:
            root_img_dir = opt.root_img_flipped if flip else opt.root_img
            frame_t_path = os.path.join(root_img_dir, filenameUtils.getFramePaths(seqName, frame_t_ind).getImgPath())
            frame_tmi_path = os.path.join(root_img_dir, filenameUtils.getFramePaths(seqName, frame_tmi_ind).getImgPath())

            calculate_flow(frame_t_path, frame_tmi_path, flo_path, opt.netwarp_build_dir, opt.expName)
            flo = readOpticalFlowFromFile(flo_path)

            if opt.remove_generated_OF_files:
                os.remove(flo_path)
        except Exception as e:
            print(flo_path)
            raise e
    return flo


def readOpticalFlowFromFile(flo_path):
    with open(flo_path, mode='rb') as f:
        ftag = np.fromfile(f, dtype=np.float32, count=1)[0]
        if ftag != 202021.25:
            raise ('Error in reading flo')
        width = np.fromfile(f, dtype=np.int32, count=1)[0]
        height = np.fromfile(f, dtype=np.int32, count=1)[0]
        flo = np.fromfile(f, dtype=np.float32, count=-1)
    flo = np.reshape(flo, (height, width, 2))
    return flo


def get_scaled_flo_array(img_ind_batch, seqName, scale, x1, y1, flip, num_prev_frames, rangeTuple, isVolatile, filenameUtils, args):
    H, W = DatasetFactory.getConstants(args.dataset).img_original_size
    crop_size = (H, W) if args.imgSize<0 else (args.imgSize, args.imgSize)
    batch_size = len(img_ind_batch)
    flo_arr = [None]*batch_size

    for b in xrange(0, batch_size, 1):
        currImgIndTimeT = img_ind_batch[b]

        for i in xrange(1, num_prev_frames, 1):
           prevInd = currImgIndTimeT - (i*args.frame_ind_gap)

           if prevInd >= rangeTuple[0][b]:
               flow_path = os.path.join(args.root_optical_flow, seqName[b], str(currImgIndTimeT) + '_' + str(prevInd) + '.flo')
               if flip[b]:
                   flow_path = os.path.join(args.root_optical_flow, seqName[b], str(currImgIndTimeT) + '_' + str(prevInd) + '_flipped.flo')

               flo = fetch_flo_data(flow_path, currImgIndTimeT, prevInd, seqName[b], flip[b], filenameUtils, args)
           else:
               flo = np.zeros((H, W, 2))

           flo = fetch_flo(flo, scale[b], x1[b], y1[b], crop_size, args)
           flo = np.expand_dims(flo, axis=3)

           if flo_arr[b] is None:
               flo_arr[b] = flo
           else:
               flo_arr[b] = np.concatenate([flo_arr[b], flo], axis=3)

    flo_arr = torch.from_numpy(np.array(flo_arr)).type(torch.FloatTensor)
    flo_var = wrap(args.use_cuda, flo_arr, volatile=isVolatile)
    return flo_var


def get_scaled_img_array(img_ind_batch, seqName, scale, x1, y1, flip, num_prev_frames, rangeTuple, isVolatile, filenameUtils, args):
    H, W = DatasetFactory.getConstants(args.dataset).img_original_size
    crop_size = (H, W) if args.imgSize < 0 else (args.imgSize, args.imgSize)
    batch_size = len(img_ind_batch)
    img_arr = [None]*batch_size

    for b in xrange(0, batch_size, 1):
        currImgIndTimeT = img_ind_batch[b]

        for i in xrange(1, num_prev_frames, 1):
           prevInd = currImgIndTimeT - (i*args.frame_ind_gap)

           if prevInd >= rangeTuple[0][b]:
               path = os.path.join(args.root_img, filenameUtils.getFramePaths(seqName[b], prevInd).getImgPath())
               img = fetch_img_data(path)
               isDummyData = False
           else:
               img = np.zeros((H, W, 3))
               isDummyData = True

           img = fetch_img(img, scale[b], x1[b], y1[b], flip[b], isDummyData, crop_size, args)
           img = img.unsqueeze(3)

           if img_arr[b] is None:
               img_arr[b] = img
           else:
               img_arr[b] = torch.cat([img_arr[b], img], 3)

    img_var = wrap(args.use_cuda, torch.stack(img_arr), volatile=isVolatile)
    return img_var


def get_scaled_img_flo_array(img_ind_batch, seqName, scale, x1, y1, flip, num_prev_frames, rangeTuple, isVolatile, filenameUtils, args):
    flo_var = get_scaled_flo_array(img_ind_batch, seqName, scale, x1, y1, flip, num_prev_frames, rangeTuple, isVolatile, filenameUtils, args)
    img_var = get_scaled_img_array(img_ind_batch, seqName, scale, x1, y1, flip, num_prev_frames, rangeTuple, isVolatile, filenameUtils, args)
    return flo_var, img_var