import os
import argparse
import numpy as np
from PIL import Image
from pathlib import Path
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.semanticSegmentation.utils import get_colored_image


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--prediction_dir', type=str, help='prediction_dir')
parser.add_argument('--prediction_colored_dir', type=str, help='prediction_colored_dir')
parser.add_argument('--annotation_dir', type=str, help='prediction_colored_dir')
parser.add_argument('--dataset', type=str, help='dataset', default='camvid')
args = parser.parse_args()

dataset_constants = DatasetFactory.getConstants(args.dataset)

def convertPrediction(pred, filename):
    seg_map = get_colored_image(pred, dataset_constants.palette)
    color_output_path = os.path.join(args.prediction_colored_dir, filename)
    if not os.path.exists(os.path.dirname(color_output_path)):
        os.makedirs(os.path.dirname(color_output_path))
    seg_map.save(color_output_path)


pathlist = Path(args.prediction_dir).glob('**/**/*.png')
pathlist = [str(path) for path in pathlist]

for path in pathlist:
    #read img
    predictionImg = Image.open(path)
    predictionNp = np.array(predictionImg)
    gtImg = Image.open(path.replace(args.prediction_dir, args.annotation_dir))
    gtNp = np.array(gtImg)
    predictionNp[gtNp==dataset_constants.void_labels[0]]=dataset_constants.non_void_nclasses

    convertPrediction(predictionNp, path.replace(os.sep.join(args.prediction_dir.split(os.sep)[:-1])+os.sep, ''))

print('Done!!')