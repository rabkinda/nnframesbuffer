import torch


class GridSamplerModule(torch.nn.Module):
    def __init__(self):
        super(GridSamplerModule, self).__init__()

    def forward(self, variableInput, variableFlow, flowNormalized=False):
        if hasattr(self, 'tensorGrid') == False or \
                self.tensorGrid.size(0) != variableInput.size(0) \
                or self.tensorGrid.size(2) != variableInput.size(2) \
                or self.tensorGrid.size(3) != variableInput.size(3):

            torchHorizontal = torch.linspace(-1.0, 1.0, variableInput.size(3))\
                .view(1, 1, 1, variableInput.size(3))\
                .expand(variableInput.size(0), 1, variableInput.size(2), variableInput.size(3))

            torchVertical = torch.linspace(-1.0, 1.0, variableInput.size(2))\
                .view(1, 1, variableInput.size(2), 1)\
                .expand(variableInput.size(0), 1, variableInput.size(2), variableInput.size(3))

            self.tensorGrid = torch.cat([ torchHorizontal, torchVertical ], 1)
            if variableFlow.is_cuda:
                self.tensorGrid = self.tensorGrid.cuda()

        variableGrid = torch.autograd.Variable(data=self.tensorGrid, volatile=not self.training)

        #if not flowNormalized:
        variableFlow = torch.cat([ variableFlow[:, 0:1, :, :] / ((variableInput.size(3) - 1.0) / 2.0)
                                     , variableFlow[:, 1:2, :, :] / ((variableInput.size(2) - 1.0) / 2.0) ], 1)

        return torch.nn.functional.grid_sample(input=variableInput, grid=(variableGrid + variableFlow).permute(0, 2, 3, 1), mode='bilinear')#, padding_mode='border')