# Copyright 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import argparse
import os

import torch
import torch.nn as nn
from tqdm import tqdm

from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.datasets.dataset_loader_key_frame_and_n_prev_frames_faster import TBPTTIterOuter
from framesBuffer.datasets.dataset_outer_key_frame_and_n_prev_frames import DatasetOuter
from framesBuffer.datasets.threaded_generator import ThreadedGenerator
from framesBuffer.semanticSegmentation.utils import accuracy, loadClassWeights, intersectionAndUnion, \
    savePrediction, loadInitialSegmentationModel, save_reindexed_colored_image
from framesBuffer.semanticSegmentation.utils import getImageDescriptors
from framesBuffer.utils import my_import, wrap
from semanticSegmentationPytorchFineTuned.utils import AverageMeter
import time
import numpy as np
from pathos.threading import ThreadPool as ThreadPool


parser = argparse.ArgumentParser(description='PyTorch Phonological Loop \
                                    Generation')
parser.add_argument('--checkpoint', default='checkpoints/vctk/lastmodel.pth',
                    type=str, help='Model used for generation.')
parser.add_argument('--gpu', default=0,
                    type=int, help='GPU device ID, use -1 for CPU.')
parser.add_argument('--list_test',
                    default='test.txt')
parser.add_argument('--num_test', default=-1, type=int,
                    help='number of images to evalutate')
parser.add_argument('--batch-size', type=int, default=64,
                    help='Batch size')
parser.add_argument('--root_img',
                    default='images_step_1_png')
parser.add_argument('--root_seg',
                    default='annotations')
parser.add_argument('--root_optical_flow',
                    default='OF_DIS_sequential_key_frame_n_prev_frames')
parser.add_argument('--netwarp_build_dir', default='/home/rabkinda/workspace/duchki/netwarp_public/build/external/OF_DIS/run_OF_RGB')
parser.add_argument('--remove_generated_OF_files', type=int, default=1)

parser.add_argument('--root_pred', default='predictions')
parser.add_argument('--root_reindexed_pred', default='reindexed_predictions')
parser.add_argument('--root_colored_pred', default='colored_predictions')

parser.add_argument('--imgSize', default=-1, type=int, help='input image size')
parser.add_argument('--segSize', default=-1, type=int, help='output image size')
parser.add_argument('--seq-len', type=int, default=100, help='Sequence length for tbptt')
# parser.add_argument('--num_class', default=11, type=int,
#                     help='number of classes')
parser.add_argument('--vis', default='../vis',
                    help='folder to output visualization during training')

parser.add_argument('--init_seg_id', help='init_seg_id', type=str)
parser.add_argument('--init_seg_ckpt', help='init_seg_ckpt', type=str)

parser.add_argument('--loop_model', help='loop_model', type=str)
parser.add_argument('--use_cuda', help='use_cuda', type=int, default=1)
parser.add_argument('--use_flip', help='use_flip', type=int, default=0)
parser.add_argument('--multi_scale', type=int, default=0, help='multi_scale')

parser.add_argument('--data', default='data/vctk',
                    metavar='D', type=str, help='Data path')

parser.add_argument('--frame_ind_gap', type=int, default=1, help='frame_ind_gap')
parser.add_argument('--prev_frames_num', type=int, default=5, help='prev_frames_num')
parser.add_argument('--use_data_augmentation', type=int, default=0, help='use_data_augmentation')

parser.add_argument('--state_with_softmax', type=int, default=0, help='state_with_softmax')
parser.add_argument('--include_optical_flow_confidence', type=int, default=1, help='include_optical_flow_confidence')
parser.add_argument('--include_spatial_dilation', type=int, default=0, help='include_spatial_dilation')
parser.add_argument('--time_division', type=int, default=2, help='time_division')
parser.add_argument('--use_conv3d', type=int, default=1, help='use_conv3d')
parser.add_argument('--use_dropout', type=int, default=0, help='use_dropout')

parser.add_argument('--sequential_model', type=int, default=1, help='sequential_model')
parser.add_argument('--use_noise', type=int, default=0, help='use_noise')

parser.add_argument('--prev_seq_len_type', type=str, default='', help='prev_seq_len_type')
parser.add_argument('--spatial_kernel_size', type=int, default=7, help='spatial_kernel_size')

parser.add_argument('--use_batch_norm', type=int, default=0, help='use_batch_norm')

parser.add_argument('--use_descriptor_div_8', type=int, default=0, help='use_descriptor_div_8')
parser.add_argument('--pspnet_chunk_batch_size', type=int, default=5, help='pspnet_chunk_batch_size')

parser.add_argument('--use_my_cj', type=int, default=0, help='use_my_cj')
parser.add_argument('--use_cj', type=int, default=0, help='use_cj')

parser.add_argument('--dataset', type=str, default='camvid', help='dataset')

parser.add_argument('--calculateAccIou', type=int, default=1, help='calculateAccIou')

parser.add_argument('--gtExists', type=int, default=1, help='gtExists')

parser.add_argument('--use_padding', type=int, default=0, help='use_padding')

parser.add_argument('--use_op_conf_file', type=int, default=0, help='use_op_conf_file')

parser.add_argument('--save_all_seq', type=int, default=0, help='save_all_seq')

parser.add_argument('--data_queue_size', type=int, default=10, help='data_queue_size')

parser.add_argument('--buffer_spatial_kernel', type=int, default=1, help='buffer_spatial_kernel')

# init
args = parser.parse_args()
if args.gpu >= 0 and args.use_cuda:
    torch.cuda.set_device(args.gpu)


def copyTrainArgs(train_args):
    #args.batch_size = train_args.batch_size
    args.clip_grad = train_args.clip_grad
    args.mem_size = train_args.mem_size
    args.num_class = train_args.num_class
    args.seed = train_args.seed
    #args.seq_len = train_args.seq_len
    args.weighting = train_args.weighting
    args.frame_ind_gap = train_args.frame_ind_gap
    args.state_with_softmax = train_args.state_with_softmax
    args.include_optical_flow_confidence = train_args.include_optical_flow_confidence
    args.include_spatial_dilation = train_args.include_spatial_dilation
    args.time_division = train_args.time_division
    args.use_conv3d = train_args.use_conv3d
    args.use_dropout = train_args.use_dropout
    args.spatial_kernel_size = train_args.spatial_kernel_size if 'spatial_kernel_size' in train_args else args.spatial_kernel_size
    args.use_batch_norm = train_args.use_batch_norm if 'use_batch_norm' in train_args else args.use_batch_norm
    args.use_descriptor_div_8 = train_args.use_descriptor_div_8 if 'use_descriptor_div_8' in train_args else args.use_descriptor_div_8


def calculateResult(target, pred, image, imgFilename, acc_meter, intersection_meter, union_meter, args, dataset_constants):
    imgFilenameForSave = imgFilename.replace('_leftImg8bit', '')
    savePrediction(imgFilenameForSave, pred, args)
    #save_reindexed_colored_image(args, dataset_constants, imgFilenameForSave, pred[0], target[0].data)

    if args.calculateAccIou:
        acc, pix = accuracy(target, pred, args)
        acc_meter.update(acc, pix)
        intersection, union = intersectionAndUnion(target, pred, args.num_class, args)
        intersection_meter.update(intersection)
        union_meter.update(union)


def main():
    modelWeights = torch.load(args.checkpoint,
                         map_location=lambda storage, loc: storage)
    opt = torch.load(os.path.dirname(args.checkpoint) + '/args.pth')
    train_args = opt[0]

    copyTrainArgs(train_args)

    args.list_test = os.path.join(args.data, args.list_test)
    args.root_img = os.path.join(args.data, args.root_img)
    args.root_seg = os.path.join(args.data, args.root_seg)
    args.root_optical_flow = os.path.join(args.data, args.root_optical_flow)
    args.root_pred = os.path.join(args.data, args.root_pred)
    args.root_reindexed_pred = os.path.join(args.data, args.root_reindexed_pred)
    args.root_colored_pred = os.path.join(args.data, args.root_colored_pred)

    args.use_fixed_crop_buffer = 0
    args.official_pspnet_checkpoint=''
    args.use_official_pspnet=0

    if not os.path.exists(args.root_pred):
        os.makedirs(args.root_pred)

    # scales for evaluation
    args.scales = (1,)
    if args.multi_scale == 1:
        args.scales = (0.5, 0.75, 1, 1.25, 1.5)

    test_dataset = DatasetOuter(args.list_test, args, max_sample=args.num_test, is_train=0)
    test_loader = torch.utils.data.DataLoader(test_dataset,
                                               batch_size=args.batch_size,
                                               num_workers=2,
                                               pin_memory=True)

    __import__(args.loop_model)
    Loop = my_import(args.loop_model + '.Loop')
    
    model = Loop(args, use_softmax=False)
    model.load_state_dict(modelWeights)

    if args.use_cuda:
        model.cuda()

    model.eval()

    weights = None
    if train_args.weighting:
        weights = wrap(args.use_cuda, loadClassWeights(args))

    dataset_constants = DatasetFactory.getConstants(args.dataset)
    criterion = nn.NLLLoss2d(weight=weights, ignore_index=dataset_constants.void_labels[0]).cuda()

    acc_meter = AverageMeter()
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()
    pspnet_time_meter = AverageMeter()
    buffer_time_meter = AverageMeter()

    (initial_seg_nets, semanticSegmentationArgs) = loadInitialSegmentationModel(args)

    test_enum = tqdm(test_loader)
    segSize = (args.imgSize, args.imgSize) if args.use_data_augmentation and args.imgSize > 0 else dataset_constants.img_original_size
    total = 0
    pool = ThreadPool(4)

    for keyframe_relpath, seqName, scale, x1, y1, flip, rangeTuple, length in test_enum:

        batch_iter = TBPTTIterOuter(args.list_test, args, keyframe_relpath, seqName, scale, x1, y1, flip, rangeTuple, length, args.seq_len, pool)
        batch_iter = ThreadedGenerator(iterator=batch_iter, queue_maxsize=args.data_queue_size)

        batch_total = 0
        loss_calculation_num = 1e-6
        for _, img, seg, prev_img, label_exist, optical_flow, curr_length, img_ind, imgFilename, seqNameWithPrefix, start, end in batch_iter:

            img = img.contiguous()
            start_pspnet_time = time.time()
            imgDescriptor = getImageDescriptors(img, initial_seg_nets, semanticSegmentationArgs, args, segSize=segSize)
            end_pspnet_time = time.time()

            inputDescriptor = wrap(args.use_cuda, imgDescriptor, volatile=True)
            inputImg = wrap(args.use_cuda, img, volatile=True)
            prevImg = wrap(args.use_cuda, prev_img, volatile=True)
            inputOpticalFlow = wrap(args.use_cuda, optical_flow, volatile=True)
            target = wrap(args.use_cuda, seg, volatile=True).contiguous()
            target_exist = wrap(args.use_cuda, label_exist, volatile=True)

            # Forward
            start_buffer_time = time.time()
            pred = model(inputDescriptor, inputImg, prevImg, inputOpticalFlow, target, target_exist, imgFilename
                , img_ind, scale, x1, y1, flip, seqNameWithPrefix, rangeTuple, start)
            end_buffer_time = time.time()
            indices_for_loss = (target_exist == 1).nonzero()

            pspnet_time_meter.update(end_pspnet_time-start_pspnet_time)
            buffer_time_meter.update(end_buffer_time-start_buffer_time)

            if args.save_all_seq:
                seq_len, batch_size = target.size()[:2]
                for s in xrange(0, seq_len, 1):
                    for b in xrange(0, batch_size, 1):
                        imgFilenameForSave = imgFilename[b][s].replace('_leftImg8bit', '')
                        if imgFilenameForSave!='':
                            save_reindexed_colored_image(args, dataset_constants, imgFilenameForSave, pred[s,b], target[s,b].data)

            if len(indices_for_loss)>0:
                batches_for_loss = indices_for_loss[:, 1].data
                seqs_for_loss = indices_for_loss[:, 0].data

                target_for_loss = target[seqs_for_loss, batches_for_loss, :, :]
                pred_for_loss = pred[seqs_for_loss, batches_for_loss, :, :, :]

                loss = criterion(pred_for_loss, target_for_loss)
                batch_total += loss.data[0]
                loss_calculation_num += 1

                for b, s in zip(batches_for_loss, seqs_for_loss):
                    currImgFilename = keyframe_relpath[0][b]
                    currTarget = target[s, b, :, :].unsqueeze(0)
                    currPred = pred[s, b, :, :, :].unsqueeze(0)
                    currImage = inputImg[s, b, :, :, :].unsqueeze(0)
                    calculateResult(currTarget, currPred, currImage, currImgFilename, acc_meter, intersection_meter, union_meter, args, dataset_constants)

        batch_total = batch_total / loss_calculation_num
        total += batch_total

    if args.calculateAccIou:
        loss_avg = total / len(test_loader)
        iou = intersection_meter.sum / (union_meter.sum + 1e-10)
        for i, _iou in enumerate(iou):
            print('class [{}], IoU: {}'.format(i, _iou))

        iouForMean = iou[union_meter.sum>0]
        if len(iouForMean)>0:
            print('[Eval Summary]:')
            print('Mean IoU: {:.4}, Accuracy: {:.2f}%, Loss_Avg: {:.4f}'.format(iouForMean.mean(), acc_meter.average() * 100, loss_avg))

    avg_pspnet_time = pspnet_time_meter.sum / (len(test_loader) * (args.prev_frames_num + 1))
    avg_buffer_time = buffer_time_meter.sum / (len(test_loader) * (args.prev_frames_num + 1))

    print('avg_pspnet_time1=' + str(avg_pspnet_time) + ' , avg_pspnet_time2=' + str(pspnet_time_meter.avg))
    print('avg_buffer_time1=' + str(avg_buffer_time) + ' , avg_buffer_time2=' + str(buffer_time_meter.avg))

    print('Done!!')

if __name__ == '__main__':
    main()
