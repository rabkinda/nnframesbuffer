import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('op_conf_netwarp_buffer_camvid.txt', sep='\n', lineterminator='\n')
df['op_conf'] = (df['op_conf']*100).astype(int)
df['op_conf'].sort_values().value_counts().sort_index()\
    .plot.bar(figsize=(14,4), title='optical flow confidence histogram')
plt.show()