import argparse
from framesBuffer.datasets.DatasetFactory import DatasetFactory


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--src_txt', type=str, help='src_txt')
parser.add_argument('--trg_txt', type=str, help='trg_txt')
parser.add_argument('--prev_frames_num', type=int, default=5, help='prev_frames_num')
parser.add_argument('--dataset', type=str, help='dataset', default='camvid')
args = parser.parse_args()

dataset_constants = DatasetFactory.getConstants(args.dataset)
filenameUtils = DatasetFactory.getFilenameUtils(args.dataset, args.src_txt)

trg_file = open(args.trg_txt, 'w')
list_sample = filenameUtils.getListSample()

for sample in list_sample:
    seq_name = filenameUtils.getCatagory(sample.getImgPath())
    startFrameInd = filenameUtils.getStartFrameInd(seq_name)
    frame_t_ind = filenameUtils.getFrameIndex(sample.getImgPath())

    for i in xrange(args.prev_frames_num, 0, -1):
        if frame_t_ind-i>=startFrameInd:
            trg_file.write(filenameUtils.getFramePaths(seq_name, frame_t_ind-i).getImgPath()+'\n')

    trg_file.write(sample.getImgPath()+'\n')

trg_file.close()
print('Done!!')