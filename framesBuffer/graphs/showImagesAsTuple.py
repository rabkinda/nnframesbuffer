import argparse
import os
import sys
import numpy as np
import scipy
from scipy.misc import imsave

from framesBuffer.datasets.DatasetFactory import DatasetFactory

sys.path.append('..')


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--images_dir', type=str, help='images_dir')
parser.add_argument('--annotation_dir', type=str, help='annotation_dir')
parser.add_argument('--mine_colored_predictions_dir', type=str, help='mine_colored_predictions_dir')
#parser.add_argument('--others_colored_predictions_dir', type=str, help='others_colored_predictions_dir')
parser.add_argument('--pspnet_colored_predictions_dir', type=str, help='pspnet_colored_predictions_dir')
parser.add_argument('--dst_dir', type=str, help='dst_dir')
parser.add_argument('--txt_with_filenames', type=str, help='txt_with_filenames')
parser.add_argument('--dataset', type=str, default='cityscapes', help='dataset')
args = parser.parse_args()

filenameUtils = DatasetFactory.getFilenameUtils(args.dataset, args.txt_with_filenames)
list_sample = filenameUtils.getListSample()

def getFileNameShorter(file_rel_path):
    return file_rel_path.replace('_leftImg8bit','').replace('_gtFine_labelTrainIds', '')

for file_rel_path in list_sample:
    base_file_name = getFileNameShorter(file_rel_path.getImgPath())
    img = scipy.misc.imread(os.path.join(args.images_dir, file_rel_path.getImgPath()), mode='RGB')
    gt = scipy.misc.imread(os.path.join(args.annotation_dir, file_rel_path.getGTPath()), mode='RGB')
    mine_pred = scipy.misc.imread(os.path.join(args.mine_colored_predictions_dir, base_file_name), mode='RGB')
    #others_pred = scipy.misc.imread(os.path.join(args.others_colored_predictions_dir, file_rel_path), mode='RGB')
    pspnet_pred = scipy.misc.imread(os.path.join(args.pspnet_colored_predictions_dir, base_file_name), mode='RGB')

    im_vis = np.concatenate((img, gt, pspnet_pred, mine_pred), axis=0).astype(np.uint8)#others_pred, mine_pred), axis=0).astype(np.uint8)
    imsave(os.path.join(args.dst_dir, (base_file_name.split(os.sep)[-1]).replace('.png','.jpg')), im_vis)

print('Done!!')