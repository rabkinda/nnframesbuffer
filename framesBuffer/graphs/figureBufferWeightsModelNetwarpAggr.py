import sys
from framesBuffer.models.final_models.modelNetwarpAggrSeqOpticalFlow import Loop
import argparse
import torch
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
sys.path.append('..')


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--checkpoint_dir', type=str, help='checkpoint_dir')
args = parser.parse_args()

checkpoint = os.path.join('/home/rabkinda/Documents/thesis/final_checkpoints/cityscapes', args.checkpoint_dir)

weights = torch.load(checkpoint + '/bestmodel.pth', map_location=lambda storage, loc: storage)
train_args = torch.load(checkpoint + '/args.pth')
opt = train_args[0]
opt.checkpoint = checkpoint
if not ('dataset' in opt):
    opt.dataset = 'camvid'
opt.use_fixed_crop_buffer=1
model = Loop(opt)
model.load_state_dict(weights)

N_o = weights['decoder.N_o.aggr_module.conv_w.weight'].numpy()
N_u = weights['decoder.N_u.aggr_module.conv_w.weight'].numpy()

N_list = [N_o, N_u]

op_median_confidence = 0.67#0.87- cityscapes

fig = plt.figure(figsize=(8, 4.25))
ax = fig.add_subplot(111)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

names = ('N_o', 'N_u')
op_median_confidence_arr = np.array([1.0, op_median_confidence, op_median_confidence**2, op_median_confidence**3])

for i, layer in enumerate(N_list):
    N_plot = np.abs(layer).squeeze()
    N_plot = np.mean(N_plot.reshape(opt.mem_size, opt.num_class), 1)*op_median_confidence_arr
    plt.plot(np.arange(1, N_plot.shape[0]+1,1, dtype=np.uint8), N_plot,
             ('-r*' if names[i].startswith('N_o') else '-bo'))

plt.legend(names)

plt.xlim([0.75, opt.mem_size+0.25])
plt.xlabel('Buffer Position', fontsize=13)
plt.ylabel('Mean Of Absolute Weights', fontsize=13)
plt.grid()
plt.savefig('./results/'+args.checkpoint_dir+'_Buffer_Weights.png', dpi=200)
