import argparse
import glob
import os
import sys
import numpy as np
import multiprocessing
from PIL import Image
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.semanticSegmentation.utils import get_reindexed_image, get_colored_image

sys.path.append('..')


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--prediction_dir', type=str, help='checkpoint_dir')
parser.add_argument('--colored_prediction_dir', type=str, help='colored_prediction_dir')
parser.add_argument('--annotation_dir', type=str, help='annotation_dir')
parser.add_argument('--add_black', type=int, default=1, help='add_black')
args = parser.parse_args()

dataset_constants = DatasetFactory.getConstants('cityscapes')
pred_files = glob.glob(os.path.join(args.prediction_dir, 'val/**/*.png'))


def save_colored_seg(pred_file):
    predictionImg = Image.open(pred_file)
    predictionNp = np.array(predictionImg)

    if args.add_black:
        gtImg = Image.open(pred_file.replace('.png', '_gtFine_labelTrainIds.png').replace(args.prediction_dir, args.annotation_dir))
        gtNp = np.array(gtImg)
        predictionNp[gtNp==dataset_constants.void_labels[0]] = dataset_constants.non_void_nclasses

    seg_map = get_colored_image(predictionNp, dataset_constants.palette)

    color_output_path = pred_file.replace(args.prediction_dir, args.colored_prediction_dir)
    if not os.path.exists(os.path.dirname(color_output_path)):
        os.makedirs(os.path.dirname(color_output_path))
    seg_map.save(color_output_path)

pool = multiprocessing.Pool(4)
pool.map(save_colored_seg, pred_files)

print('Done!!')