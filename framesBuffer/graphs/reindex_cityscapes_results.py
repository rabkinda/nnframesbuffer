import argparse
import glob
import os
import sys
import numpy as np
import multiprocessing
from PIL import Image
from framesBuffer.datasets.DatasetFactory import DatasetFactory
from framesBuffer.semanticSegmentation.utils import get_reindexed_image
sys.path.append('..')


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--prediction_dir', type=str, help='checkpoint_dir')
parser.add_argument('--reindex_dir', type=str, help='reindex_dir')
args = parser.parse_args()

dataset_constants = DatasetFactory.getConstants('cityscapes')
pred_files = glob.glob(os.path.join(args.prediction_dir, 'val/**/*.png'))


def save_reindexed_seg(pred_file):
    predictionImg = Image.open(pred_file)
    predictionNp = np.array(predictionImg)
    seg_map = get_reindexed_image(predictionNp, dataset_constants.cIds)

    index_output_path = pred_file.replace(args.prediction_dir, args.reindex_dir)
    if not os.path.exists(os.path.dirname(index_output_path)):
        os.makedirs(os.path.dirname(index_output_path))
    seg_map.save(index_output_path)

pool = multiprocessing.Pool(4)
pool.map(save_reindexed_seg, pred_files)

print('Done!!')