import argparse
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import torch
from matplotlib.ticker import MaxNLocator
from framesBuffer.models.final_models.modelConvAggrSeqOpticalFlow import Loop
sys.path.append('..')


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--checkpoint_dir', type=str, help='checkpoint_dir')
args = parser.parse_args()

checkpoint = os.path.join('/home/rabkinda/Documents/thesis/final_checkpoints/camvid', args.checkpoint_dir)

weights = torch.load(checkpoint + '/bestmodel.pth', map_location=lambda storage, loc: storage)
train_args = torch.load(checkpoint + '/args.pth')
opt = train_args[0]
opt.checkpoint = checkpoint
opt.use_padding = 0
model = Loop(opt)
model.load_state_dict(weights)

N_o_first_conv = np.apply_over_axes(np.mean, np.abs(weights['decoder.N_o.aggr_module.convSeq.0.weight'].numpy()), (0, 1, 3, 4)).squeeze()
N_u_first_conv = np.apply_over_axes(np.mean, np.abs(weights['decoder.N_u.aggr_module.convSeq.0.weight'].numpy()), (0, 1, 3, 4)).squeeze()

N_o_second_conv = np.apply_over_axes(np.mean, np.abs(weights['decoder.N_o.aggr_module.convSeq.2.weight'].numpy()), (0, 1, 3, 4)).squeeze()
N_u_second_conv = np.apply_over_axes(np.mean, np.abs(weights['decoder.N_u.aggr_module.convSeq.2.weight'].numpy()), (0, 1, 3, 4)).squeeze()

op_median_confidence = 0.86#0.89#(1-F.tanh(torch.from_numpy(numpy.array([numpy.mean(numpy.abs(weights['decoder.ConvRtGate.weight'].numpy()))])))).numpy()[0]

N_u_buffer = [N_u_first_conv[0] * N_u_second_conv[0],
              (((N_u_first_conv[1] * N_u_second_conv[0]) + (N_u_first_conv[0] * N_u_second_conv[1]))/2.0) * op_median_confidence,
              (((N_u_first_conv[1] * N_u_second_conv[1]) + (N_u_first_conv[0] * N_u_second_conv[2]))/2.0) * (op_median_confidence ** 2),
              (N_u_first_conv[1] * N_u_second_conv[2]) * (op_median_confidence ** 3)]

N_o_buffer = [N_o_first_conv[0] * N_o_second_conv[0],
              (((N_o_first_conv[1] * N_o_second_conv[0]) + (N_o_first_conv[0] * N_o_second_conv[1]))/2.0) * (op_median_confidence),
              (((N_o_first_conv[1] * N_o_second_conv[1]) + (N_o_first_conv[0] * N_o_second_conv[2]))/2.0) * (op_median_confidence ** 2),
              (N_o_first_conv[1] * N_o_second_conv[2]) * (op_median_confidence ** 3)]


fig = plt.figure(figsize=(8, 4.25))
ax = fig.add_subplot(111)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

names = ('N_o', 'N_u')

N_plot = np.array(N_o_buffer)
plt.plot(np.arange(1, opt.mem_size+1,1, dtype=np.uint8), N_plot,
         ('-r*' if names[0].startswith('N_o') else '-bo'))

N_plot = np.array(N_u_buffer)
plt.plot(np.arange(1, opt.mem_size+1,1, dtype=np.uint8), N_plot,
         ('-r*' if names[1].startswith('N_o') else '-bo'))

plt.legend(names)

plt.xlim([0.75, opt.mem_size+0.25])
plt.xlabel('Buffer Position', fontsize=13)
plt.ylabel('Mean Of Absolute Weights', fontsize=13)
plt.grid()
plt.savefig('./results/'+args.checkpoint_dir+'_Buffer_Weights.png', dpi=200)

#========================================================================================================
#second graph
#========================================================================================================

# fig = plt.figure(figsize=(8, 4.25))
# ax = fig.add_subplot(111)
# ax.xaxis.set_major_locator(MaxNLocator(integer=True))
#
# names = ('N_o_first_conv', 'N_o_second_conv', 'N_u_first_conv', 'N_u_second_conv')
# op_median_confidence_first_conv = [1.0, op_median_confidence]
# op_median_confidence_second_conv = [1.0, op_median_confidence, op_median_confidence**2]
# N_list = [N_o_first_conv*op_median_confidence_first_conv, N_o_second_conv*op_median_confidence_second_conv,
#           N_u_first_conv*op_median_confidence_first_conv, N_u_second_conv*op_median_confidence_second_conv]
#
# for i, layer in enumerate(N_list):
#     N_plot = layer
#     plt.plot(np.arange(1, N_plot.shape[0]+1,1, dtype=np.uint8), N_plot,
#              ('-r' if names[i].startswith('N_o') else '-b') + ('o' if 'first' in names[i] else '*'))
#
# plt.legend(names)
#
# plt.xlim([0.75, opt.mem_size+0.25])
# plt.xlabel('Conv Temporal Position', fontsize=13)
# plt.ylabel('Mean Of Absolute Weights', fontsize=13)
# plt.grid()
# plt.savefig('./results/'+args.checkpoint_dir+'_Conv_Temporal_Weights.png', dpi=200)