import collections
import sys
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MaxNLocator
sys.path.append('..')


wavenet_results = {2: 70.98, 4: 71.03}
netwarp_results = {2: 71.13, 3: 71.68, 4: 71.8, 5: 71.79}
conv_results = {2: 70.79, 3: 71.19, 4: 70.98, 5: 71.34}

fig = plt.figure(figsize=(8, 4.25))
ax = fig.add_subplot(111)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))

names = ('wavenet_aggr', 'netwarp_aggr', 'conv_aggr')

wavenet_results = collections.OrderedDict(sorted(wavenet_results.items()))
netwarp_results = collections.OrderedDict(sorted(netwarp_results.items()))
conv_results = collections.OrderedDict(sorted(conv_results.items()))

N_plot = np.array(wavenet_results.values())
plt.plot(np.array([np.uint8(buffer_size) for buffer_size in wavenet_results.keys()]), N_plot, '-r*')

N_plot = np.array(netwarp_results.values())
plt.plot(np.array([np.uint8(buffer_size) for buffer_size in netwarp_results.keys()]), N_plot, '-bo')

N_plot = np.array(conv_results.values())
plt.plot(np.array([np.uint8(buffer_size) for buffer_size in conv_results.keys()]), N_plot, '-g^')

plt.legend(names)

plt.xlim([1.75, 5+0.25])
plt.xlabel('Buffer Size', fontsize=13)
plt.ylabel('Mean Iou', fontsize=13)
plt.grid()
plt.savefig('./results/Results_vs_Buffer_Size.png', dpi=200)