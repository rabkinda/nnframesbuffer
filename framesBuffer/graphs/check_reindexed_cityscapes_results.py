import argparse
import glob
import multiprocessing
import os
import sys
import numpy as np
from PIL import Image
from framesBuffer.datasets.DatasetFactory import DatasetFactory
sys.path.append('..')


parser = argparse.ArgumentParser(description='PyTorch Loop')
parser.add_argument('--reindex_dir', type=str, help='reindex_dir')
args = parser.parse_args()

dataset_constants = DatasetFactory.getConstants('cityscapes')
pred_files = glob.glob(os.path.join(args.reindex_dir, 'test/**/*.png'))


def sublist(lst1, lst2):
   filtered_lst1 = [element for element in lst1 if element in lst2]
   return len(filtered_lst1) == len(lst1)


def check_reindexed_seg(pred_file):
    predictionImg = Image.open(pred_file)
    predictionNp = np.array(predictionImg)

    assert(sublist(np.unique(predictionNp), dataset_constants.cIds.values()))


pool = multiprocessing.Pool(4)
pool.map(check_reindexed_seg, pred_files)

print('Done!!')